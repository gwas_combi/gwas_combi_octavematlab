% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [asympt_p, chisq_value] = chi_square_goodness_of_fit_test(data, labels)

% NOTE It seems that somewhen, somewhere, the data came encoded with 3
%   values per SNP, but as of now, this should never be the case anymore.
has_index = 0;
if (has_index)
	vals_per_snp = 3; %#ok<UNRCH> % per SNP: [allele1, allele2, index]
else
	vals_per_snp = 2; % per SNP: [allele1, allele2]
end

%% Assertions

assert(mod(size(data, 2), vals_per_snp) == 0);

%% Initialization

labels = labels(:)';
individuals = size(data, 1);
num_snps = size(data, 2) / vals_per_snp;
nillich = uint8('0');
%lex_min = zeros(1, num_snps, 'uint8');
%lex_max = zeros(1, num_snps, 'uint8');
%call_rate = zeros(1, num_snps);
tables = zeros(2, 3, num_snps);
%maf = zeros(1, num_snps);
%asympt_p = zeros(1, num_snps);
%chisq_value = zeros(1, num_snps);
%tolerance = 2 * eps(1.0);

%% Prepare data

allele1 = uint8(data(:, 1:vals_per_snp:end));
allele2 = uint8(data(:, 2:vals_per_snp:end));

%% find successfully called individuals and calculate calling rate
valide = (allele1 ~= nillich) & (allele2 ~= nillich);
%nutz_n = sum(valide, 1);
%call_rate = nutz_n / individuals;

%% build (2x3)-tables
temp1 = allele1;
temp2 = allele2;
temp1(~valide) = 255;
temp2(~valide) = 255;
lex_min_1 = min(temp1);
lex_min_2 = min(temp2);
lex_min = min(lex_min_1, lex_min_2);
%lex_max_1 = max(allele1);
%lex_max_2 = max(allele2);
%lex_max = max(lex_max_1, lex_max_2);
clear temp;

cases = (labels == 1)';
cases = repmat(cases, 1, num_snps);
controls = (labels == -1)';
controls = repmat(controls, 1, num_snps);

valide_cases = valide & cases;
num_valide_cases = sum(valide_cases)';
valide_controls = valide & controls;
num_valide_controls = sum(valide_controls)';

clear cases controls;

min_mat = repmat(lex_min, individuals, 1);

lex_min_mat1  = allele1 == min_mat;
lex_min_mat2  = allele2 == min_mat;
not_equal_mat = allele1 ~= allele2;

clear min_mat;

%% calculate the contingency tables
tables(1, 1, :) = sum(lex_min_mat1 & lex_min_mat2 & valide_cases, 1);
tables(1, 2, :) = sum(not_equal_mat & valide_cases, 1);
tables(1, 3, :) = num_valide_cases - squeeze(tables(1, 1, :)) - squeeze(tables(1, 2, :));
tables(2, 1, :) = sum(lex_min_mat1 & lex_min_mat2 & valide_controls, 1);
tables(2, 2, :) = sum(not_equal_mat & valide_controls, 1);
tables(2, 3, :) = num_valide_controls - squeeze(tables(2, 1, :)) - squeeze(tables(2, 2, :));

%% compute minor allele frequencies (MAFs)
%s = 2*(tables(1, 1, :) + tables(2, 1, :)) + tables(1, 2, :) + tables(2, 2, :);
%freq = squeeze(s(1, 1, :)) ./ (2*nutz_n(:));
%maf(:) = min(freq, (1.0 - freq));

%% compute p-values
row_marginals = squeeze(sum(tables, 2));
col_marginals = squeeze(sum(tables, 1));
if (num_snps == 1)
	col_marginals = col_marginals';
end
N = squeeze(sum(row_marginals));
chisq_value = zeros(1, num_snps);
%e = zeros(num_snps, 1);

for i = 1:2
	for j = 1:3
		e = (row_marginals(i, :) .* col_marginals(j, :)) ./ N;
		if ~(sum(e == 0) == length(e))
			chisq_value(e ~= 0) = chisq_value(e ~= 0) + ((squeeze(tables(i, j, e ~= 0))' - e(e ~= 0)).^2 ./ e(e ~= 0));
		end
	end
end
asympt_p = 1 - chi2cdf(chisq_value, 2);

