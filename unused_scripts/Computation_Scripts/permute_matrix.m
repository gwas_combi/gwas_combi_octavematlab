% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function matrix_ =  permute_matrix(matrix,pi_from,pi_to,dim)

assert(dim==1||dim==2);

if (size(matrix,2)==1)&&(length(pi_from)>1)
  matrix = matrix';
  dim = 2;
end

matrix_ = matrix;
if dim==1
  matrix_(pi_to,:) = matrix(pi_from,:);
else
  matrix_(:,pi_to) = matrix(:,pi_from);
end
