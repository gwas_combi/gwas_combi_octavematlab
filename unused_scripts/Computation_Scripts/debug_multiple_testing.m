% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

load('/home/bmieth/svn/iew/data/wtccc/BipolarDisorder/chromo_09.mat');
load('/home/bmieth/svn/iew/data/wtccc/BipolarDisorder/labels');
data = X;
X = string_to_featmat(X);
X_sicher = X;

snp = data(:,19224*3+1:19227*3);
p_chi = multiple_testing_chi(snp,y);
pvalue_bug.chi = p_chi(2);

p_fisher = multiple_testing_fisher(snp,y);
pvalue_bug.fisher_exact = p_fisher(2);


% features
X(1:10,1:10)
X = scale(center(X_sicher),6);
X = scale(center(X_sicher));
X = center(scale(X_sicher));
X = center(scale(X_sicher,6));

% training
result = train_libsvm(X,y,1,0.01)
result = train_liblinear(X,y,1,0.01)
result = train_l1lr(X,y,1,0.01)

%analysis
[foo ind] = sort(result.w,'descend');
find(ind==19226)
plot(result.w);