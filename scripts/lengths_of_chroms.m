% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function lengths_of_chroms(datadir, savedir, permtestdir)
% Save weights proportional to lenghts of chromosomes in file
% example values:
% datadir = '/home/bmieth/svn/iew/data/wtccc/';
% savedir = '/home/bmieth/svn/iew/qsub/wtccc_crohn/results/WTCCC/permtest/Crohn/with_aplha_weights/';
% permtestdir = '/home/bmieth/svn/iew/qsub/wtccc_crohn/results/WTCCC/permtest/Crohn/with_new_optimal_Cs/';

num_chromosomes = 22;
lengths_of_chroms = zeros(num_chromosomes, 1);

for chromo = 1:num_chromosomes
  data = load([datadir, 'chromo_', int2str_leading_zeros(chromo, 2), '.mat']);
  lengths_of_chroms(chromo) = size(data.X, 2);
end

weights = lengths_of_chroms ./ sum(lengths_of_chroms);

savefile = [savedir, 'weights_of_chroms.mat'];
save(savefile, 'weights')


%% Postprocessing: If you have permtest results for alpha, then you can scale alpha without running permtest again!

for chromo = 1:num_chromosomes
	load([permtestdir, 'permtest_result', int2str_leading_zeros(chromo, 2), '.mat']);
	alpha_sig = alpha_sig * weights(chromo);

	% Find t_star as alpha-percentile of sorted p-values
	t_star = p_lowest_sorted(round(B * alpha_sig));

	savefile = [savedir, 'permtest_result', int2str_leading_zeros(chromo, 2), '.mat'];
	save(savefile, 'alpha_sig', 't_star');
end

