% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function K = addRidge(K, epsilon)
% Adds a small ridge to the diagonal of the kernel matrix/matrices.
%
% Synopsis:
%   K = addRidge(K);
%   K = addRidge(K, epsilon);
% 
% Arguments: 
%   K:             kernel matrix
%   epsilon:       ridge constant  (default = 10^(-10))
%                  the ridge is calculated as ridge constant *  mean(diag(K))
%
% Return:
%   K:             kernel matrix
%
% Author: Marius Kloft

if exist('epsilon', 'var')
	epsRidge = epsilon;
else
	epsRidge = 10^(-10);
end

if (ndims(K) == 2)
	K = K + epsRidge * mean(diag(K)) * speye(size(K, 2));
elseif (ndims(K) == 3)
	for k = 1:size(K, 3)
		K(:, :, k) = K(:, :, k) + ...
			epsRidge * mean(diag(K(:, :, k))) * speye(size(K, 2));
	end
end
