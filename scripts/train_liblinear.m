% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_liblinear(X, y, encoding_factor, rep, Cs, p, classy, epsilon, using_shogun, debug_output, debug_midfix)
% Trains libLinear (through the Shogun Toolbox) with the given data,
% and returns the results (the state of the classifier) in a struct.
% using_shogun can be:
% * 0 for using libLinear directly, through its own Matlab/Octave interface
% * 1 to use it through the Shogun Toolbox

tic;

X = double(X); % HACK This should be fixed to limit memory requirements...


%% PARAMETERS
if ~exist('encoding_factor', 'var'), encoding_factor = 3; end % HACK We only supply this default value for backwards compatibility. The encoding factor should actually always be passed as argument!
if ~exist('Cs', 'var'), Cs = logspace(-5, 5, 11); end
num_Cs = size(Cs, 1);
if ~exist('rep', 'var')
	if num_Cs == 1,
		rep = 1;
	else
		rep = 30;
	end
end
if ~exist('p', 'var'), p = 2; end
if ~exist('epsilon', 'var'),
	epsilon = 1e-3;
end
if ~exist('classy', 'var'),
	classy = 'LIBLINEAR_L2R_L1LOSS_SVC_DUAL';
	% Can be: LIBLINEAR_L2R_LR, LIBLINEAR_L2R_L2LOSS_SVC_DUAL, LIBLINEAR_L2R_L2LOSS_SVC, LIBLINEAR_L2R_L1LOSS_SVC_DUAL, 'LIBLINEAR_L1R_L2LOSS_SVC'
end
if ~exist('using_shogun', 'var')
	using_shogun = 0; % by default: use libLinear directly
end
if ~exist('debug_output', 'var')
	debug_output = 0; % by default: no debug output
end
if ~exist('debug_midfix', 'var')
	debug_midfix = '';
end
loglevel = 'ERROR'; % 'ERROR' or 'ALL'

num_feat = size(X, 2);

%% Sanity Checks
assert(mod(num_feat, encoding_factor) == 0, ...
	['The specified encoding factor does not correspond with the data (num_feat % encoding_factor =!= 0): ', ...
	num2str(num_feat), ' % ', num2str(encoding_factor), ' = ', num2str(mod(num_feat, encoding_factor))]);


%% INITIALIZATION
num_data = size(y, 1);
if (classy(12) == '1')
	Cs = Cs * ((num_feat / encoding_factor) ^ 0.5);
end
acc_val = zeros(rep, num_Cs);
acc_test = zeros(rep, num_Cs);
w_val = zeros(rep, num_feat, num_Cs);
%w = zeros(rep, num_feat);
%ranking = zeros(rep, num_feat, num_Cs);


%% VALIDATION LOOP
if rep > 1 || num_Cs > 1
	% if rep == 1 skip validation loop!

	idx = 0;
	for i = 1:rep
		fprintf('Validation loop, repetition %d / %d\n...', i, rep);
		%% TRAIN / VAL SPLITS
		perm = randperm(num_data);
		divtr = perm(1:floor(num_data / 2));
		divval = perm(floor(num_data / 2) + 1:floor(num_data * 3 / 4));
		divte = perm(floor(num_data * 3 / 4) + 1:end);
		Xtr = X(:, divtr);
		Xval = X(:, divval);
		Xte = X(:, divte);
		ytr = y(divtr);
		yval = y(divval);
		yte = y(divte);

		for j = 1:num_Cs
			C = Cs(j);

			%% SVM TRAINING, VALIDATION, TESTING
			if strcmp(classy, 'LIBLINEAR_L1R_L2LOSS_SVC')
				Xtr = Xtr';
			end
			[w_val(i, :, j), acc_val(i, j), acc_test(i, j)] = ...
				liblinear_raw_trainAndPredict(Xtr, ytr, Xval, yval, Xte, yte, C, classy, epsilon, loglevel, using_shogun);

			idx = idx + 1;
			%print_progress(idx, rep * num_Cs);
		end
	end

	%% MODEL SELECTION
	[~, ind] = max(mean(acc_val, 1)); % returns [max, index_max]
	acc_test = mean(acc_test(:, ind), 1);
	C = Cs(ind);
else
	C = Cs;
end


%% SVM TRAINING ON FULL DATA
if strcmp(classy, 'LIBLINEAR_L1R_L2LOSS_SVC')
	X = X';
end
% XXX We do this for now, because it is not possible to use a bias term
% in the Java implementation of libLinear, which is used by GWASpi.
% This way we get comparable results.
use_bias = 0;
[weights_per_feature_normalized, weights_per_feature_raw] = liblinear_raw_train(X, y, C, classy, epsilon, loglevel, using_shogun, use_bias);

weights_per_snp_raw_matrix = reshape(weights_per_feature_raw(:)', encoding_factor, num_feat / encoding_factor);
weights_per_snp_normalized_matrix = reshape(weights_per_feature_normalized(:)', encoding_factor, num_feat / encoding_factor);
weights_per_snp_matrix = weights_per_snp_raw_matrix;

if (debug_output)
	dlmwrite(['w_encoded_', debug_midfix, '_raw.txt'], weights_per_snp_raw_matrix, 'delimiter', ' ', 'precision', '%.6f');
	dlmwrite(['w_encoded_', debug_midfix, '_normalized.txt'], weights_per_snp_normalized_matrix, 'delimiter', ' ', 'precision', '%.6f');
end

%% COMPUTE RANKINGS w
if (encoding_factor > 1)
	w = sum(abs(weights_per_snp_matrix) .^ p) .^ (1 / p);
else
	% There is no need to squeeze one dimension together into one again
	w = weights_per_snp_matrix;
end

if (debug_output)
	dlmwrite(['w_', debug_midfix, '_raw.txt'], w, 'delimiter', ' ', 'precision', '%.6f');
end

w = w / norm(w);

if (debug_output)
	dlmwrite(['w_', debug_midfix, '_normalized.txt'], w, 'delimiter', ' ', 'precision', '%.6f');
end

%% COLLECT RESULTS
if rep > 1
	w_avg = median(w_val(:, :, ind), 1);
	w_avg = mean(reshape(w_avg(:)', encoding_factor, num_feat / encoding_factor), 1);
	w_avg = w_avg / norm(w_avg);
	result = struct( ...
		'w_avg', w_avg(:)', ...
		'w', w(:)', ...
		'acc_val', acc_val, ...
		'acc_test', acc_test, ...
		'Cs', Cs, ...
		'C', C, ...
		'rep', rep, ...
		'classy', classy, ...
		'epsilon', epsilon, ...
		'time', toc);
else
	result = struct( ...
		'w', w(:)', ...
		'Cs', Cs, ...
		'C', C, ...
		'rep', rep, ...
		'classy', classy, ...
		'epsilon', epsilon, ...
		'time', toc);
end

liblinear_raw_cleanup(using_shogun);

