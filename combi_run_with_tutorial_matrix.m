% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [p_values, selected_indices, svm_weights, selected_rs_ids] ...
	= combi_run_with_tutorial_matrix()
% Import data from PLink Binary data-set 'Tutorial_Matrix',
% and run the COMBI method on it.

add_required_paths();

dataset_name = 'Tutorial_Matrix';
input_dir = [pwd, '/data/'];
input_files = { ...
	[input_dir, dataset_name, '.fam']; ...
	[input_dir, dataset_name, '.bim']; ...
	[input_dir, dataset_name, '.bed'] ...
	};
dataset_info = dataset_info_create('PLinkBinary', input_files);

output_dir = [input_dir, get_host_app_name(), '/'];

[p_values, selected_indices, svm_weights, selected_rs_ids] ...
	= combi(dataset_info, output_dir);
