% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function   pvalues_final = multi_split( data, labels, num_splits, top_k , pnorm_feature_scaling, svm_rep, Cs, p, classy, filter_window_size,p_pnorm_filter, gammas_multisplit)
num_snps = size(data,2)/3;

%keyboard;
tic;
% PARAMETERS
if ~exist('num_splits'), num_splits = 1000; end

if size(labels,1)>size(labels,2), labels=labels'; end

% INITIALIZATION 
num_subj = length(labels);

%  LOOP
% TRAINING
w_train = zeros(num_splits, num_snps);
ind_train = zeros(num_splits, num_subj/2);
ind_val= zeros(num_splits, num_subj/2);
labels_perm_train= zeros(num_splits, num_subj/2);
labels_perm_val= zeros(num_splits, num_subj/2);

for i = 1:num_splits
	print_progress(i, num_splits);

	% RANDOM train and validation sets
	perm = randperm(num_subj);	 
	ind_train(i,:) = perm(1:ceil(num_subj/2));
	ind_val(i,:) = perm(ceil(num_subj/2)+1:end);
	labels_perm_train(i,:) = labels(ind_train(i,:));
	labels_perm_val(i,:) = labels(ind_val(i,:));
	
	%calculate weights
	%%% string data to feature_matrix %%%
	featmat1 = string_to_featmat( data(ind_train(i,:),:) );
	featmat1 = double(featmat1);
	featmat1 = center(featmat1);
	featmat1 = scale(featmat1,pnorm_feature_scaling);

	%%% SVM training %%%
	res1 = train_liblinear( featmat1 , labels_perm_train(i,:), svm_rep, Cs, p, classy);

	%%% Postprocessing with Moving Average Filter %%%
	w_train(i,:) = filter_pnorm(res1.w,filter_window_size,p_pnorm_filter);
	
end

%pre-screening
[foo, ranking] = sort(w_train, 2, 'descend');
indices = ranking(:,1:top_k);
%selector = ranking_to_selector(ranking1,top_k);  

%%% Compute p-values Bonferoni %%%
%pvalues_final  = chi_square_goodness_of_fit_test(data2,labels2);
%pvalues_final = min(pvalues_final* top_k,1);

%pvalues_final = process_pvalues(pvalues_final,selector);



% VALIDATION
% Compute p-values Bonferoni %%%
pvalue_all_val = ones(num_splits, num_snps);

for i = 1:num_splits
	print_progress(i, num_splits);

	indices_now_ = indices(i,:);
	indices_now_ = sort(indices_now_);
	indices_now_3 = sort([indices_now_*3, indices_now_*3-1, indices_now_*3-2],2); 

	pvalue_all_ =chi_square_goodness_of_fit_test(data(ind_val(i,:),indices_now_3) ,labels_perm_val(i,:));
	%pvalue_all_val(i, indices_now_)=min(pvalue_all_* top_k,1);
	pvalue_all_val(i, indices_now_)= pvalue_all_;
end

pvalue_all_val_sorted = sort(pvalue_all_val);
quantiles = ones(length(gammas_multisplit), num_snps);
for j=1:length(gammas_multisplit)
	print_progress(j, length(gammas_multisplit));
	quantiles(j,:)=pvalue_all_val_sorted(max(ceil(num_splits*gammas_multisplit(j))-1, 1), :);

end
max_quantiles = max(quantiles);

pvalues_final = min((1-log(min(gammas_multisplit)))*max_quantiles,1);

