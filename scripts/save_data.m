% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function save_data(save_dir, data, save_labels, save_chromosomewise_data)
% Saves a GWAS data-set in the format used by the COMBI method
% Matlab/Octave scripts to '*.mat' files.

%% Set argument default values
if ~exist('save_labels', 'var')
	save_labels = 1;
end
if ~exist('save_chromosomewise_data', 'var')
	save_chromosomewise_data = 1;
end

fprintf('\nSaving GWAS data-set to "%s" ...\n', save_dir);


%% Write the info file
info_file = [save_dir, 'README.markdown'];
if (~exist(info_file, 'file'))
	fprintf('writing the info file "%s" ...\n', info_file);
	info_file_id = fopen(info_file, 'w');
	fprintf(info_file_id, 'This directory contains a genomic data-set in the format used by\n');
	fprintf(info_file_id, 'the (%s) COMBI method scripts.\n', get_host_app_name(1));
	fprintf(info_file_id, 'See "save_data.m" for details of that format,\n');
	fprintf(info_file_id, 'and "loaded_dataset_info.txt" for meta-data of the currently loaded data.\n');
	fclose(info_file_id);
end

%% Save complete dataset
savefile_data = [save_dir, 'complete.mat'];
fprintf('saving data to file "%s" ...\n', savefile_data);
save_options_big_data_struct ...
	= create_save_big_options(savefile_data, '-struct', 'data');
save(save_options_big_data_struct{:});


%% Save chromosomewise data in files
if (save_chromosomewise_data)
	num_chromosomes = 22;
	chromos = data.chromos;
	double_chromos_sorted = sort([chromos; chromos]);
	chromos_sorted = sort(chromos);
	for ci = 1:num_chromosomes
		filename = [save_dir, 'chromo_', int2str_leading_zeros(ci, 2), ...
			'.mat'];
		fprintf('saving chromosome %d / %d to file "%s" ...\n', ...
			ci, num_chromosomes, filename);
		alleles = extract_chrom(data.alleles, ci, double_chromos_sorted); %#ok<NASGU> Supress "unused value" warning; see `save()` below
		indices = (chromos_sorted == ci);
		rs_ident = data.rs_ident(indices); %#ok<NASGU> Supress "unused value" warning; see `save()` below
		save_options_big_chromosome_data ...
			= create_save_big_options(filename, 'alleles', 'indices', 'rs_ident');
		save(save_options_big_chromosome_data{:});
		clear alleles indices rs_ident;
	end
end


%% Save labels
if (save_labels)
	affection = data.affection; %#ok<NASGU> Supress "unused value" warning; see `save()` below
	chromos = data.chromos; %#ok<NASGU> Supress "unused value" warning; see `save()` below
	rs_ident = data.rs_ident; %#ok<NASGU> Supress "unused value" warning; see `save()` below
	savefile_labels = [save_dir, 'labels.mat'];
	fprintf('saving labels to file "%s" ...\n', savefile_labels);
	save(savefile_labels, 'affection', 'chromos', 'rs_ident');
end

fprintf('done saving GWAS data-set.\n\n');

