% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function manhattan_per_chromosome(results_dir, chromosomes, diseases)
% Pretty version of (chromosome wise) manhattan plot(s)

chrom_to_plot = chromosomes;
num_chromosomes = size(chrom_to_plot, 2);
disease_count = size(diseases, 1);


t_star_all = zeros(1, num_chromosomes); % 0.00001 % t_star aus permuation_test


%% Chromosomewise plot of original p-values
start_point = 1;
subplot(disease_count, 2, 2*(disease_count - 1) + 1)

color_line_odd = [0, 0, 0.5];
color_line_even = [0.4, 0.4, 0.8];
color_line = [0, 0.8, 0];
plot_title = 'Raw p-value thresholding';
plt_marker = '.';

for chromo = chrom_to_plot
	result_chromo_file = [results_dir, 'result_combi_chromo_', int2str_leading_zeros(chromo, 2), '.mat'];
	data = load(result_chromo_file, 'p_values', 'p_values_all');
	p_values_reference = data.p_values_all;
	p_values_main = data.p_values_all;
	num_p_values = size(p_values_reference, 2);

	if (mod(chromo, 2))
		color_line_current = color_line_odd;
	else
		color_line_current = color_line_even;
	end
	plot(start_point:(start_point + num_p_values - 1), ...
		-log10(p_values_main), ...
		'.', ...
		'color', color_line_current);
	hold on;
	seq = start_point:(start_point + num_p_values - 1);
	plt_limit = 5;
	plot(seq(-log10(p_values_main) >= plt_limit), ...
		-log10(p_values_main(-log10(p_values_main) >= plt_limit)), ...
		plt_marker, ...
		'Linewidth', 2, ...
		'color', color_line);
	text(start_point + (num_p_values / 2), -1, num2str(chromo), 'rotation', 270);

	set(gca,'color', 'none')
	if ((disease_count == 7) && (chromo == 7)) % HACK
		text(start_point, -5, 'Chromosome');
	end
	start_point = start_point + num_p_values;
end
line([0, start_point], [5, 5], 'Color', 'k', 'Linewidth', 0.5, 'Linestyle', '--');

axis([0, start_point, 0, 15])
set(gca, 'xtick', []);
set(gca, 'box', 'off')
set(gca, 'TickDir', 'out')
hold off;
if (disease_count == 1)
	title(plot_title, 'FontSize', 20);
end
ylabel('-log_{10}(P)');
xlabel('Chromosome-wise position of SNPs');


text(-150000, 7.5, diseases{disease_count}, 'FontSize', 12, 'FontWeight', 'bold');



%% Chromosomewise plot of SVM + Filter Screening p-values
start_point = 1;
subplot(disease_count, 2, 2*(disease_count - 1) + 2)

color_line_odd = [0, 0, 0.7];
color_line_even = [0.4, 0.4, 0.8];
color_line = [0, 0.8, 0];
plot_title = 'COMBI method';
plt_marker = '.g';

for chromo = chrom_to_plot
	result_chromo_file = [results_dir, 'result_combi_chromo_', int2str_leading_zeros(chromo, 2), '.mat'];
	data = load(result_chromo_file, 'p_values', 'p_values_all');
	p_values_reference = data.p_values_all;
	p_values_main = data.p_values;
	num_p_values = size(p_values_reference, 2);

	if (mod(chromo, 2))
		color_line_current = color_line_odd;
	else
		color_line_current = color_line_even;
	end
	plot(start_point:(start_point + num_p_values - 1), ...
		-log10(p_values_main), ...
		'.', ...
		'color', color_line_current);
	hold on;
	set(gca,'color', 'none')
	seq = start_point:(start_point + num_p_values - 1);
	plt_limit = -log10(t_star_all(chromo));
	plot(seq(-log10(p_values_main) >= plt_limit), ...
		-log10(p_values_main(-log10(p_values_main) >= plt_limit)), ...
		plt_marker, ...
		'Linewidth', 2, ...
		'color', color_line);

	if ((disease_count == 7) && (chromo == 7)) % HACK
		text(start_point, -5, 'Chromosome');
	end
	text(start_point + (num_p_values / 2), -1, num2str(chromo), 'rotation', 270);

	line([start_point, start_point + num_p_values - 1], ...
		[-log10(t_star_all(chromo)), -log10(t_star_all(chromo))], ...
		'Color', 'k', ...
		'Linewidth', 0.5, ...
		'Linestyle', '--');

	start_point = start_point + num_p_values;
end

axis([0, start_point, 0, 15])
set(gca, 'xtick', []);
set(gca, 'box', 'off')
set(gca, 'TickDir', 'out')
hold off;
if (disease_count == 1)
	title(plot_title, 'FontSize', 20);
end
ylabel('-log_{10}(P)');
xlabel('Chromosome-wise position of SNPs');

