% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = permtest_buehl_meins(dirname, rep, top_k, alpha_sig_EV, savedir )

%function result = permtest_genome(dirname, rep, C, p, classy, epsilon, filter_window_size, p_pnorm_filter, top_k, alpha_sig, p_feat_scale )

tic;
% PARAMETERS
if ~exist('rep'), rep = 1000; end

load([dirname '/labels.mat']);	
if size(y,1)>size(y,2), y=y'; end

% INITIALIZATION 
num_subj = length(y);

% INITIALIZE EVERYTHING YOU NEED!
%	pvalue_lowest = zeros(rep,top_k);  %FIXME
%	pvalue_all_combi_perm = zeros(rep,top_k);   %FIXME
%	all_pvalues_sorted = zeros(rep,top_k);
%pvalue_all = zeros(rep, num_snps)
num_snps=0;
for chr = 1:22
	chr,	
	data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
	data = data.X;
	num_snps_per_chr(chr) = size(data, 2)/3;
	num_snps = num_snps+size(data, 2)/3;
end

% PERMUTATION LOOP

% TRAINING

for i = 1:rep
	i
	
	pvalue_all_ =[];
	
	% RANDOM train and validation sets
	 
	perm = randperm(num_subj);	 
	ind_train(i,:) = perm(1:ceil(num_subj/2));
	ind_val(i,:) = perm(ceil(num_subj/2)+1:end);
	labels_perm_train(i,:) = y(ind_train(i,:));
	labels_perm_val(i,:) = y(ind_val(i,:));

	for chr = 1:22
		chr
		data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
		data = data.X;

		pvalue_all_ = [pvalue_all_, multiple_testing_chi(data(ind_train(i,:),:) ,labels_perm_train(i,:))];

		clear data ;

	end

	pvalue_all_train(i, :)=pvalue_all_;
	
end

%save('permtest_buehl_meins_after60')
%pre-screening
[foo, ranking] = sort(pvalue_all_train, 2, 'ascend');
indices = ranking(:,1:top_k);

% VALIDATION
% Compute p-values Bonferoni %%%
num_snps_per_chr(num_snps_per_chr==0) = [];
start_points_of_chrs =  cumsum([1,num_snps_per_chr]);

pvalue_all_val = ones(rep, num_snps);

for i = 1:rep
	i
	pvalue_all_ =[];
	indices_now_ = indices(i,:);
	indices_now_ = sort(indices_now_);


	for chr = 1:22
		chr
		indices_now_chr = indices_now_((indices_now_>=start_points_of_chrs(chr))&(indices_now_<start_points_of_chrs(chr+1)));
		
		data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
		data = data.X;
		
		if ~isempty(indices_now_chr)
			indices_now_chr = indices_now_chr - (start_points_of_chrs(chr)-1);
			indices_now_chr = sort([indices_now_chr*3, indices_now_chr*3-1, indices_now_chr*3-2],2); 

			pvalue_all_ = [pvalue_all_, multiple_testing_chi(data(ind_val(i,:),indices_now_chr) ,labels_perm_val(i,:))];
		end
		clear data ;
	end

	pvalue_all_val(i, indices_now_)=min(pvalue_all_* top_k,1);
	
end

%save('permtest_buehl_meins_after93')
pvalue_all_val_sorted = sort(pvalue_all_val);
gammas = 0.05:0.01:1;
for j=1:length(gammas)

	 
	quantiles(j,:)=pvalue_all_val_sorted(max(ceil(rep*gammas(j))-1, 1), :);

	%t_star_FWER = p_lowest_sorted(max(ceil(rep*alpha_sig_FWER)-1, 1));
	%quantiles(j,:)=quantile(pvalue_all_val, gammas(j));
end
max_quantiles = max(quantiles);

pvalues_final = min((1-log(0.05))*max_quantiles,1);
pvalues_final = (1-log(0.05))*max_quantiles;

%save('permtest_buehl_meins_after108')



%%% clear workspace
clearvars foo ranking indices;

	
%%% Find t_star as

t_star_EV = alpha_sig_EV;

clearvars featmat data labels p_lowest_sorted;



% COLLECT RESULTS
result = struct('rep',rep,'pvalues_final', pvalues_final, 'pvalue_all_train', pvalue_all_train,'pvalue_all_val', pvalue_all_val, 't_star_EV', t_star_EV,  'alpha_sig_EV', alpha_sig_EV );


