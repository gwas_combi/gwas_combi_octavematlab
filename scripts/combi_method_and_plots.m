% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [p_values, selected_indices, svm_weights, selected_rs_ids] = ...
	combi_method_and_plots(input_dir, combi_params, results_dir)
% Loads data, runs the COMBI method and generates a Manhatten plot.
%
% Parameters:
%	input_dir:
%		the path to a directory containing genome data
%		in the COMBI methods own Matlab/Octave format (in "*.mat" files);
%		see "save_data.m" & "load_data_plink_binary.m"
%	combi_params:
%		parameters for the COMBI method
%	results_dir:
%		The path to a directory where result data is written to.
%		If not explicitly provided, it is set to `input_dir`.

if ~exist('results_dir', 'var')
	results_dir = input_dir;
end

results_file_prefix = [results_dir, 'result_combi_'];
result_file = [results_file_prefix, 'complete', '.mat'];

if (combi_params.debug_disable_combi_run)
	load(result_file, 'p_values', 'selected_indices', 'svm_weights', 'p_values_all', 'selected_rs_ids');
else
	num_chromosomes = 22;
	p_values = cell(num_chromosomes, 1);
	selected_indices = cell(num_chromosomes, 1);
	selected_rs_ids = cell(num_chromosomes, 1);
	svm_weights = cell(num_chromosomes, 1);
	p_values_all = cell(num_chromosomes, 1);

	if (combi_params.run_per_chromosome)
		% Run the COMBI method separately per each chromosome

		%% load labels
		label_data = load([input_dir, 'labels.mat'], 'affection');
		affection = label_data.affection; clear label_data;
		%individuals = size(affection, 1);

		combi_method_chromo_time = zeros(num_chromosomes, 1);
		for chromo = 1:num_chromosomes
			%% Loading the genotypes for the chromosome (in our own Matlab format)
			data_file = [input_dir, 'chromo_', int2str_leading_zeros(chromo, 2), '.mat'];
			data = load(data_file);
			rs_ident = data.rs_ident;


			%% Prepare/Preprocess data
			alleles = data.alleles;
			num_alleles = size(alleles, 2);
			assert(mod(num_alleles, 2) == 0);
			num_snp = num_alleles / 2;

			[~, alleles] = eventually_reduce_alleles(alleles, combi_params);

			alleles = alleles';


			disp('');
			disp('Creating the feature matrix...');
			featureMatrix_time_id = tic;

			[feature_matrix, encoding_factor] = string_to_featmat(alleles', 'double', combi_params.feature_embedding);
			num_features = size(feature_matrix, 1);

			featureMatrix_time = toc(featureMatrix_time_id);
			disp(['done: Creating the feature matrix in (s): ', num2str(featureMatrix_time)]);
			disp('');


			if ((num_snp > 0) && combi_params.threshold_calibration_permutation_1_used)
				disp('');
				disp('Calculating alpha (1st permutation test)...');
				permTest1_time_id = tic;
				alpha = combi_params.threshold_calibration_alpha;
				[p_value_targets] = permutation_test_phase_1(alleles, affection, combi_params, alpha);
				permTest1_time = toc(permTest1_time_id);
				disp(['done: Calculating alpha (1st permutation test) in (s): ', num2str(permTest1_time)]);
				disp('');
			else
				p_value_targets = combi_params.threshold_calibration_p_value_targets;
			end
			if ((num_snp > 0) && combi_params.threshold_calibration_permutation_2_used)
				disp('');
				disp('Calculating the threshold (2nd permutation test)...');
				permTest2_time_id = tic;
				[thresholds] = permutation_test_phase_2(alleles, affection, combi_params, p_value_targets, feature_matrix, encoding_factor);
				permTest2_time = toc(permTest2_time_id);
				disp(['done: Calculating the threshold (2nd permutation test) in (s): ', num2str(permTest2_time)]);
				disp('');
			else
				thresholds = params.thresholds;
			end


			disp('');
			fprintf('Running the COMBI method for chromosome %d/%d with %d SNPs...\n', chromo, num_chromosomes, num_snp);

			tic;
			[p_values{chromo}, selected_indices{chromo}, svm_weights{chromo}, ~, ~, ~, p_values_all{chromo}] = ...
				combi_method(alleles, affection, combi_params, feature_matrix, encoding_factor);
			combi_method_chromo_time(chromo) = toc;

			selected_rs_ids{chromo} = rs_ident(logical(selected_indices{chromo}));

			fprintf('ran in %fs\n', combi_method_chromo_time(chromo));
		end
		fprintf('total COMBI runtime: %fs\n', sum(combi_method_chromo_time));
	else
		% Run the COMBI method just once, over the whole genome (all chromosomes)

		if (combi_params.debug_disable_combi_run)
			load(result_file, 'p_values', 'selected_indices', 'svm_weights', 'selected_rs_ids');
		else
			%% Loading the genotypes for the whole genome (in our own Matlab format)
			disp('');
			disp('Loading data...');
			data_file = [input_dir, 'complete.mat'];
			data = load(data_file);
			disp('done: Loading data.');
			disp('');


			disp('');
			disp('Preprocessing data...');

			alleles = data.alleles;
			num_alleles = size(alleles, 2);
			assert(mod(num_alleles, 2) == 0);

			affection = data.affection;

			[reduced_snp_indices, alleles] = eventually_reduce_alleles(alleles, combi_params);
			chromos = data.chromos(reduced_snp_indices);
			num_alleles = size(alleles, 2);
			%assert(mod(num_alleles, 2) == 0);
			num_snp = num_alleles / 2;

			alleles = alleles';

			disp('done: Preprocessing data.');
			disp('');


			disp('');
			disp('Creating the feature matrix...');

			[feature_matrix, encoding_factor] = string_to_featmat(alleles', 'double', combi_params.feature_embedding);
			num_features = size(feature_matrix, 1);

			disp('done: Creating the feature matrix.');
			disp('');


			if ((num_snp > 0) && combi_params.threshold_calibration_permutation_1_used)
				disp('');
				disp('Calculating alpha (1st permutation test)...');
				permTest1_time_id = tic;
				alpha = combi_params.threshold_calibration_alpha;
				[p_value_target] = permutation_test_phase_1(alleles, affection, combi_params, alpha);
				permTest1_time = toc(permTest1_time_id);
				disp(['done: Calculating alpha (1st permutation test) in (s): ', num2str(permTest1_time)]);
				disp('');
			else
				p_value_target = combi_params.threshold_calibration_p_value_targets;
			end
			if ((num_snp > 0) && combi_params.threshold_calibration_permutation_2_used)
				disp('');
				disp('Calculating the threshold (2nd permutation test)...');
				permTest2_time_id = tic;
				[threshold] = permutation_test_phase_2(alleles, affection, combi_params, p_value_target, feature_matrix, encoding_factor);
				permTest2_time = toc(permTest2_time_id);
				disp(['done: Calculating the threshold (2nd permutation test) in (s): ', num2str(permTest2_time)]);
				disp('');
			else
				threshold = params.thresholds;
			end


			disp('');
			disp('Running the COMBI method...');

			tic;
			[p_values_genome, selected_indices_genome, svm_weights_genome, ~, ~, ~, p_values_all_genome] = ...
				combi_method(alleles, affection, combi_params);
			combi_method_time = toc;

			%selected_rs_ids_genome = data.rs_ident(selected_indices_genome);

			selected_rs_ids_genome_filter = false(1, num_snp);
			selected_rs_ids_genome_filter(selected_indices_genome) = true;

			%% Split genome data into chromosome cell array
			for chromo = 1:num_chromosomes
				chromo_indices = (chromos == chromo)';
				selected_rs_ids_chromo = (selected_rs_ids_genome_filter & chromo_indices);

				p_values{chromo} = p_values_genome(chromo_indices);
				selected_indices{chromo} = selected_indices_genome(chromo_indices);
				svm_weights{chromo} = svm_weights_genome(chromo_indices);
				p_values_all{chromo} = p_values_all_genome(chromo_indices);
				selected_rs_ids{chromo} = data.rs_ident(selected_rs_ids_chromo);
			end

			disp('done: Running the COMBI method.');
			fprintf('total COMBI runtime: %fs\n', combi_method_time);
			disp('');
		end
	end

	%% Save the result data
	save_result_data(results_file_prefix, p_values, selected_indices, svm_weights, p_values_all, selected_rs_ids);

	if (combi_params.save_results_csv)
		complete_data_csv = cell(0, 2);
		for chromo = 1:num_chromosomes
			result_chromo_file = [results_file_prefix, 'chromo_', int2str_leading_zeros(chromo, 2), '.csv'];

			data_file = [input_dir, 'chromo_', int2str_leading_zeros(chromo, 2), '.mat'];
			chromo_input_data = load(data_file, 'rs_ident');
			chromo_rs_ids = chromo_input_data.rs_ident;
			reduced_snp_indices = eventually_reduce_snp_indices(size(chromo_rs_ids, 1), combi_params);
			chromo_rs_ids = chromo_rs_ids(reduced_snp_indices);

			chromo_p_values = p_values{chromo}';
			chromo_p_values = num2cell(chromo_p_values);

			chromo_data_csv = [chromo_rs_ids, chromo_p_values];
			complete_data_csv = [complete_data_csv(:, :); chromo_data_csv(:, :)];

			externalize(result_chromo_file, chromo_data_csv, '%s,%f\n', 'RS-Id,COMBI-P-Value');
		end
		result_complete_file = [results_file_prefix, 'complete', '.csv'];
		externalize(result_complete_file, complete_data_csv, '%s,%f\n', 'RS-Id,COMBI-P-Value');
	end
end

if (~(combi_params.debug_disable_plotting))
	%% Generate the plot(s)
	disp('');
	disp('Generating plot(s)...');

	manhattan_per_chromosome(results_dir, 1:22, {'myDeseaseName'});

	disp('done: Generating plots.');
	disp('');
end

