% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function alleles_chrom = extract_chrom(alleles, chrom, double_chromos_sorted)
% Extracts genotype data for one chromosome when the full dataset
% of all chromosomes is given.
% Arguments:
%	alleles:               complete genotype data-set in SNP*2 format:
%                          [allele1, allele2,    allele1, allele2,    ... ]
%	chrom:                 index of chromosome to extract
%	double_chromos_sorted: sort(<all the SNPs chromosome indices> * 2)
%                          because we have 2 alleles per chromosome
%
% Return: the chromosomes genotype data-set in SNP*2 format:
%         [allele1, allele2,    allele1, allele2,    ... ]

alleles_chrom = alleles(:, double_chromos_sorted == chrom);

