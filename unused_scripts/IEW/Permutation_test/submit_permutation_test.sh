#$ -S /bin/bash -t 1-22:1 -o /home/bmieth/svn/iew/qsub/wtccc_crohn/logs/job_out.$JOB_ID.$TASK_ID -e /home/bmieth/svn/iew/qsub/wtccc_crohn/logs/job_err.$JOB_ID.$TASK_ID -m e -M bettina.mieth@tu-berlin.de -l matlab=1 -l h_vmem=35G -l mem_free=35G  

pt1='/home/bmieth/svn/iew/scripts/marius/old_scripts/Permutation_test/';
sc='wrapper_permutation_test_IEW.m'
pt2='/home/bmieth/svn/iew/qsub/wtccc_crohn/temp/';

#basescript.m muss enthalten chromo=replacethis1

let rem=$SGE_TASK_ID-1

echo $rem

cmd="sed ${pt1}${sc} -e 's\\replacethis1\\"$rem"\\g'  > ${pt2}runscr_${sc}_"$rem".m" 

echo $cmd
eval $cmd

matlab -nojvm -nosplash -singleCompThread < ${pt2}runscr_${sc}"_"$rem".m" 


