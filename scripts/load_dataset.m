% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function load_dataset(input_dataset, output_dir)
% Imports data from any of the supported external genome data formats
% into the COMBI Matlab/Octave format, if neccessary,
% and runs the COMBI method on it.

dataset_info_file = [output_dir, 'loaded_dataset_info.txt'];

if (exist(dataset_info_file, 'file'))
	loaded_input_dataset = dataset_info_read(dataset_info_file);
	if (loaded_input_dataset.data_hash == input_dataset.data_hash)
		% The requested data was already converted into our own format
		% and stored at this location. Ther is nothing left to do.
		fprintf('This data-set is already imported in location "%s".\n', output_dir);
		return;
	else
		% We delete the old datasets info before starting to load the new data,
		% to ensure we do not end up in an invlaid state,
		% where we would have mixed data (partly from either dataset),
		% but the info file indicating the old dataset.
		delete(dataset_info_file);
	end
end

if (~exist(output_dir, 'file'))
	mkdir(output_dir);
end

if (strcmp(input_dataset.format, 'PLinkBinary'))
	filename_fam = input_dataset.files{1};
	filename_bim = input_dataset.files{2};
	filename_bed = input_dataset.files{3};

	data = load_data_plink_binary(filename_fam, filename_bim, filename_bed);
elseif (strcmp(input_dataset.format, 'PLinkFlat'))
	filename_map = input_dataset.files{1};
	filename_ped = input_dataset.files{2};

	data = load_data_plink_flat(filename_map, filename_ped);
else
	throw(MException('UnknownInputDatasetFormat', ['unkown input format: "', input_dataset.format, '"']));
end
save_data(output_dir, data, 1, 1);
dataset_info_write(dataset_info_file, input_dataset);

