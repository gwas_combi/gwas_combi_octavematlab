% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Randomized and de-randomized p-values based on exact and %
% asymptotic Chi-squared trend tests for (2x3)-tables      %
% here: score tests                                        %
% scores == 'ptrend' => scores = [1, 2, 3]                 %
% scores == 'nptrend' => scores are average ranks          %
%                                                          %
% exact == 1 => exact p-values are computed                %
% exact == 0 => only asymptotic (chi-square based)         %
%               p-values are computed                      %
%                                                          %
% Thorsten Dickhaus, 30.07.2012                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [asympt_p, chisq_value, nonrand_p, rand_p] = ...
                            score_trend_test23(obs, epsilon, scores, exact)

% obs = observations = a 2x3 table

% build marginals = (n1., n2., n.1, n.2, n.3)
marginals = [sum(obs, 2)', sum(obs, 1)];
n = sum(marginals) / 2;

% calculate scores
if(strcmp(scores, 'ptrend'))
   the_scores = [1, 2, 3];
end

if(strcmp(scores, 'nptrend'))
   % average ranks: 
   ave_ranks1 = sum(1:marginals(3)) / marginals(3);  
   ave_ranks2 = sum(marginals(3)+1:marginals(3)+marginals(4)) / marginals(4);
   ave_ranks3 = sum(marginals(3)+marginals(4)+1:n) / marginals(5);
   the_scores = [ave_ranks1, ave_ranks2, ave_ranks3];
end

% calculate Chi-squared value for observed table
y = [repmat(0.0, 1, marginals(1)), repmat(1.0, 1, marginals(2))];
a = [repmat(the_scores(1), 1, obs(1, 1)), repmat(the_scores(2), 1, obs(1, 2)), ...
     repmat(the_scores(3), 1, obs(1, 3)), repmat(the_scores(1), 1, obs(2, 1)), ...
     repmat(the_scores(2), 1, obs(2, 2)), repmat(the_scores(3), 1, obs(2, 3))];

R = corrcoef(y, a);
chisq_value = (n-1) * (R(1, 2))^2;
 
% calculate aymptotic p-value
asympt_p = 1.0 - chi2cdf(chisq_value, 1);

% calculate exact p-values
if(exact == 1)
  x = zeros(2, 3);

  nonrand_p = 0.0;
  random_sum = 0.0;

  dim1 = min(marginals(1), marginals(3));

  %traverse all possible tables with given marginals
  for k=0:dim1
      for l=max(0,marginals(1)-marginals(5)-k):min(marginals(1)-k, marginals(4))
          x(1, 1) = k;
          x(1, 2) = l;
          x(1, 3) = marginals(1) - x(1, 1) - x(1, 2);
          x(2, 1) = marginals(3) - x(1, 1);
          x(2, 2) = marginals(4) - x(1, 2);
          x(2, 3) = marginals(5) - x(1, 3);
          %x
          a_lauf = [repmat(the_scores(1), 1, x(1, 1)), repmat(the_scores(2), 1, x(1, 2)), ...
                    repmat(the_scores(3), 1, x(1, 3)), repmat(the_scores(1), 1, x(2, 1)), ...
                    repmat(the_scores(2), 1, x(2, 2)), repmat(the_scores(3), 1, x(2, 3))];
          R_lauf = corrcoef(y, a_lauf);
          chisq_lauf = (n-1) * (R_lauf(1, 2))^2;      
          if(abs(chisq_lauf - chisq_value) < epsilon)
            random_sum = random_sum + calc_tableprob(x);
          end  
            
          if(chisq_lauf >=  chisq_value)
            nonrand_p = nonrand_p  + calc_tableprob(x);
          end
      end
  end
  u = rand(1);
  rand_p = max(0.0, nonrand_p - u*random_sum);
else
  nonrand_p = -1.0;
  rand_p = -1.0;
end
% End of function 'chisq_exact_p23'. 


% Example from Les Kalish:
% ========================
%
% obs = [19, 31, 67; 1, 5, 21]
% my_eps = eps(1.0)
%
%[ap, ch, np, rp] = score_trend_test23(obs, my_eps, 'ptrend', 1);
%[ch, ap, np, rp]
%
%ans =
%
%    4.5149    0.0336    0.0364    0.0355
% (compare with "Mantel-Haenszel Chi-Square" on page 1 of PDF!!)
%
%
%
%[ap, ch, np, rp] = score_trend_test23(obs, my_eps, 'nptrend', 1);
%[ch, ap, np, rp]
%
%ans =
%
%    4.4063    0.0358    0.0384    0.0368
% (compare with "nptrend" on page 5 of PDF!!) 


