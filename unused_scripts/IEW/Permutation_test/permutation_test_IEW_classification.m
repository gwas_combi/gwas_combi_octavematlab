% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

tic;
%%% load dichotomized labels
load(labels_di_file);
labels_di = y_di; clear y_di;
if size(labels_di, 1)~=1
labels_di = labels_di';
end


%%% load labels
labels = labels_di;
if size(labels, 1)~=1
labels = labels';
end
individuals = length(labels);



%%% load genotypes
data = load([datadir 'chromo_' int2str_leading_zeros(chromo,2) '.mat']);
data = data.X;
assert(mod(size(data,2),3)==0);
num_snp = size(data,2)/3;
load_data_time_ = toc;

%%% Initialize
p_lowest = zeros(1,B);

%%% Calculate feature matrix
featmat = string_to_featmat( data );
featmat = center(featmat);
featmat = scale(featmat,pnorm_feature_scaling);

for i = 1:B

   %%% Show progress
   disp([num2str(round(i*100/B)),'%']);
   
   %%% Permute labels
   labels_perm=zeros(1,length(labels));
   labels_di_perm=zeros(1,length(labels));
   permutation_order = randperm(length(labels));
   labels_perm(permutation_order)=labels;
   labels_di_perm(permutation_order)=labels_di;
   
   %%% SVM training %%%
   res = train_liblinear( featmat, labels_perm,rep, Cs, p_svm);
   w_ = res.w;
   
   %%% filter postprocessing of w %%%
   w_filtered_ = filter_pnorm(w_,filter_window_size,p_pnorm_filter);
   
   %%% compute feature ranking %%%
   [foo, ranking_SVM_filtered_] = sort(w_filtered_, 'descend');
   
   %%% Compute p-values Bonferoni %%%
   pvalue_mtest_= chi_square_goodness_of_fit_test(data,labels_di_perm);

   %%% perform feature screening and compute new p-values %%%
   selector_filtered = ranking_to_selector(ranking_SVM_filtered_,top_k);  
   pvalue_combi_filtered_ = process_pvalues(pvalue_mtest_,selector_filtered);
   
   %%% save lowest p-value
   p_lowest(i)=min(pvalue_combi_filtered_);
   pvalue_combi_filtered_all{i}=pvalue_combi_filtered_;
   
   %%% clear workspace
   clearvars selector_filtered foo labels_perm res w_filtered_ ranking_SVM_filtered_ pvalue_combi_filtered_ pvalue_mtest_ chisq_value call_rate maf lex_min lex_max tables w_;
   
end

%%% Find t_star as alpha-percentile of sorted p-values
p_lowest_sorted = sort(p_lowest);
t_star = p_lowest_sorted(max(ceil(B*alpha_sig)-1, 1));

clearvars featmat data labels;