% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [featmat, encoding_factor] = string_to_featmat(data, return_type, feature_embedding)
%% Encode Alleles (char/String) to double values
% According to a certain encodig rule,
% 'A', 'C', 'G', 'T' are first converted to a binary encoding
% of usually 2 to 4 bits per letter,
% and then pre-processed until they are ready to be used as input for an SVM.
%
% Usage:
%    featmat = string_to_featmat(data, data_type_to_be_returned, ...
%                                feature_embedding)
% Parameters:
%   data:               complete genotype data-set in SNP*2 format:
%                       [allele1, allele2,    allele1, allele2,    ... ]
%   return_type:        single, double, logical, uint8
%   feature_embedding:  genotypic, allelic, nominal
%


%% Initialization

if ~exist('return_type', 'var')
	return_type = 'double';
end

if ~exist('feature_embedding', 'var')
	feature_embedding = 'genotypic';
end

% NOTE It seems that somewhen, somewhere, the data came encoded with 3
%   values per SNP, but as of now, this should never be the case anymore.
has_index = 0;
if (has_index)
	vals_per_snp = 3; %#ok<UNRCH> % per SNP: [allele1, allele2, index]
else
	vals_per_snp = 2; % per SNP: [allele1, allele2]
end


%% Global Parameters

[individuals, num_snp_x] = size(data);
num_snp = num_snp_x / vals_per_snp;


%% Assertions

% Each SNP is encoded as a sturct of `vps` values;
% check if the size of the data reflects that.
assert(mod(num_snp_x, vals_per_snp) == 0, ...
	['invalid ammount of SNP raw values: ', num2str(num_snp_x)]);


%% Main script

data = uint8(data);
data1 = data(:, 1:vals_per_snp:end);
data2 = data(:, 2:vals_per_snp:end);
%num_genotyping_errors = sum(data1 == 48) + sum(data2 == 48);

lexmax_allele_1 = max(data1, [], 1);
lexmax_allele_2 = max(data2, [], 1);
lexmax_overall = max(lexmax_allele_1, lexmax_allele_2);
data1(data1 == 48) = 255;
data2(data2 == 48) = 255;
lexmin_allele_1 = min(data1, [], 1);
lexmin_allele_2 = min(data2, [], 1);
lexmin_overall = min(lexmin_allele_1, lexmin_allele_2);

invalid = find(lexmin_overall == lexmax_overall);
%valid = find(lexmin_overall ~= lexmax_overall);

allele1_lexminor = (data(:, 1:vals_per_snp:end) == repmat(lexmin_overall, [individuals, 1]))';
allele1_lexmajor = (data(:, 1:vals_per_snp:end) == repmat(lexmax_overall, [individuals, 1]))';
allele2_lexminor = (data(:, 2:vals_per_snp:end) == repmat(lexmin_overall, [individuals, 1]))';
allele2_lexmajor = (data(:, 2:vals_per_snp:end) == repmat(lexmax_overall, [individuals, 1]))';

switch feature_embedding
	case 'allelic'
		encoding_factor = 4;
		featmat = false(encoding_factor * num_snp,individuals);
		featmat(1:encoding_factor:end, :) = allele1_lexminor;
		featmat(2:encoding_factor:end, :) = allele1_lexmajor;
		featmat(3:encoding_factor:end, :) = allele2_lexminor;
		featmat(4:encoding_factor:end, :) = allele2_lexmajor;
		featmat((invalid - 1) * encoding_factor + 1, :) = 0;
		featmat((invalid - 1) * encoding_factor + 2, :) = 0;
		featmat((invalid - 1) * encoding_factor + 3, :) = 0;
		featmat((invalid - 1) * encoding_factor + 4, :) = 0;
	case 'genotypic'
		encoding_factor = 3;
		featmat = false(encoding_factor * num_snp,individuals);
		featmat(1:encoding_factor:end, :) = (allele1_lexminor & allele2_lexminor);
		featmat(2:encoding_factor:end, :) = (allele1_lexminor & allele2_lexmajor | allele1_lexmajor & allele2_lexminor);
		featmat(3:encoding_factor:end, :) = allele1_lexmajor & allele2_lexmajor;
		featmat((invalid - 1) * encoding_factor + 1, :) = 0;
		featmat((invalid - 1) * encoding_factor + 2, :) = 0;
		featmat((invalid - 1) * encoding_factor + 3, :) = 0;
	case 'nominal'
		encoding_factor = 1;
		marginals = [ ...
			sum(allele1_lexminor, 2) + sum(allele2_lexminor, 2), ...
			sum(allele2_lexmajor, 2) + sum(allele1_lexmajor, 2)];
		[~, risk_allele] = min(marginals, [], 2); % returns [min, index_min]
		risk_allele_is_lexminor = (risk_allele == 1);
		risk_allele_is_lexmajor = (risk_allele == 2);
		featmat ...
			= (allele1_lexminor + allele2_lexminor) .* repmat(risk_allele_is_lexminor, [1, individuals]) ...
			+ (allele1_lexmajor + allele2_lexmajor) .* repmat(risk_allele_is_lexmajor, [1, individuals]);
		featmat(invalid, :) = 0;
	otherwise
		error('type of feature embedding invalid');
end

clear data1 data2 lexmin_allele_1 lexmin_allele_2 lexmin_overall lexmax_allele_1 lexmax_allele_2 lexmax_overall;


%% Data type to be returned

switch return_type
	case 'single'
		featmat = single(featmat);
	case 'double'
		featmat = double(featmat);
	case 'logical'
		featmat = logical(featmat);
	case 'uint8'
		featmat = uint8(featmat);
end

