% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [prec,rec,auc] = val_PRC(s,Ytr,varargin)

% This function computes the PRC for one repetition and one top_k. It returns coordinates of the curve (prec and rec) and the AUC value.

% val_prc - compute a precision recall curve and area under curve (prc)
%
% Synopsis: 
%   [prec,rec,auc] = val_prc(s,Ytr)
%   [prec,rec,auc] = val_prc(s,Ytr,'property',value)
% 
% Arguments: 
%   s:   scores 
%   Ytr: labels (>=1 for cases, 0 for noncases)
%
% Returns:
%    prec:  an array of precision values
%    rec:  an array of recall values
%    auc: area under curve
% 
% Properties:
%    'ub':   Specify upper bound for recall
%    'lb':   Specify lower bound for recall
% 

opt = propertylist2struct(varargin{:});
opt = set_defaults(opt, 'lb',0, 'ub', 1.0);

% Simplify labels
Ytr(find(Ytr >= 1)) = 1;
Ytr(find(Ytr <= 0)) = 0;

% Determine indices
indp = find(Ytr==1);
indm = find(Ytr==0);

T = sort(s(indp));
k = length(T);

rec = zeros(1,k+2);
prec = zeros(1,k+2);

for j = 1:k
  indc = find(s>=T(j));
  rec(j+1) = sum(Ytr(indc))/length(indp);
  prec(j+1) = sum(Ytr(indc))/(length(indc));
end

rec(1) = 1;
prec(1) = prec(2);
rec(end) = 0;
prec(end) = 1;

fidx_ub = find(rec < opt.ub);
fidx_lb = find(rec > opt.lb);
fidx = intersect(fidx_ub, fidx_lb);

%%% find previous and next element beyond bounds %%%
i1 = max(find(rec >= opt.ub));
i2 = min(find(rec <= opt.lb));

ub_nrec = rec(i1); 
ub_nprec = prec(i1); 
lb_nrec = rec(i2);
lb_nprec = prec(i2);

range = opt.ub - opt.lb;

prec = prec(fidx);
rec = rec(fidx);

%%% add first and last approximated value to rec, prec

if ~isempty(fidx)          
  lb.prec = [prec(end) lb_nprec];
  lb.rec = [rec(end) lb_nrec];

  ub.prec = [prec(1) ub_nprec];
  ub.rec = [rec(1) ub_nrec];
else
  % there is no data point within interval bound
  lb.prec = [ub_nprec lb_nprec];
  lb.rec = [ub_nrec lb_nrec];

  ub.prec = [lb_nprec ub_nprec];
  ub.rec = [lb_nrec ub_nrec];
end

[fprec, lprec] = approx_ROC(lb, ub, opt);
prec(2:end+1) = prec;	
rec(2:end+1) = rec;

prec(1) = lprec;
rec(1) = opt.ub;
prec(end+1) = fprec;
rec(end+1) = opt.lb;

%Stützstellen das Treppe entsteht...
bla=rec(2:end)+0.001;
rec=sort([bla rec], 'descend');

prec = vertcat(prec,prec);
prec = prec(:)'; 
prec=prec(1:end-1);

% AUC berechnen
auc = -diff(rec)*prec(1:end-1)' / range;
%%% returns first and last prec additionally added to prec list
function [fprec, lprec] = approx_ROC(lb, ub, opt)

  %%% upper bound approximation %%%
  m = (ub.prec(2) - ub.prec(1)) / (ub.rec(2) - ub.rec(1));
  lprec = m*opt.ub + ub.prec(1) - m*ub.rec(1);

  %%% lower bound approximation %%%
  m = (lb.prec(1) - lb.prec(2)) / (lb.rec(1) - lb.rec(2));
  fprec = m*opt.lb + lb.prec(2) - m*lb.rec(2); 
