% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function externalize(save_file, data, row_format, header_line)
% Saves a data cell array matrix in plain-text/csv format into a file.


%% init
fprintf('saving data to file "%s" ...\n', save_file);


%% using 'save()' (NOTE can not be used with cell/string data)
%save_options_big_data_struct ...
%	= create_save_big_options(save_file, '-ascii', '-double', '-tabs', 'data');
%save(save_options_big_data_struct{:});


%% using 'dlmwrite()' (NOTE can not be used with cell/string data)
%dlmwrite(save_file, data, 'delimiter', ',');


%% using 'csvwrite()' (NOTE can not be used with cell/string data)
%csvwrite(save_file, data);


%% using 'fprintf()'
[nRows, ~] = size(data);
save_file_id = fopen(save_file, 'w');
if exist('header_line', 'var')
	fprintf(save_file_id, '%s\n', header_line);
end
for row = 1:nRows
	fprintf(save_file_id, row_format, data{row, :});
end
fclose(save_file_id);


%% finalize
fprintf('done. (saving data to file "%s")\n', save_file);

