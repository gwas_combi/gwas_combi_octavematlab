% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

replacethis1=3;

diseases ={'BipolarDisorder', 'CoronaryArteryDisease', 'CrohnsDisease', 'Hypertension', 'RheumatoidArthritis', 'Type1Diabetes', 'Type2Diabetes'};
disease = diseases{replacethis1+1},

diseases_short = {'BD', 'CAD', 'CD', 'HT', 'RA', 'T1D', 'T2D'};
disease_short = diseases_short{replacethis1+1};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Fix Directories: Please change to yours    	 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

workdir = '/home/bmieth/svn/iew/qsub/wtccc_crohn/scripts/';
addpath(genpath(workdir))
cd(workdir);
addpath('/home/bmieth/svn/iew/scripts/marius');
addpath('/home/bmieth/svn/iew/scripts/marius/computation_scripts');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Variable Directories: Please change to yours   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

savedir = ['/home/bmieth/svn/iew/qsub/wtccc_crohn/results/WTCCC/computation_results/',disease_short,'/genomewide_training/'];
datadir = ['/home/bmieth/svn/iew/data/wtccc/',disease,'/'];
alphadir = ['/home/bmieth/svn/iew/qsub/wtccc_crohn/results/WTCCC/computation_results/',disease_short,'/'];
optimal_top_k_dir = ['/home/bmieth/svn/iew/qsub/wtccc_crohn/results/WTCCC/computation_results/',disease_short,'/'];
optimal_pfs_dir = ['/home/bmieth/svn/iew/qsub/wtccc_crohn/results/WTCCC/computation_results/',disease_short,'/'];


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Main Scripts!   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%
parameters_genomewide;

dirname=datadir;
rep=rep_permtest;
C=C_svm;
p=p_svm;
epsilon=epsilon_svm;
p_feat_scale=pnorm_feature_scaling;

tic;

% LOAD DATA AND KERNEL PRE-COMPUTATION
load([dirname '/labels.mat']);
K = zeros(length(y),length(y));
num_feat = 0;
d_all=0;
for chr = 1:22
chr
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  X = string_to_featmat( data.X );
  [d,foo]=size(X); %same es 3*size(x)?
  d_all = d_all+d;
end

for chr = 1:22
chr
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  X = string_to_featmat( data.X );
  if p_feat_scale<100
    X = scale_new(X,p_feat_scale,d_all);
  end
  assert(mod(size(X,1),3)==0);
  num_feat = num_feat + size(X,1)/3;
  K = K + X'*X;
end
K = addRidge(kscale(K));
sg('set_kernel', 'CUSTOM', K, 'FULL');
sg('init_kernel','TRAIN');



% INITIALIZATION 
num_subj = length(y);
sg('new_svm', classy);
sg('c', C);
sg('svm_use_bias', 1);
sg('svm_epsilon',epsilon);
if size(y,1)>size(y,2), y=y'; end
alpha_true = zeros(num_subj,1);
w_true = zeros(1,num_feat);


% SVM TRAINING ON TRUE LABELS
sg('set_labels','TRAIN', y);
sg('svm_train');
[b,alpha] = sg('get_svm');
alpha_true(alpha(:,2)+1) = alpha(:,1);
  

% COMPUTE SVM WEIGHTS FROM ALPHAS
tic,
idx = 0;
feat_idx = 0;
for chr = 1:22 
chr
  %load and scale data
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  X = string_to_featmat( data.X );
  if p_feat_scale<100
    X = scale_new(X,p_feat_scale, d_all);
  end
  num_feat_ = size(X,1);

  % compute true SVM w from alpha
  w_true(1,feat_idx+1:feat_idx+num_feat_) = (X * alpha_true)';
  feat_idx = feat_idx + num_feat_;
  
end
w_true = abs(w_true(:)) .* ( ones(length(w_true),1) * sum(w_true(:).*w_true(:),1).^(-0.5) );
w_true = squeeze(sum(reshape(w_true.^p,3,length(w_true)/3),1)).^(1/p); 
w_true = filter_pnorm(w_true,filter_window_size,p_pnorm_filter);
time.compute_w = toc;

%%% clear workspace


% CLEAN MEMORY
sg('clean_kernel');




% Try with the things that are different to train_libsvm --> same effect, even stronger 
tic,
idx = 0;
feat_idx = 0;
for chr = 1:22 
chr
  %load and scale data
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  X = string_to_featmat( data.X );
  X = center(X); %!!!!! NEW HERE !!!!!

  if p_feat_scale<100
    X = scale(X,p_feat_scale);
  end
  num_feat_ = size(X,1);

  X=double(X); % !!!!! NEW HERE !!!!!
  % compute true SVM w from alpha
  w_true(1,feat_idx+1:feat_idx+num_feat_) = (X * alpha_true)';
  feat_idx = feat_idx + num_feat_;
  
end
w_true = abs(w_true(:)) .* ( ones(length(w_true),1) * sum(w_true(:).*w_true(:),1).^(-0.5) );
w_true = squeeze(sum(reshape(w_true.^p,3,length(w_true)/3),1)).^(1/p); 
w_true = filter_pnorm(w_true,filter_window_size,p_pnorm_filter);






% Try to multiply alpha with one big X (not 22 small ones)
tic,
idx = 0;
X_complete=[];
for chr = 1:22 

  %load and scale data
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  X = string_to_featmat( data.X );
 % X = center(X); %!!!!! NEW HERE !!!!!

  if p_feat_scale<100
    X = scale(X,p_feat_scale);
  end
  num_feat_ = size(X,1);

 % X=double(X); % !!!!! NEW HERE !!!!!

  X_complete = [X_complete; X];
end

% compute true SVM w from alpha
w_true = (X_complete * alpha_true)';
  
w_true = abs(w_true(:)) .* ( ones(length(w_true),1) * sum(w_true(:).*w_true(:),1).^(-0.5) );
w_true = squeeze(sum(reshape(w_true.^p,3,length(w_true)/3),1)).^(1/p); 
w_true = filter_pnorm(w_true,filter_window_size,p_pnorm_filter);


% X oben dasselbe wie oben?
% string to feamat