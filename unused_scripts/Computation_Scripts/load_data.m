% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function data = load_data(filename)
% Loads Alleles/SNPs from a plain-text file, that looks like this:
% GGTGATG
% GGGGAGG
% GTTGCTG
% GGTGATT

buffsize = 10^9;

txt = textread(filename, '%s', 'delimiter', '\n', 'whitespace', '', 'bufsize', buffsize);
data = char(zeros(length(txt), length(txt{1})));

missing = 0;
for i = 1:length(txt) 
	if (size(data, 2) == length(txt{i}))
		data(i - missing, :) = txt{i};
	else
		warning('missing subject / data point');
		missing = missing + 1;
	end
end

if (data(1, end) ~= ' ')
	data(:, end + 1) = ' ';
end

if (missing > 0)
	fprintf('\n');
	error('  n=%d subjects were missing in the data\n', missing);
end
