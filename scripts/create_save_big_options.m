% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function opts = create_save_big_options(varargin)
% Potentially appends one option (meant for the save() function),
% which ensures we can save more then 2GB data
% on systems that support it (in general, all except Windows 32bit).
% Though, only 64+bit systems support more then 4GB big files.

if (is_octave())
	additional_opts = {};
else
	additional_opts = {'-v7.3'};
end

opts = {varargin{:}, additional_opts{:}};

