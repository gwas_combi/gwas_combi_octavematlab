% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function featmat = string_to_featmat_allelic(data, data_type_to_be_returned, feature_embedding)

if ~exist('data_type_to_be_returned', 'var')
	data_type_to_be_returned = 'double';
end

if ~exist('feature_embedding', 'var')
	feature_embedding = 'binary';
end

%if ~exist('min_maf', 'var')
%	min_maf = 0;
%end


%% Assertions

assert(mod(size(data, 2), 3) ~= 1);
if mod(size(data, 2), 3) == 2
	data(:, end + 1) = ' ';
end

data = uint8(data);


%% Global Parameters

[individuals, num_snp3] = size(data);
num_snp = num_snp3 / 3;


%% Initialization

%matA = (data == 'A');
%matC = (data == 'C');
%matG = (data == 'G');
%matT = (data == 'T');


%% Main script

featmat = zeros(num_snp*4, individuals);
data1 = data(:, 1:3:end);
data2 = data(:, 2:3:end);
num_genotyping_errors_1 = sum(data1 == 48);
num_genotyping_errors_2 = sum(data2 == 48);

num_genotyping_errors = num_genotyping_errors_1 + num_genotyping_errors_2;

data1(data1 == 48) = 255;
data2(data2 == 48) = 255;
min_allele_1 = min(data1, [], 1);
min_allele_2 = min(data2, [], 1);
min_overall = min(min_allele_1, min_allele_2);
max_allele_1 = max(data(:, 1:3:end), [], 1);
max_allele_2 = max(data(:, 2:3:end), [], 1);
max_overall = max(max_allele_1, max_allele_2);
invalid_save = find(min_overall == max_overall);
valid_save = find(min_overall ~= max_overall);

featmat(1:4:end, :) = (data(:, 1:3:end) == repmat(min_overall, [individuals, 1]))';
featmat(2:4:end, :) = (data(:, 1:3:end) == repmat(max_overall, [individuals, 1]))';
featmat(3:4:end, :) = (data(:, 2:3:end) == repmat(min_overall, [individuals, 1]))';
featmat(4:4:end, :) = (data(:, 2:3:end) == repmat(max_overall, [individuals, 1]))';

clear matA matC matG matT data1 data2 min_allele_1 min_allele_2 min_overall max_allele_1 max_allele_2 max_overall;


%% Data type to be returned

marginals = sum(featmat, 2);
switch feature_embedding
	case 'nominal' % das wurde noch nicht dem neuen feature encoding angepasst!
%	featmat = uint8(featmat);
%	marginals_grouped = reshape(marginals, [3, size(featmat, 1)/3]);
%	[~, ind_minor_aux] = min(marginals_grouped,[],1);
%	ind_minor = 3*[0:length(ind_minor_aux) - 1] + ind_minor_aux;
%	featmat = featmat(ind_minor, :)*2 + featmat(2:3:end, :);
	case 'binary'
		% do nothing
	otherwise
		error('Error: The chosen feature embedding is not a valid feature embedding.');
end


%% Data type to be returned

% here you could delete SNPs with very low MAF, but as agreed with Barcelona, we won't delete any, but we will use different test!

%marginals1 = marginals(1:4:end);
%marginals2 = marginals(2:4:end);
%marginals3 = marginals(3:4:end);
%marginals4 = marginals(4:4:end);

%allele_count_max = marginals1 + marginals3;
%allele_count_min = marginals2 + marginals4;

%maf = min(allele_count_max, allele_count_min)/(individuals*2);

%invalid = union(find(maf < min_maf), invalid_save);
invalid = invalid_save;
valid = valid_save;

%maf_cleaned_data = data;
%maf_cleaned_data(:, (invalid - 1)*3 + 1) = 48;
%maf_cleaned_data(:, (invalid - 1)*3 + 2) = 48;

switch feature_embedding
	case 'binary'
		featmat((invalid - 1)*4 + 1, :) = 0;
		featmat((invalid - 1)*4 + 2, :) = 0;
		featmat((invalid - 1)*4 + 3, :) = 0;
		featmat((invalid - 1)*4 + 4, :) = 0;

		marginals1 = marginals(1:4:end);
		marginals2 = marginals(2:4:end);
		marginals3 = marginals(3:4:end);
		marginals4 = marginals(4:4:end);

		assert(sum((marginals1(valid) + marginals2(valid) + marginals3(valid) + marginals4(valid)) == (individuals)*2 - num_genotyping_errors(valid)') == length(valid));
%		error_positions = find((marginals1(valid) + marginals2(valid) + marginals3(valid) + marginals4(valid)) ~= (individuals)*2 - num_genotyping_errors(valid)'); % this are the indices of errors in the valid vector!!!

	case 'nominal' % FIXME this is not yet adjusted to the new feature encoding!
		featmat(invalid, :) = 0;
end


%% Data type to be returned

switch data_type_to_be_returned
	case 'single'
		featmat = single(featmat);
	case 'double'
		featmat = double(featmat);
	case 'logical'
		featmat = logical(featmat);
	case 'uint8'
		featmat = uint8(featmat);
end
