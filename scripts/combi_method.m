% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [p_values_smooth, selected_indices_smooth, weights_smooth, ...
	p_values_raw, selected_indices_raw, weights_raw, ...
	p_values_all, feature_matrix, encoding_factor] ...
	= combi_method(data, labels, combi_parameters, feature_matrix, encoding_factor)
% Run the COMBI-Method and identify the top k best SNPs
% You should consider the *_smooth values as the actual result.
% The *_raw ones are only returned for debug purposes.
% For performance reasons, feature_matrix and encoding_factor are
% in- and out-params, so we can reuse them for consecutive runs.

if size(labels, 1) > size(labels, 2)
	labels = labels';
end

%num_individuals = size(labels, 1);
%num_snp2 = size(data, 2);
%num_snp = num_snp2 / 2; % as we have [allele1, allele2] for each SNP

if (~exist('feature_matrix', 'var'))
	[feature_matrix, encoding_factor] = string_to_featmat(data', 'double', combi_parameters.feature_embedding);
end
num_features = size(feature_matrix, 1);
% we could use this as an alternative
%encoding_factor = num_features / size(data, 1);

debug_output = (combi_parameters.debug_write_intermediate_data && (num_features > 0));

if (debug_output)
	dlmwrite(['featmat_', combi_parameters.feature_embedding, '_raw.txt'], feature_matrix, 'delimiter', ' ');
end

feature_matrix = center_combi(feature_matrix);

if (debug_output)
	dlmwrite(['featmat_', combi_parameters.feature_embedding, '_centered.txt'], feature_matrix, 'delimiter', ' ');
end

pStdDev = combi_parameters.feature_scaling_p_norm;
%feature_matrix = scale(feature_matrix, pStdDev); % exists only in matlab, not octave
feature_matrix = scale_combi(feature_matrix, pStdDev);

if (debug_output)
	dlmwrite(['featmat_', combi_parameters.feature_embedding, '_scaled.txt'], feature_matrix, 'delimiter', ' ');
end

if (~combi_parameters.use_libLinear && ~is_octave())
	warning('COMBI:canNotUseLibSVM', 'libSVM can not be used with Matlab (only with Octave), as its commands (svmtrain, svmpredict, ...) overlap with Matlab built-in functions. Using libLinear instead...'); % FIXME Is there a way to work-around this?
end

%% SVM training
svmTrainingNarrow_time_id = tic;
if (combi_parameters.use_libLinear)
	res = train_liblinear(...
		feature_matrix', ...
		labels, ...
		encoding_factor, ...
		combi_parameters.svm_rep, ...
		combi_parameters.svm_Cs, ...
		combi_parameters.weights_decoding_p_norm, ...
		combi_parameters.svm_classy, ...
		combi_parameters.svm_epsilon, ...
		combi_parameters.using_shogun, ...
		debug_output, ...
		combi_parameters.feature_embedding);
else
	res = train_libsvm(...
		feature_matrix', ...
		labels, ...
		encoding_factor, ...
		combi_parameters.svm_rep, ...
		combi_parameters.svm_Cs, ...
		combi_parameters.weights_decoding_p_norm, ...
		combi_parameters.svm_classy, ...
		combi_parameters.svm_epsilon, ...
		combi_parameters.using_shogun);
end
svmTrainingNarrow_time = toc(svmTrainingNarrow_time_id);
disp(['done: SVM training narrow took (s): ', num2str(svmTrainingNarrow_time)]);
weights_raw = res.w;
if (num_features == 0)
	weights_raw = zeros([1, 0]);
end

%% Postprocessing with Moving Average Filter
weights_smooth = filter_pnorm(weights_raw, combi_parameters.smoothing_filter_window_size, combi_parameters.smoothing_filter_p_norm);

if (debug_output)
	dlmwrite(['w_', combi_parameters.feature_embedding, '_filtered.txt'], weights_smooth, 'delimiter', ' ', 'precision', '%.6f');
end

%% Compute all P-values (Bonferoni)
onlyPValues_time_id = tic;
p_values_all = chi_square_goodness_of_fit_test(data', labels);
onlyPValues_time = toc(onlyPValues_time_id);
disp(['done: only P-Values caluclation took (s): ', num2str(onlyPValues_time)]);

%% Filter the P-values using the SVM weights
num_snp = size(p_values_all, 2);
if (combi_parameters.snps_to_keep_absolute == -1)
	top_k = ceil(num_snp * combi_parameters.snps_to_keep_fraction);
else
	top_k = combi_parameters.snps_to_keep_absolute;
	if (~combi_parameters.run_per_chromosome)
		num_chromosomes = 22;
		top_k = top_k * num_chromosomes;
	end
	top_k = min(top_k, num_snp);
end
[p_values_raw, selected_indices_raw] = ...
	filter_p_values_by_combi_weights(weights_raw, p_values_all, top_k);
[p_values_smooth, selected_indices_smooth] = ...
	filter_p_values_by_combi_weights(weights_smooth, p_values_all, top_k);

