% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function dataset_info = dataset_info_read(input_file)
% Reads the contents of a dataset_info struct into a file.
% See dataset_info_write.m for details.
%
% INPUT:
%   input_file: path to a file where to read the info from

fprintf('reading data-set info from file "%s" ...\n', input_file);

input_file_id = fopen(input_file, 'r');

dataset_info.format = fscanf(input_file_id, 'Format:\t%s\n', 1);
dataset_info.data_hash = fscanf(input_file_id, 'Data-Hash:\t%s\n', 1);
dataset_info.files = fscanf(input_file_id, 'File:\t%s\n');

fclose(input_file_id);

