% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Complete Toy Experiment   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Multiple Testing with no Screening 
% Multiple Testing with Filter Screening
% Multiple Testing with SVM Screening
% Roshan method
% COMBI Method - Multiple Testing with SVM and Filter Screening
% COMBI Method with Single and Multisplit from Wasserman & R�der and B�hlmann & Meinshausen


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Directories: Please change to yours   		 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

workdir = '/home/bmieth/svn/iew/scripts/marius/old_scripts/Toy_experiment/';
cd(workdir);
script_path = '/home/bmieth/svn/iew/scripts/marius/computation_scripts/';
addpath(script_path);
savedir= workdir;
savefile = [savedir, 'toy_experiment.mat'],


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Set Parameters  	      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parameters_complete; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Toy Experiment		%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

toy_experiment;

clearvars -except pvalue_no_screening pvalue_filter_screening pvalue_SVM_screening pvalue_SVM_filter_screening roshan_scores pvalue_SVM_filter_screening_multisplit pvalue_SVM_filter_screening_singlesplit top_k top_k_roshan_1 top_k_roshan_2 alpha_sig rep num_splits svm_rep svm_rep_permtest Cs use_seed seed use_scaling use_filter pnorm_feature_scaling filter_window_size p_pnorm_filter classy filter_window_size_mtest p_pnorm_filter_mtest p_svm svm_epsilon rep_inform inform_snps noise_snps individuals noise_parameter roc_ub prc_ub max_plot t_vector gammas_multisplit num_snps savefile workdir

save(savefile);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Evaluation / Plots		  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

calculate_curves;
generate_plot;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Save Results  			  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clearvars -except pvalue_no_screening pvalue_filter_screening pvalue_SVM_screening pvalue_SVM_filter_screening roshan_scores pvalue_SVM_filter_screening_multisplit pvalue_SVM_filter_screening_singlesplit top_k top_k_roshan_1 top_k_roshan_2 alpha_sig rep num_splits svm_rep svm_rep_permtest Cs use_seed seed use_scaling use_filter pnorm_feature_scaling filter_window_size p_pnorm_filter classy filter_window_size_mtest p_pnorm_filter_mtest p_svm svm_epsilon rep_inform inform_snps noise_snps individuals noise_parameter roc_ub prc_ub max_plot t_vector gammas_multisplit num_snps savefile rec fp prec_no_screening prec_filter_screening prec_SVM_screening prec_SVM_filter_screening prec_roshan prec_SVM_filter_screening_multisplit prec_SVM_filter_screening_singlesplit tp_no_screening tp_filter_screening tp_SVM_screening tp_SVM_filter_screening tp_roshan tp_SVM_filter_screening_multisplit tp_SVM_filter_screening_singlesplit FWER_no_screening FWTPR_no_screening FWER_filter_screening FWTPR_filter_screening FWER_SVM_screening FWTPR_SVM_screening FWER_SVM_filter_screening FWTPR_SVM_filter_screening FWER_SVM_filter_screening_multisplit FWTPR_SVM_filter_screening_multisplit FWER_SVM_filter_screening_singlesplit FWTPR_SVM_filter_screening_singlesplit

save(savefile);

return;