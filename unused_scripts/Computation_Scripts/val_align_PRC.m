% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function precs = val_align_PRC(prc, recs)

% This function aligns the given coordinates to a smooth nice curve.

% val_align_prc - compute an "aligned" prc-curve over several trials
%
% Synopsis:
%   precs = val_align_prc(prc, recs)
%
% Arguments:
%   prc:  a prc-curve structure array ...
%         .prec   precision vector
%         .rec   recall vector
%   recs:  a vector of recall "sample points"
%
% Returns:
%   precs:  a matrix of precision values at "sample points"
%
% Description: 
%   val_align_prc aligns several prc curves, possibly of different length,
%   obtained from different experiments. The prc curves are assumed to be
%   stored as structure array with fields .prec and .rec. Alignment is
%   performed for a vector of "sample points" providing the recall
%   values, for which the precision values are searched for in all prc
%   curves. The result is a [length(prc), length(recs)] matrix containing in
%   colums precision from all experiment for a given recall
%   value.
%
% See also:
%   val_prc

precs = zeros(length(prc), length(recs)); % pre-allocate memory

for a = 1:length(prc)
	preca = prc(a).prec;
	reca = prc(a).rec;
	for t = 1:length(recs)
		j = find(reca >= recs(t));
		if ~isempty(j)
			[~, max_idx] = min(reca(j)); % returns: [b, max_idx]
			max_idx = j(max_idx);
		else
			[~, max_idx] = max(reca); % returns: [m, max_idx]
		end

		j = find(reca <= recs(t));
		if ~isempty(j)
			[~, min_idx] = max(reca(j)); % returns: [b, min_idx]
			min_idx = j(min_idx);
		else
			[~, min_idx] = min(reca); % returns: [m, min_idx]
		end

		if reca(max_idx) ~= reca(min_idx)
			dy = preca(max_idx) - preca(min_idx);
			dx = reca(max_idx) - reca(min_idx);
			s = dy / dx;
			precs(a, t) = s * (recs(t) - reca(min_idx)) + preca(min_idx);
		else
			precs(a, t) = preca(max_idx);
		end
	end
end
