% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Directions  	  	      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

script_path = '/home/bmieth/svn/iew/scripts/marius/computation_scripts';
addpath(script_path);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Parameters (flexible)  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rep = 10, % 1000
use_seed = 1; % Soll Seed verwendet werden? 0 oder 1
seed = 13;
use_scaling = 1; % Soll Feature Scaling verwendet werden? 0 oder 1
pnorm_feature_scaling = 6; % 0.5, 1, 2, 10, 20, Inf =no scaling
rep_permtest = 3;
C = 0.0022;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Parameters (fixed)     %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rep_inform = 10;
inform_snps = 20;   %15-20
noise_snps = 10000;    %10,000, even
individuals = 300;  %100-300
noise_parameter = 6;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Toy Experiment  	      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%
%%  Data Extraction  %%%
%%%%%%%%%%%%%%%%%%%%%%%%

generate_small_crohn(individuals,inform_snps,noise_snps,rep_inform);
srcfile_inform = '/home/bmieth/svn/iew/scripts/marius/tmp/crohn_informative.txt';
srcfile_noise = '/home/bmieth/svn/iew/scripts/marius/tmp/crohn_noise.txt';



%%%%%%%%%%%%%%%%%%%%%%%
%%  Initialization  %%%
%%%%%%%%%%%%%%%%%%%%%%%

num_snps = inform_snps + noise_snps;

if use_seed == 1
 rand('seed',seed);
 randn('seed',seed);
end

%%%%%%%%%%%%%%%%
%%  Cascade  %%%
%%%%%%%%%%%%%%%%

tic;


for i = 1:rep
i
	a = [1.1:0.1:1.1+(inform_snps-1)*0.1].^([3.1:0.1:3.1+(inform_snps-1)*0.1]);   
    param = dirichletrnd(a);
	param =  permute_matrix(param,1:length(param),randperm(length(param)),2);
	
	%%% Data Generation %%%
	tic; 
	inform_data = load_data(srcfile_inform);
	offset = 3 * round(inform_snps*(rep_inform-1)*rand);
	inform_data = inform_data(:,offset+1:offset+3*inform_snps);
	
	noise_data = load_data(srcfile_noise);
	data = [noise_data(:,1:noise_snps*3/2) inform_data noise_data(:,noise_snps*3/2+1:noise_snps*3)];
	labels = generate_toy_labels(inform_data,noise_data,param, noise_parameter);
	time.data_generation(i) = toc;
	
	%%% string data to feature_matrix %%%
	tic;
	featmat = string_to_featmat( data );
	featmat = double(featmat);
	featmat = center(featmat);
	if use_scaling==1
		featmat_improved = scale(featmat,pnorm_feature_scaling);
	end
  
  
	X = featmat_improved;
	y = labels;
	clear featmat_improved labels;
	fprintf('\n');

	fprintf('\n LIBMSVM\n');
	result = permtest_libsvm(X, y, rep_permtest, C, 2, 'LIBSVM');
	time_libsvm(i)=result.time;
	fprintf('\n SVMLIGHT\n');
	result = perm_test_svmlight(X, y, rep_permtest, C, 2, 'SVMLIGHT');
	time_svmlight(i)=result.time;
	fprintf('\n LIBLINEAR\n');
	result = perm_test_liblinear(X, y, rep_permtest, C, 2);
	time_liblinear(i)=result.time;

end

plot(sort(time_svmlight),'.'); hold on;
plot(sort(time_libsvm),'.k'); hold on;
plot(sort(time_liblinear),'.c'); hold off;
legend('time-svmlight', 'time-libsvm','time-liblinear')

return




% without data generation (loading files instead)
rep_permtest = 3;
C = 0.0022;

for i = 1:10

	featfile = ['/home/bmieth/svn/iew/scripts/marius/featmat_' int2str(i) '.mat'];
	labfile = ['/home/bmieth/svn/iew/scripts/marius/labels_' int2str(i) '.mat'];
	load(featfile);
	load(labfile);
	X = featmat_improved;
	y = labels;
	clear featmat_improved labels;
	fprintf('\n');

	fprintf('\n SVMLIGHT\n');
	result = perm_test_svmlight(X, y, rep_permtest, C, 2, 'SVMLIGHT');
	fprintf('\n LIBMSVM\n');
	result = perm_test_svmlight(X, y, rep_permtest, C, 2, 'LIBSVM');
	fprintf('\n LIBLINEAR\n');
	result = perm_test_liblinear(X, y, rep_permtest, C, 2);
	
end




rep = 10;
Cs = logspace(-5,5,11);
classy = 'SVMLIGHT';
%classy = 'LIBSVM';

for i = 1:10

	featfile = ['/home/bmieth/svn/iew/scripts/marius/featmat_' int2str(i) '.mat'];
	labfile = ['/home/bmieth/svn/iew/scripts/marius/labels_' int2str(i) '.mat'];
	load(featfile);
	load(labfile);
	X = featmat_improved;
	y = labels;
	clear featmat_improved labels;
	fprintf('\n');

	%result = train_liblinear(X, y, rep, Cs);
	result = train_svmlight(X, y, rep, Cs, 2, classy);
	
end
