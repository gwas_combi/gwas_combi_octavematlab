% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function wnew = filter_pnorm(w, k, p)
% Filters data in w with windowsize k (which has to be an odd number),
% using p-norm p.
% method from:
% http://www.mathworks.com/matlabcentral/fileexchange/12276-movingaverage-v3-1-mar-2008

%% Error checking

assert(k/2 ~= round(k/2), 'Window length has to be an odd number');

if (size(w, 1) == 1 || size(w, 2) == 1)
	w = w(:)';
end

if (k == 1)
	wnew = w;
	return;
end

if (p == 1)
	%N = size(w, 1);

	%% Smooth the edges but with the first and last element intact (haut nicht hin!)
	%Nsumedge = (1:2:(k - 2));
	%wnew1 = w(1:(k - 2));
	%wnew2 = flipud(w(N - (k - 2) + 1:N ));
	%wnew1 = cumsum(wnew1, 1);
	%wnew2 = cumsum(wnew2, 1);
	%wnew1 = wnew1(1:2:(k - 2)) ./ Nsumedge;
	%wnew2 = wnew2(1:2:(k - 2)) ./ Nsumedge;

	%% Recursive moving average method
	% With CUMSUM trick copied from RUNMEAN bwnew Jos van der Geest (12 mar 2008)
	wnew = [zeros(1, (k - 1)/2 + 1), w, zeros(1, (k - 1)/2)];
	wnew = cumsum(wnew);
	wnew = wnew(k + 1:end) - wnew(1:end - k);
	wnew = wnew ./ k;

	%% Sets the smoothed edges:
	%wnew(1:(k - 1)/2) =         wnew1;
	%wnew(N - (k - 1)/2 + 1:N) = flipud(wnew2);
else
	w = w .^ p;
	%N = size(w, 1);

	%% Smooth the edges but with the first and last element intact (haut nicht hin!)
	%Nsumedge = (1:2:(k - 2));
	%wnew1 = w(1:(k - 2));
	%wnew2 = flipud(w(N - (k - 2) + 1:N));
	%wnew1 = (cumsum(wnew1, 1)) .^ (1/p);
	%wnew2 = (cumsum(wnew2, 1)) .^ (1/p);
	%wnew1 = wnew1(1:2:(k - 2)) ./ (Nsumedge .^ (1/p));
	%wnew2 = wnew2(1:2:(k - 2)) ./ (Nsumedge .^ (1/p));
	%% Recursive moving average method
	% With CUMSUM trick copied from RUNMEAN bwnew Jos van der Geest (12 mar 2008)
	wnew = [zeros(1, (k - 1)/2 + 1), w, zeros(1, (k - 1)/2)];
	wnew = cumsum(wnew);
	wnew = (wnew(k + 1:end) - wnew(1:end - k)) .^ (1/p);
	wnew = wnew ./ (k .^ (1/p));
	%% Sets the smoothed edges:
	%wnew(1:(k - 1)/2) =         wnew1;
	%wnew(N - (k - 1)/2 + 1:N) = flipud(wnew2);
end

