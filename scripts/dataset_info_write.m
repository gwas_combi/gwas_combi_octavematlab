% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function dataset_info_write(output_file, dataset_info)
% Writes the contents of a dataset_info struct into a file in plain-text.
%
% INPUT:
%   output_file: path to a file where to write the info to
%   dataset_info: struct containing genome data-set infos:
%           see dataset_info_create.m for details

fprintf('saving data-set info to file "%s" ...\n', output_file);

output_file_id = fopen(output_file, 'w');

fprintf(output_file_id, '%s\t%s\n', 'Format:', dataset_info.format);
fprintf(output_file_id, '%s\t%s\n', 'Data-Hash:', dataset_info.data_hash);
for file_index = 1:length(dataset_info.files)
	file = dataset_info.files{file_index};
	fprintf(output_file_id, '%s\t%s\n', 'File:', file);
end

fclose(output_file_id);

