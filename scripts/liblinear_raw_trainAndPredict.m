% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [weights_normalized, accuracy_validation, accuracy_testing] ...
	= liblinear_raw_trainAndPredict( ...
	X_training,   y_training, ...
	X_validation, y_validation, ...
	X_testing, y_testing, ...
	cost, classifier, epsilon, loglevel, ...
	using_shogun)
% A basic wrapper for libLinear (training),
% that either invokes it through the Shogun Toolbox,
% or directly through the libraries own Matlab/Octave interface.
% using_shogun can be:
% * 0 for using libLinear directly, through its own Matlab/Octave interface
% * 1 to use it through the Shogun Toolbox

if ~exist('using_shogun', 'var')
	using_shogun = 0; % by default: use libLinear directly
end

if (using_shogun)
	sg('loglevel', loglevel); % 'ERROR' or 'ALL'

	%% INITIALIZATION
	sg('new_classifier', classifier);
	sg('svm_use_bias', 1);
	sg('svm_epsilon', epsilon);

	%% TRAINING
	sg('set_features', 'TRAIN', X_training);
	sg('set_labels','TRAIN', y_training);
	sg('c', cost);
	%sg('svm_max_train_time', 60);
	sg('train_classifier');
	[~, w_] = sg('get_classifier'); % returns [bias, weights]
	weights_normalized = abs(w_') / norm(w_);

	%% VALIDATION
	sg('set_features', 'TEST', X_validation);
	out_validation = sg('classify');
	accuracy_validation = 1 - mean(y_validation ~= sign(out_validation));

	%% TESTING
	sg('set_features', 'TEST', X_testing);
	out_testing = sg('classify');
	accuracy_testing = 1 - mean(y_testing ~= sign(out_testing));
else
	libLinear_classifier = liblinear_shogun_to_raw_classifier(classifier);
	libLinear_options = [ ...
		'-s ', num2str(libLinear_classifier), ...
		' -c ', num2str(cost), ...
		' -e ', num2str(epsilon), ...
		' -B ', num2str(1)];

	%% TRAINING
	model = train(y_training, X_training, libLinear_options);
	w_ = model.w;
	weights_normalized = abs(w_') / norm(w_);

	%% VALIDATION
	[out_validation, accuracy_validation_ll, ~] = predict(y_validation, X_validation, model);
	accuracy_validation_self = 1 - mean(y_validation ~= sign(out_validation));

	assert(accuracy_validation_self ~= accuracy_validation_ll, ...
		'The accurracy for validation we calculated ourselfs (%f) differs from the one calculated by libLinear (%f)', ...
		accuracy_validation_self, accuracy_validation_ll);
	accuracy_validation = accuracy_validation_self;

	%% TESTING
	[~, accuracy_testing, ~] = predict(y_testing, X_testing, model);
end

