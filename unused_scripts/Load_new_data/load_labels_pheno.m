% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [y, y_di] = load_labels_pheno(filename_input_pheno)
% Loads affection labels from a '*.pheno' file.

%[~, id, pheno] = textread(filename_input_pheno, '%s%s%s');
% this would replace the next 4 lines,
% but it does not work for all .pheno files
% % y = str2num(cell2mat(pheno))';
% for i = 1:length(pheno)
%	y(i) = str2num(pheno{i});
% end
% y = y';

[~, ~, pheno] = textread(filename_input_pheno, '%s%s%d');
y = pheno';

y_di = zeros(size(y)) - 1;
y_di(y >= median(y)) = 1;
