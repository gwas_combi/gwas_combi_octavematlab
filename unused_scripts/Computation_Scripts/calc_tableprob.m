% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute conditional table probability for a given (RxC)- %
% table under the null hypothesis of independent sampling  %
%                                                          %
% Auxiliary routine for randomized p-values based on       %
% Fisher's exact test                                      %
%                                                          %
% Thorsten Dickhaus, 26.08.2009                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function prob = calc_tableprob(obs)

% obs = observations = a RxC table

%build marginals
marginals = [sum(obs, 2)', sum(obs, 1)];

n = sum(marginals) / 2;

%build log nominator statistic
log_nom = sum(sum(gammaln(marginals+1))) - gammaln(n+1);

%build log denominator statistic
log_denom = sum(sum(gammaln(obs+1)));

%compute table probability
prob = exp(log_nom - log_denom);

% End of function 'calc_tableprob'. 
