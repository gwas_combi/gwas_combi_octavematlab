% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [chosen_snp_indices, alleles_reduced] = eventually_reduce_alleles(alleles, combi_params)
% If desired by the user, make data-size much smaller, for faster testing.
% TODO This should be renamed "eventually" -> "potentially(?)"

num_alleles = size(alleles, 2);
num_snp = num_alleles / 2;

chosen_snp_indices = eventually_reduce_snp_indices(num_snp, combi_params);

if (size(chosen_snp_indices, 2) == num_snp)
	alleles_reduced = alleles;
else
	chosen_snp_indices_doubled = sort([(chosen_snp_indices * 2), (chosen_snp_indices * 2 + 1)]);
	alleles_reduced = alleles(:, chosen_snp_indices_doubled);
end

