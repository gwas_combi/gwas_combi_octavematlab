% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function classifier_raw = liblinear_shogun_to_raw_classifier(classifier_shogun)
% A basic wrapper for libLinear (training),
% that either invokes it through the Shogun Toolbox,
% or directly through the libraries own Matlab/Octave interface.
% PARAMETER classifier_shogun can be:
% * LIBLINEAR_L2R_LR
% * LIBLINEAR_L2R_L2LOSS_SVC_DUAL
% * LIBLINEAR_L2R_L2LOSS_SVC
% * LIBLINEAR_L2R_L1LOSS_SVC_DUAL
% * LIBLINEAR_L1R_L2LOSS_SVC

if (strcmp(classifier_shogun, 'LIBLINEAR_L2R_LR'))
	classifier_raw = 0;
elseif (strcmp(classifier_shogun, 'LIBLINEAR_L2R_L2LOSS_SVC_DUAL'))
	classifier_raw = 1;
elseif (strcmp(classifier_shogun, 'LIBLINEAR_L2R_L2LOSS_SVC'))
	classifier_raw = 2;
elseif (strcmp(classifier_shogun, 'LIBLINEAR_L2R_L1LOSS_SVC_DUAL'))
	classifier_raw = 3;
elseif (strcmp(classifier_shogun, 'LIBLINEAR_L1R_L2LOSS_SVC'))
	classifier_raw = 5;
else
	% libLinear also suports 4, 6 and 7, but we currently do not
	% support them
	classifier_raw = -1;
end

