% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [chosen_snp_indices] = eventually_reduce_snp_indices(num_snp, combi_params)
% If desired by the user, make data-size much smaller, for faster testing.

if (combi_params.debug_reduce_num_snps)
	disp('HACK enabled: Reducing the data-size a lot, for faster testing!');
	if (combi_params.debug_reduce_num_snps_to_absolute == -1)
		num_snp_small = ceil(num_snp * combi_params.debug_reduce_num_snps_to_fraction);
	else
		num_snp_small = combi_params.debug_reduce_num_snps_to_absolute;
	end
	fprintf('Reducing the number of SNPs from %d to %d\n', num_snp, num_snp_small);
	chosen_snp_indices = 1:num_snp_small;
else
	chosen_snp_indices = 1:num_snp;
end

