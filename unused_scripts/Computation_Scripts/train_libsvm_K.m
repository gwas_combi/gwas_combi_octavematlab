% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_libsvm_K(K, y, rep, Cs, p, classy, epsilon)

tic;

%% PARAMETERS
if ~exist('Cs', 'var'), Cs = logspace(-5, 5, 11); end
num_Cs = size(Cs, 1);
if ~exist('rep', 'var')
	if num_Cs == 1,
		rep = 1;
	else
		rep = 30;
	end
end
if ~exist('epsilon', 'var'),
	epsilon = 1e-3;
end
if ~exist('classy', 'var'),
	classy = 'LIBSVM';
	% Can be: 'SVMLIGHT', 'LIBSVM'
end
loglevel = 'ERROR'; % 'ERROR' or 'ALL'


%% INITIALIZATION
num_data = size(y, 1);
sg('new_svm', classy);
sg('svm_use_bias', 1);
if size(y, 1) > size(y, 2), y = y'; end
sg('svm_epsilon', epsilon);
acc_val = zeros(rep, num_Cs);
acc_test = zeros(rep, num_Cs);


%% KERNEL PRE-COMPUTATION
K = addRidge(K);

%% VALIDATION LOOP
if rep > 1 || num_Cs > 1
	% if rep == 1 skip validation loop!

	idx = 0;
	for i = 1:rep
		fprintf('Validation loop, repetition %d / %d\n...', i, rep);
		%% TRAIN / VAL SPLITS
		perm = randperm(num_data);
		divtr = perm(1:floor(num_data / 2));
		divval = perm(floor(num_data / 2) + 1:floor(num_data * 3 / 4));
		divte = perm(floor(num_data * 3 / 4) + 1:end);
		Ktr = K(divtr, divtr);
		Kval = K(divtr, divval);
		Kte = K(divtr, divte);
		ytr = y(divtr);
		yval = y(divval);
		yte = y(divte);

		for j = 1:num_Cs
			C = Cs(j);

			%% SVM TRAINING
			sg('set_labels', 'TRAIN', ytr);
			sg('set_kernel', 'CUSTOM', Ktr, 'FULL');
			sg('init_kernel', 'TRAIN');
			sg('c', C);
			sg('svm_train');
			[b, alpha] = sg('get_svm');

			%% SVM TESTING
			s = b + alpha(:, 1)' * Kval(alpha(:, 2) + 1, :);
			acc_val(i, j) = 1 - mean(yval ~= sign(s));
			s = b + alpha(:, 1)' * Kte(alpha(:, 2) + 1, :);
			acc_test(i, j) = 1 - mean(yte ~= sign(s));

			idx = idx + 1;
			%print_progress(idx, rep * num_Cs);
		end
	end

	%% MODEL SELECTION
	[~, ind] = max(mean(acc_val, 1)); % returns [max, index_max]
	verbose.acc_test = acc_test;
	verbose.acc_test_stderr = std(acc_test, [], 1) / sqrt(rep);
	acc_test_stderr = std(acc_test(:, ind)) / sqrt(rep);
	acc_test = mean(acc_test(:, ind), 1);
	C = Cs(ind);
else
	C = Cs;
end


%% SVM TRAINING ON FULL DATA
sg('set_labels', 'TRAIN', y);
sg('set_kernel', 'CUSTOM', K, 'FULL');
sg('init_kernel', 'TRAIN');
sg('new_svm', classy);
sg('c', C);
sg('svm_train');
[bias, alpha] = sg('get_svm');
sg('clean_kernel');


%% COMPUTE ALPHA
alpha_full = zeros(length(y), 1);
alpha_full(alpha(:, 2) + 1) = alpha(:, 1);


%% COLLECT RESULTS
if rep > 1
	result = struct( ...
		'alpha', alpha_full(:), ...
		'acc_val', acc_val, ...
		'acc_test', acc_test, ...
		'acc_test_stderr', acc_test_stderr, ...
		'verbose', verbose, ...
		'Cs', Cs, ...
		'C', C, ...
		'rep', rep, ...
		'classy', classy, ...
		'epsilon', epsilon, ...
		'time', toc);
else
	result = struct( ...
		'alpha', alpha_full(:), ...
		'Cs', Cs, ...
		'C', C, ...
		'rep', rep, ...
		'classy', classy, ...
		'epsilon', epsilon, ...
		'time', toc, ...
		'b', bias);
end
