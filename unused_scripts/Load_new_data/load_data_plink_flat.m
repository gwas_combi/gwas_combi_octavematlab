% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function genomic_data_set = load_data_plink_flat(filename_map, filename_ped)
% Loads SNPs, labels and some meta-data from the 2 PLink (Flat) files,
% and stores them in a struct.

error('Loading from PLink Flat is not yet implemented! Please use the native "plink" utility or GWASpi to convert your files to PLink Binary'); % TODO This file is still mostly a copy of load_data_plink_binary.m, and thus needs to be implemented, basically from scratch



num_ind = count_lines(filename_ped); % number of individuals/samples
num_snp = count_lines(filename_map); % number of SNPs/markers


%% Load SNP position meta-data

fprintf('\nParsing PLink (Flat) data, reading from %s ...\n', filename_ped);

% .fam-file is simply the first six columns of test.ped
% the six columns are:
% Family ID, Individual ID, Paternal ID, Maternal ID, Sex (1=male; 2=female; other=unknown), Affection (1=affected; 2=unaffected; other=unknown), Phenotypes ...

%skip_sep_tsf = '%*[,\t]';
skip_sep_tsf = ' ';
ped_textscan_format = [ ...
	'%s', skip_sep_tsf, ... % family ID & separator
	'%s', skip_sep_tsf, ... % sample ID & separator
	'%d', skip_sep_tsf, ... % father & separator
	'%d', skip_sep_tsf, ... % mother & separator
	'%d', skip_sep_tsf, ... % sex & separator
	'%d' ...                % affection
	];
for snp_index = 1:num_snp
	ped_textscan_format = [ped_textscan_format, ...
		skip_sep_tsf, '%s' ... % separator & SNP minor allele
		skip_sep_tsf, '%s' ... % separator & SNP major allele
		];
end

ped_textscan_format
''

%[fam_ID, ind_ID, ~, ~, sex, y] = textscan(filename_ped, ped_textscan_format);
ped_fileId = fopen(filename_ped);
ped_content = textscan(ped_fileId, ped_textscan_format);
fclose(ped_fileId);
%ped_content

fam_ID = ped_content{1};
ind_ID = ped_content{2};
%father = ped_content{3};
%mother = ped_content{4};
sex = ped_content{5};
y = ped_content{6};


%return

% .map-file is the extended map file, which also includes the names of the alleles
% the four columns are: chromosome, SNP/RS-Id, default-genetic-distance, position

[chromos, rs_ident, ~, bp_pos] = textread(filename_map, '%d%s%d%d');


%% Load subjects

% The bed-file is to be interpreted the following way:
% First 8-Bit:  1101100 = 108 = first magic number (not important)
% Second 8-Bit: 11011 = 27 = second magic number (not important)
% Third 8-Bit:  1 = 1 = mode (1 = SNP-major (i.e. list all individuals for first SNP, all individuals for second
%               SNP, etc), 0 = individual-major (i.e. list all SNPs for the first individual, list all SNPs for
%               the second individual, etc))
% Following 8-Bits code for a maximum of four genotypes/ read backwards!
%               00  Homozygote "1"/"1"
%               01  Heterozygote
%               11  Homozygote "2"/"2"
%               10  Missing genotype
% 11111111 = 255 = 4*homozygot major

% Load file
fprintf('reading raw data from file "%s" ...\n', filename_bed);
fid = fopen(filename_bed, 'rb');
bed_data_long = fread(fid, 'uint8');
fclose(fid);


% Cut off the first 3 bytes
bed_data_short = bed_data_long(4:end);

clear bed_data_long;

part_size_limit = 1000000;
num_parts = ceil(size(bed_data_short, 1) / part_size_limit);
bed_data_bin = false(size(bed_data_short, 1), 8);
for pi = 1:num_parts
	fprintf('calculate the binary representation of the SNPs, part %d / %d ...\n', ...
		pi, num_parts);
	part_start = (pi - 1)*part_size_limit + 1;
	part_end = pi*part_size_limit;
	part_end = min(part_end, size(bed_data_short, 1));
	bed_data_short_part = bed_data_short(part_start:part_end);
	% Turn into the binary representation
	bed_data_bin_char_part = dec2bin(bed_data_short_part);
	%bed_data_bin_part = logical(str2double(bed_data_bin_char_part));
	bed_data_bin_part = false(size(bed_data_bin_char_part, 1), 8);
	bed_data_bin_part(bed_data_bin_char_part == '1') = true;
	% Flip data from left to right, because bits are read from back to front
	bed_data_bin_part = fliplr(bed_data_bin_part);
	%bed_data_dec = [bed_data_dec; bed_data_dec_];
	bed_data_bin(part_start:part_end, :) = bed_data_bin_part(:, :);
end

clear bed_data_short bed_data_dec_ start ende num_parts;

% %% Turn into the binary representation
%bed_data_short = dec2bin(bed_data_short);

%% Flip data from left to right, because bits are read from back to front
%bed_data_short = fliplr(bed_data_short);

%% Reshape into num_ind*2 x num_snp Matrix
m = num_ind*2;
n = num_snp;
p = 2;
q = 1;

bed_data_bin_new = reshape(bed_data_bin', [], n);
bed_data_bin_new = bed_data_bin_new(1:2*num_ind, :);

clear bed_data_dec;

part_size_limit = 500;
num_parts = ceil(num_ind / part_size_limit);
bed_data_letter = char(zeros(m, n)); % pre-allocate memory for performance
%bed_data_letter = false(m, n); % pre-allocate memory for performance
for pi = 1:num_parts
	fprintf('calculate many things of individuals, part %d / %d ...\n', ...
		pi, num_parts);
	part_start = (pi - 1)*part_size_limit*2 + 1;
	part_end = pi*part_size_limit*2;
	part_end = min(part_end, num_ind*2);

	num_ind_now = (part_end - part_start + 1) / 2;

	allele1_mat = repmat(allele1, 2*num_ind_now, 1);
	allele2_mat = repmat(allele2, 2*num_ind_now, 1);

	bed_data_bin_new_part = bed_data_bin_new(part_start:part_end, :);
	bed_data_char_new_part = char(zeros(size(bed_data_bin_new_part)));
	bed_data_char_new_part(bed_data_bin_new_part == false) ...
		= allele1_mat(bed_data_bin_new_part == false);
	bed_data_char_new_part(bed_data_bin_new_part == true) ...
		= allele2_mat(bed_data_bin_new_part == true);

	missing_mat = allele1_mat;
	missing_mat(1:2:end, :) = allele2_mat(1:2:end, :);

	bla = (bed_data_char_new_part == missing_mat);
	bla = bla(1:2:end, :) & bla(2:2:end, :);

	%bla2 = zeros(2*num_ind_now, num_snp);
	bla2(1:2:2*num_ind_now, 1:num_snp) = bla;
	bla2(2:2:2*num_ind_now, 1:num_snp) = bla;

	bed_data_char_new_part(logical(bla2)) = '0'; % TODO this should already be logical, so the 'logical()' operator should not be required
	clear bla2;
	bed_data_letter(part_start:part_end, :) = bed_data_char_new_part(:, :);
end

clear bed_data_dec_new allele1_mat allele2_mat bed_data_dec_new_ missing_mat bla bla2 ende i num_ind_now num_parts;

%% Reshape into num_ind x 2*num_snp Matrix
bed_data_letter = reshape(bed_data_letter, [p, m/p, q, n/q]);
bed_data_letter = permute(bed_data_letter, [3, 2, 1, 4]);

X = reshape(bed_data_letter, [q*m/p, p*n/q]);

clear m n p q bed_data_letter;


%% Insert spaces
%part_size_limit = 500;
%num_parts = ceil(num_ind / part_size_limit);
%X = [];
%for pi = 1:num_parts

	%disp(pi);
	%part_start = (i - 1)*part_size_limit + p1;
	%part_end = pi*part_size_limit;
	%part_end = min(part_end, num_ind);

	%num_ind_now = (part_end - part_start + 1);

	%X_ = zeros(num_ind_now, 3*num_snp);
	%X_(:, 1:3:3*num_snp)=bed_data_letter(part_start:part_end,1:2:end);
	%X_(:, 2:3:3*num_snp)=bed_data_letter(part_start:part_end,2:2:end);
	%X_(:, 3:3:3*num_snp)=' ';
	%X{pi}=X_;
	%X= [X; X_];
	%clear X_;
%end

%X= cell2mat(X');

%X= [X{1}; X{2}; X{3}; X{4}; X{5}; X{6}; X{7}; X{8}; X{9}; X{10}];
%clear X_ bed_data_letter;

%X= char(X);

y = y';

% PLink labels come as either 2 or 1,
% while we want them as either 1 or -1,
% so we convert (1 -> -1) and (2 -> 1).
% We make sure it is all integer, and no floating point math.
y = (2 * y) - 3;
sex = (2 * sex) - 3;


%% Output

genomic_data_set.alleles = X; % 2 alleles (minor, major) per SNP
genomic_data_set.affection = y; % 1 label per individual (0: unaffected, 1: affected)
genomic_data_set.chromos = chromos; % 1 chromosome index (a number from 1 to 22) per SNP
genomic_data_set.rs_ident = rs_ident; % 1 RS ID per SNP
genomic_data_set.bp_pos = bp_pos; % 1 ??BP?? position per SNP
genomic_data_set.sex = sex;
genomic_data_set.fam_ID = char(fam_ID);
genomic_data_set.ind_ID = char(ind_ID);

fprintf('\ndone parsing PLink Binary data.\n\n');
