% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function x = gammarnd(a)
% Produces a single random deviate from the Gamma
% distribution with mean a and variance a.

reject = true;
while reject
	y0     = log(a) - 1 / sqrt(a);
	c      = a - exp(y0);
	y      = log(rand) .* sign(rand - 0.5)/c + log(a);
	f      = a*y - exp(y) - (a*y0 - exp(y0));
	g      = c*(abs((y0 - log(a))) - abs(y - log(a)));
	reject = (log(rand) + g) > f;
end

x = exp(y);
