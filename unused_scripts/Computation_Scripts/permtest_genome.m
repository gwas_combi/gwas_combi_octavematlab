% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = permtest_genome(dirname, rep, C, p, classy, epsilon, filter_window_size, p_pnorm_filter, top_k, alpha_sig_EV, p_feat_scale )

%function result = permtest_genome(dirname, rep, C, p, classy, epsilon, filter_window_size, p_pnorm_filter, top_k, alpha_sig, p_feat_scale )

tic;

% PARAMETERS
if ~exist('rep'), rep = 1000; end
if ~exist('C'), C = 1; end
if ~exist('p'),  p = 2; end
if ~exist('p_feat_scale'),  p_feat_scale = 4; end
if ~exist('epsilon'), 
  epsilon = 1e-3;
end
if ~exist('classy'), 
  classy = 'LIBSVM';
  % Can be: 'SVMLIGHT', 'LIBSVM'
end
%sg('loglevel', 'ERROR');     % 'ERROR' or 'ALL'

% Identify overall number of features
d_all=0;
for chr = 1:22
chr
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  [foo,d]=size(data.X); % is the same as size(X) where X is feature matrix
  d_all = d_all+d;
end

% LOAD DATA AND KERNEL PRE-COMPUTATION
load([dirname '/labels.mat']);
K = zeros(length(y),length(y));
num_feat = 0;
for chr = 1:22
chr
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  X = string_to_featmat( data.X );
  X = center(X);
  if p_feat_scale<100
    X = scale(X,p_feat_scale,d_all);
  end
  assert(mod(size(X,1),3)==0);
  num_feat = num_feat + size(X,1)/3;
  X_all{chr}=X;
  K = K + X'*X;
end
K = addRidge(kscale(K));
sg('set_kernel', 'CUSTOM', K, 'FULL');
sg('init_kernel','TRAIN');
clear data X;



% INITIALIZATION 
num_subj = length(y);
sg('new_svm', classy);
sg('c', C);
sg('svm_use_bias', 1);
sg('svm_epsilon',epsilon);
if size(y,1)>size(y,2), y=y'; end
indices =  zeros(rep,top_k);
alpha_full = zeros(num_subj,rep);
alpha_true = zeros(num_subj,1);
labels_perm = zeros(rep,num_subj);
w = zeros(rep,top_k*22);
pvalue_lowest = zeros(rep,top_k);  %FIXME
pvalue_all_ = zeros(rep,top_k*22);   %FIXME
pvalue_all = zeros(rep,top_k);
w_all = zeros(rep,top_k);
w_true = zeros(1,num_feat);



% PERMUTATION LOOP
idx = 0;
tic, 
for i = 1:rep

	% RANDOM LABELS
	%fprintf('\n Genereate random labels\n');
	tic,
	perm = randperm(num_subj);
	labels_perm(i,:) = y(perm);
	time.gen_labels(i)=toc; 
	
	% SVM TRAINING
	%fprintf('\n Train SVM (only new labels)\n');
	tic,
	sg('set_labels','TRAIN', labels_perm(i,:));
	sg('svm_train');
	time.train_svm(i)=toc;

	% COMPUTE w
	%fprintf('\n Get Results\n');
	tic,
	[b,alpha] = sg('get_svm');
	time.get_results(i)=toc;
	%fprintf('\n Mutiply X with alpha\n');
	tic,
	alpha_full(alpha(:,2)+1,i) = alpha(:,1);
	time.create_full_alpha_X(i)=toc; 
	
end
time.SVM_training = toc;



% SVM TRAINING ON TRUE LABELS
sg('set_labels','TRAIN', y);
sg('svm_train');
[b,alpha] = sg('get_svm');
alpha_true(alpha(:,2)+1) = alpha(:,1);
clear K;

% COMPUTE SVM WEIGHTS FROM ALPHAS
tic,
feat_idx = 0;
for chr = 1:22 
chr
 % X=X_all{chr};
  
  data = load([dirname '/chromo_'  int2str_leading_zeros(chr,2) '.mat']);
  X = string_to_featmat( data.X );
  X = center(X);
  if p_feat_scale<100
    X = scale(X,p_feat_scale,d_all);
  end
  
  num_feat_ = size(X,1);
  data = data.X;

  % compute true SVM w from alpha
  w_true(1,feat_idx+1:feat_idx+num_feat_) = (X * alpha_true)';
  feat_idx = feat_idx + num_feat_;
  
  % compute fake SVM ws from alphas
  w_ = X * alpha_full;
  %fprintf('\n Standardize weights and bring into correct format\n');
  w_ = abs(w_) .* ( ones(num_feat_,1) * sum(w_.*w_,1).^(-0.5) );
  w_ = squeeze(sum(reshape(w_.^p,3,num_feat_/3,rep),1)).^(1/p); 
  w_ = w_';
 
 w_perm_complete = [w_perm_complete; w_]
  % pnorm filtering 
  for i = 1:rep
    w_(i,:) = filter_pnorm(w_(i,:),filter_window_size,p_pnorm_filter)';
  end

  %pre-screening
  [foo, ranking] = sort(w_, 2, 'descend');
  indices = ranking(:,1:top_k);
  indices_ = sort([indices*3, indices*3-1, indices*3-2],2); 
  for i = 1:rep
    pvalue_combi_filtered_ = multiple_testing_chi(data(:,indices_(i,:)) ,labels_perm(i,:));
    %%% save lowest p-value
    pvalue_all_(i,top_k*(chr-1)+1:top_k*chr) = pvalue_combi_filtered_;
	w(i,top_k*(chr-1)+1:top_k*chr) = w_(i,indices(i,:));
  end

end
w_true = abs(w_true(:)) .* ( ones(length(w_true),1) * sum(w_true(:).*w_true(:),1).^(-0.5) );
w_true = squeeze(sum(reshape(w_true.^p,3,length(w_true)/3),1)).^(1/p); 
w_true = filter_pnorm(w_true,filter_window_size,p_pnorm_filter);






%clear data X w_;
time.compute_w = toc;



tic;
[foo, ranking] = sort(w, 2, 'descend');
% Compute p-values Bonferoni %%%
indices = ranking(:,1:top_k);
for i = 1:rep
  pvalue_all(i,:) = pvalue_all_(i,indices(i,:));
  w_all(i,:) = w(i,indices(i,:));
end
p_lowest = min(pvalue_all,[],2);
w_highest = max(w_all,[],2);
time.compute_pvalue = toc;


%%% clear workspace
clearvars selector_filtered foo labels_perm res ranking indices pvalue_combi_filtered_ pvalue_mtest_ chisq_value call_rate maf lex_min lex_max tables w_;

	
%%% Find t_star as alpha-percentile of sorted p-values
p_lowest_sorted = sort(p_lowest);
%t_star_FWER = p_lowest_sorted(max(ceil(rep*alpha_sig_FWER)-1, 1));

%all_pvalues = sort(pvalue_all(:));
all_pvalues_sorted = sort(pvalue_all(:));

t_star_EV = all_pvalues_sorted(max(ceil(rep*alpha_sig_EV)-1, 1));

clearvars featmat data labels p_lowest_sorted;


% SO THE SAME QUANTILE DETERMINATION FOR VANILLA SVM WEIGHTS!
w_lowest_sorted = sort(w_highest, 'descend');
%SVM_t_star_FWER = w_lowest_sorted(max(ceil(rep*alpha_sig_FWER)-1, 1));
w_all_sorted = sort(w_all(:), 'descend');
SVM_t_star_EV = w_all_sorted(max(ceil(rep*alpha_sig_EV)-1, 1));


% COLLECT RESULTS
result = struct('w',w,'C',C,'rep',rep,'classy',classy,'epsilon',epsilon, 'p',p,'p_lowest', p_lowest, 'pvalue_all', pvalue_all, 't_star_EV', t_star_EV, 'w_highest', w_highest, 'w_all', w_all, 'SVM_t_star_EV', SVM_t_star_EV, 'w_true', w_true,'rep',rep, 'alpha_sig_EV', alpha_sig_EV );

% CLEAN MEMORY
sg('clean_kernel');


