% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = permtest_time_new( X,data, y, rep, C, p, classy, epsilon, filter_window_size, p_pnorm_filter, top_k, noise_snps, inform_snps, alpha_sig )

tic;

% PARAMETERS
if ~exist('rep'), rep = 1000; end
if ~exist('C'), C = 1; end
if ~exist('p'),  p = 1; end
if ~exist('epsilon'), 
  epsilon = 1e-3;
end
if ~exist('classy'), 
  classy = 'LIBSVM';
  % Can be: 'SVMLIGHT', 'LIBSVM'
end
%sg('loglevel', 'ERROR');     % 'ERROR' or 'ALL'


% INITIALIZATION 
num_data = length(y);
sg('new_svm', classy);
sg('c', C);
sg('svm_use_bias', 1);
sg('svm_epsilon',epsilon);
sg('new_svm', classy);
if size(y,1)>size(y,2), y=y'; end
num_feat = size(X,1);
num_subj = size(X,2);
assert(mod(num_feat,3)==0);
indices =  zeros(rep,top_k);
alpha_full = zeros(num_subj,rep);
labels_perm = zeros(rep,num_subj);


% KERNEL PRE-COMPUTATION
X = double(X);
K = addRidge(X'*X);
sg('set_kernel', 'CUSTOM', K, 'FULL');
sg('init_kernel','TRAIN');


% PERMUTATION LOOP
idx = 0;
for i = 1:rep

	% RANDOM LABELS
	%fprintf('\n Genereate random labels\n');
	tic,
	perm = randperm(num_data);
	labels_perm(i,:) = y(perm);
	time.gen_labels(i)=toc; 
	
	% SVM TRAINING
	%fprintf('\n Train SVM (only new labels)\n');
	tic,
	sg('set_labels','TRAIN', labels_perm(i,:));
	sg('svm_train');
	time.train_svm(i)=toc;

	% COMPUTE w
	%fprintf('\n Get Results\n');
	tic,
	[b,alpha] = sg('get_svm');
	time.get_results(i)=toc;
	%fprintf('\n Mutiply X with alpha\n');
	tic,
	alpha_full(alpha(:,2)+1,i) = alpha(:,1);
	time.create_full_alpha_X(i)=toc; 
	
end

tic,
w = X * alpha_full;
time.multiply_alpha_X = toc; 
%fprintf('\n Standardize weights and bring into correct format\n');
tic,
w = abs(w) .* ( ones(num_feat,1) * sum(w.*w,1).^(-0.5) );
w = squeeze(sum(reshape(w.^p,3,num_feat/3,rep),1)).^(1/p); 
w = w';
time.modify_w=toc; 

for i = 1:rep

  %fprintf('\n Apply filter to weights\n');
  tic,

  w(i,:) = filter_pnorm(w(i,:),filter_window_size,p_pnorm_filter)';
  time.filter(i) = toc; 

end

%%% compute feature ranking %%%
%fprintf('\n Compute feature ranking\n');
tic,
[foo, ranking] = sort(w, 2, 'descend');
time.ranking=toc; 


%%% Compute p-values Bonferoni %%%
%fprintf('\n Multiple Testing\n');
tic,
indices = ranking(:,1:top_k);
indices = sort([indices*3, indices*3-1, indices*3-2],2); 
time.indices_for_testing=toc; 

for i = 1:rep
  tic,
  pvalue_combi_filtered_ = multiple_testing_chi(data(:,indices(i,:)) ,labels_perm(i,:));
  time.testing(i)=toc; 
  %%% save lowest p-value
  %fprintf('\n  Save lowest pvalue\n');
  tic,
  p_lowest(i)=min(pvalue_combi_filtered_);
  pvalue_all{i}=pvalue_combi_filtered_;
  time.save_p(i)=toc; 
end

%%% clear workspace
clearvars selector_filtered foo labels_perm res ranking indices pvalue_combi_filtered_ pvalue_mtest_ chisq_value call_rate maf lex_min lex_max tables w_;
	
%%% Find t_star as alpha-percentile of sorted p-values
p_lowest_sorted = sort(p_lowest);
t_star = p_lowest_sorted(max(ceil(rep*alpha_sig)-1, 1));

all_pvalues = sort(cell2mat(pvalue_all));
t_star_EV = all_pvalues(max(ceil(rep*alpha_sig)-1, 1));

clearvars featmat data labels p_lowest_sorted;

% COLLECT RESULTS
result = struct('w',w,'C',C,'rep',rep,'classy',classy,'epsilon',epsilon, 'p',p,'p_lowest', p_lowest, 't_star', t_star, 'all_pvalues', all_pvalues, 't_star_EV', t_star_EV );

