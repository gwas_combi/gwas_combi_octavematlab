% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function lpm = train_lpm_( varargin )

% train_lpm  -- training a 1-norm regularized support vector machine (aka lineare programming machine)
%
% Synopsis:
%     lpm = train_lpm( properties)
%
% Returns:
%     lpm:  a trained classifier struct   (to be input to apply_lpm for testing)
%
% Properties:
%        X:   [d x n] data
%        y:   [1 x n] or [n x 1] labels
%        C:   [1 x 1] SVM Regularization parameter
%        exitflag:  0 (error)  or  1 (optimization successful)
%        w_warmstart:  [d x 1]  a guess for the optimal weight vector w
%
%
% Author: Marius Kloft, TU Berlin, 2012
%



%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Default Settings  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

properties= propertylist2struct(varargin{:});
properties = set_defaults(properties, ...
                        'X', [], ...
                        'y', [], ...
                        'C',1, ...
						'w_warmstart', [], ...
                        'exitflag', []);
struct2workspace(properties);


%%%%%%%%%%%%%%%%%%%%%%
%%%  Preparations  %%%
%%%%%%%%%%%%%%%%%%%%%%

if size(y,1)>1, 
  y = y';
end

if size(w_warmstart,2)>1, 
  w_warmstart = w_warmstart';
end

if (size(X,1) == length(y)) & (size(X,2) ~= length(y));
  X = X';
end

[d,n] = size(X);


%%%%%%%%%%%%%%%%%%%%
%%%  Assertions  %%%
%%%%%%%%%%%%%%%%%%%%

assert( length(X)>0 );
assert( size(X,2) == size(y,2) );


%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Initialization  %%%
%%%%%%%%%%%%%%%%%%%%%%%%

lpm = [];
aux = (y'*ones(1,d)).*X';
  

%%%%%%%%%%%%%%%%%%%
%%%  Warmstart  %%%
%%%%%%%%%%%%%%%%%%%

if length(w_warmstart) > 0
  w_warmstart_pos = max(0,w_warmstart);
  w_warmstart_neg = -min(0,w_warmstart);
  b_warmstart = 0;
  slack = 1 - aux * [w_warmstart_pos; w_warmstart_neg];
  x0 = [ w_warmstart_pos; w_warmstart_neg; 0; slack];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Solve the PRIMAL via CPlex  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f = [ ones(1,2*d), 0, C*ones(1,n) ];
Aineq = [ -aux, aux, -y', -eye(n) ];
bineq = -ones(n,1);
Aeq = [];
beq = [];
lb = [ zeros(2*d,1); -inf; zeros(n,1) ];
ub = inf * ones(size(lb));    %ub = inf * ones( 2*d + 1 + n, 1); 
tic;
if length(w_warmstart) > 0
  [x,fval,exitflag,output,lambda] = cplexlp(f,Aineq,bineq,Aeq,beq,lb,ub,x0);
else 
  [x,fval,exitflag,output,lambda] = cplexlp(f,Aineq,bineq,Aeq,beq,lb,ub);
end
primal_time = toc;
primal_objective = f*x;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Solve the DUAL via CPlex  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fd = -[-bineq' lb' -ub'];
Aineqd = [];
bineqd = [];
Aeqd = [Aineq', -eye(length(lb)), eye(length(lb))];
beqd = -f';
lbd = zeros(size(fd));
ubd = inf * ones(size(fd));
[x_dual,fval_dual,exitflag_dual,output_dual] = cplexlp(fd,Aineqd,bineqd,Aeqd,beqd,lbd,ubd);
fval_dual = -fval_dual;



%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Collect Result  %%%
%%%%%%%%%%%%%%%%%%%%%%%%

lpm.w = x(1:d) - x(d+1:2*d);
lpm.b = x(2*d+1);
lpm.primal_time = primal_time;
lpm.primal_objective = primal_objective;
lpm.train_loss = sum(x(end-n+1:end));
lpm.norm_w = sum(x(1:2*d));






