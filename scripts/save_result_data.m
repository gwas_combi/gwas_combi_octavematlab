% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function save_result_data(results_file_prefix, p_values_genome, selected_indices_genome, svm_weights_genome, p_values_all_genome, selected_rs_ids_genome)

num_chromosomes = size(p_values_genome, 1);
for chromo = 1:num_chromosomes
	%% Save the results per chromosome
	result_chromo_file = [results_file_prefix, 'chromo_', int2str_leading_zeros(chromo, 2), '.mat'];

	p_values = p_values_genome{chromo}; %#ok<NASGU> supress warning: unused value
	selected_indices = selected_indices_genome{chromo}; %#ok<NASGU> supress warning: unused value
	svm_weights = svm_weights_genome{chromo}; %#ok<NASGU> supress warning: unused value
	p_values_all = p_values_all_genome{chromo}; %#ok<NASGU> supress warning: unused value
	selected_rs_ids = selected_rs_ids_genome{chromo}; %#ok<NASGU> supress warning: unused value

	save(result_chromo_file, 'p_values', 'selected_indices', 'svm_weights', 'p_values_all', 'selected_rs_ids');
end

%% Save the results for the whole genome
result_file = [results_file_prefix, 'complete', '.mat'];

p_values = p_values_genome; %#ok<NASGU> supress warning: unused value
selected_indices = selected_indices_genome; %#ok<NASGU> supress warning: unused value
svm_weights = svm_weights_genome; %#ok<NASGU> supress warning: unused value
p_values_all = p_values_all_genome; %#ok<NASGU> supress warning: unused value
selected_rs_ids = selected_rs_ids_genome; %#ok<NASGU> supress warning: unused value

save(result_file, 'p_values', 'selected_indices', 'svm_weights', 'p_values_all', 'selected_rs_ids');

