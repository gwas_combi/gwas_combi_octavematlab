% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function manhattan(pvalue, lw, org_num_snps, varargin)

if nargin == 4
	threshold1 = varargin{1};
%	threshold2 = 1;
%elseif nargin == 5
%	threshold1 = varargin{1};
%	threshold2 = varargin{2};
else
	threshold1 = 1;
%	threshold2 = 1;
end

if ~exist('lw', 'var')
	lw = 1;
end

if ~exist('org_num_snps', 'var')
	org_num_snps = size(pvalue, 1);
end

k_max = 20;
%alpha = 0.05;

max_indices = size(pvalue, 1);
indizes = 1:max_indices;
%threshold = repmat(-log10(alpha / org_num_snps), max_indices, 1);

% HACK p-Values smaller then 10^(-k_max) are set to 10^(-k_max)
trunc_pvalue = max(10^(-k_max), pvalue(indizes));

%plot(indizes, -log10(trunc_pvalue), 'color', [0, 0.75, 0], 'LineWidth', lw);
plot(indizes, -log10(trunc_pvalue), '.', 'color', [0, 0, 0.7]);
axis([0, max_indices, -0.5, k_max + 1]);
ylabel('-log_{10}(P)','FontWeight', 'bold');
xlabel('SNP', 'FontWeight', 'bold');
hold on;
ha = plot(indizes, -log10(repmat(threshold1, 1, max_indices)), '-k', 'LineWidth', lw*2); hold on;
%hb = plot(indizes, -log10(repmat(threshold2, 1, max_indices)), '-k', 'LineWidth', lw*2);
hold off;
set(gca, 'XTick', (0:5000:size(pvalue, 1)));
set(gca, 'YTick', (0:5:15));
% Varargin possibly contains various thresholds.
% The first one should be Plan C, second one Bonferroni
%if (nargin == 5)
%	legend([ha, hb], {'Plan C' 'Bonferroni'})
%end
