% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = perm_test_svmlight( X, y, rep, C, p, classy, epsilon )
% NOT DONE AT ALL!!! Use permtest_libsvm!
tic;

% PARAMETERS
if ~exist('rep'), rep = 1000; end
if ~exist('C'), C = 1; end
if ~exist('p'),  p = 1; end
if ~exist('epsilon'), 
  epsilon = 1e-3;
end
if ~exist('classy'), 
  classy = 'SVMLIGHT';
  % Can be: 'SVMLIGHT', 'LIBSVM'
end
%sg('loglevel', 'ERROR');     % 'ERROR' or 'ALL'


% INITIALIZATION 
num_data = length(y);
sg('new_svm', classy);
sg('c', C);
sg('svm_use_bias', 1);
sg('svm_epsilon',epsilon);
sg('new_svm', classy);
if size(y,1)>size(y,2), y=y'; end
num_feat = size(X,1);
assert(mod(num_feat,3)==0);
w =  zeros(rep,num_feat/3);
ranking =  zeros(rep,num_feat);


% KERNEL PRE-COMPUTATION
X = double(X);
K = addRidge(X'*X);
sg('set_kernel', 'CUSTOM', K, 'FULL');
sg('init_kernel','TRAIN');


% PERMUTATION LOOP
idx = 0;
for i = 1:rep

	% RANDOM LABELS
	perm = randperm(num_data);

	% SVM TRAINING
	sg('set_labels','TRAIN', y(perm));
	sg('svm_train');
	toc,

	% COMPUTE w
	[b,alpha] = sg('get_svm');
	w_ = X(:,alpha(:,2)+1) * alpha(:,1);
	w_ = abs(w_')/norm(w_);
	w_ = (sum(abs(reshape(w_(:)',3,num_feat/3)).^p)).^(1/p); 
	w(i,:) = w_/norm(w_);
		
end;


% COLLECT RESULTS
result = struct('w',w,'C',C,'rep',rep,'classy',classy,'epsilon',epsilon,'time',toc,'p',p);


% CLEAN MEMORY
sg('clean_kernel');



