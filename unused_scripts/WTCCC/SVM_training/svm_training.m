% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% Is job_ID contained in chromos?
if ~ismember(chromo, chromos)
	return;
end
tic;


%% load labels
load([datadir ', labels.mat']);
labels = y; clear y;
individuals = length(labels);


%% load genotypes
data = load([datadir, 'chromo_', int2str_leading_zeros(chromo, 2), '.mat']);
data = data.X;
assert(mod(size(data, 2), 3) == 0);
num_snp = size(data, 2) / 3;
load_data_time_ = toc;


%% Compute p-values Bonferoni
tic;
pvalue_mtest_ = chi_square_goodness_of_fit_test(data, labels);
compute_pvalues_time_ = toc;

%% string data to feature_matrix
tic;
featmat = string_to_featmat(data);
clearvars data;
featmat = center(featmat);
featmat = scale(featmat, pnorm_feature_scaling);
string_to_featmat_time_= toc;


%% SVM training
res = train_liblinear(featmat, labels, svm_rep, Cs, p_svm, classy, svm_epsilon);

%% collect SVM results:  (1) the weight w  (2) the accuracy   (3) the optimal C
w_ = res.w;
%w_avg_ = res.w_avg;
%acc_ = res.acc_test;
C_ = res.C;

clearvars X featmat labels;

%result = struct( ...
%	'top_k', top_k, ...
%	'svm_rep', svm_rep, ...
%	'Cs', Cs, ...
%	'p_svm', p_svm, ...
%	'pnorm_feature_scaling', pnorm_feature_scaling, ...
%	'filter_window_size', filter_window_size, ...
%	'p_pnorm_filter', p_pnorm_filter, ...
%	'classy', classy, ...
%	'filter_window_size_mtest', filter_window_size_mtest, ...
%	'p_pnorm_filter_mtest', p_pnorm_filter_mtest, ...
%	'individuals',  individuals, ...
%	'w_', w_, ...
%	'w_filtered_', w_filtered_, ...
%	'ranking_SVM_', ranking_SVM_, ...
%	'ranking_SVM_filtered_', ranking_SVM_filtered_, ...
%	'ranking_mtest_filtered_', ranking_mtest_filtered_, ...
%	'pvalue_mtest_', pvalue_mtest_, ...
%	'pvalue_mtest_filtered_', pvalue_mtest_filtered_, ...
%	'pvalue_combi_', pvalue_combi_, ...
%	'pvalue_combi_filtered_', pvalue_combi_filtered_);
