% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function files_hash = files2hash(files)
% Returns the SHA-1 hash of the contents of a file.
% NOTE This works only under Matlab, not under Octave!

dataHashOptions.Method = 'SHA-1';
dataHashOptions.Input  = 'file';
dataHashOptions.Format  = 'double';
data_hash = zeros([1, 20]);
for file_index = 1:length(files)
	file = files{file_index};
	data_hash = data_hash + DataHash(file, dataHashOptions);
end
data_hash = mod(data_hash, 256);
data_hash = dec2hex(data_hash);
files_hash = reshape(data_hash', 1, 40);
