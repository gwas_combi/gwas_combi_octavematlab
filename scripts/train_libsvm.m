% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_libsvm(X, y, encoding_factor, rep, Cs, p, classy, epsilon, using_shogun)
% Trains libSVM (through the Shogun Toolbox) with the given data,
% and returns the results (the state of the classifier) in a struct.

tic;

X = double(X); % HACK This should be fixed to limit memory requirements...


%% PARAMETERS
if ~exist('Cs', 'var'), Cs = logspace(-5, 5, 11); end
num_Cs = size(Cs, 1);
if ~exist('rep', 'var')
	if num_Cs == 1,
		rep = 1;
	else
		rep = 30;
	end
end
if ~exist('p', 'var'), p = 2; end
if ~exist('epsilon', 'var'),
	epsilon = 1e-3;
end
if ~exist('classy', 'var'),
	classy = 'LIBSVM';
	% Can be: 'SVMLIGHT', 'LIBSVM'
end
if ~exist('using_shogun', 'var')
	using_shogun = 0; % by default: use libSvm directly
end
loglevel = 'ERROR'; % 'ERROR' or 'ALL'
use_bias = 1; % HACK

num_feat = size(X, 2);

%% INITIALIZATION
num_data = size(y, 1);
if size(y, 1) > size(y, 2), y = y'; end
acc_val = zeros(rep, num_Cs);
acc_test = zeros(rep, num_Cs);
w_val = zeros(rep, num_feat / 3, num_Cs);
%w = zeros(rep, num_feat / 3);
%ranking = zeros(rep, num_feat / 3, num_Cs);


%% KERNEL PRE-COMPUTATION
%K = X' * X;
K = X * X';
%K = addRidge(K);

%% VALIDATION LOOP
if rep > 1 || num_Cs > 1
	% if rep == 1 skip validation loop!

	idx = 0;
	for i = 1:rep
		fprintf('Validation loop, repetition %d / %d\n...', i, rep);
		%% TRAIN / VAL SPLITS
		perm = randperm(num_data);
		divtr = perm(1:floor(num_data / 2));
		divval = perm(floor(num_data / 2) + 1:floor(num_data * 3 / 4));
		divte = perm(floor(num_data * 3 / 4) + 1:end);
		Xtr = X(:, divtr);
		Ktr = K(divtr, divtr);
		Kval = K(divtr, divval);
		Kte = K(divtr, divte);
		ytr = y(divtr);
		yval = y(divval);
		yte = y(divte);

		for j = 1:num_Cs
			C = Cs(j);

			%% SVM TRAINING, VALIDATION, TESTING
			[w_val(i, :, j), acc_val(i, j), acc_test(i, j)] = ...
				libsvm_raw_trainAndPredict(Ktr, Xtr, ytr, Kval, yval, Kte, yte, C, classy, epsilon, loglevel, using_shogun);

			idx = idx + 1;
			%print_progress(idx, rep * num_Cs);
		end
	end

	%% MODEL SELECTION
	[~, ind] = max(mean(acc_val, 1)); % returns [max, index_max]
	verbose.acc_test = acc_test;
	verbose.acc_test_stderr = std(acc_test, [], 1) / sqrt(rep);
	acc_test_stderr = std(acc_test(:, ind)) / sqrt(rep);
	acc_test = mean(acc_test(:, ind), 1);
	C = Cs(ind);
else
	C = Cs;
end


%% SVM TRAINING ON FULL DATA
[weights_per_feature_normalized, weights_per_feature_raw] = libsvm_raw_train(K, X, y, C, classy, epsilon, loglevel, using_shogun, use_bias);

weights_per_snp_raw_matrix = reshape(weights_per_feature_raw(:)', encoding_factor, num_feat / encoding_factor);
weights_per_snp_normalized_matrix = reshape(weights_per_feature_normalized(:)', encoding_factor, num_feat / encoding_factor);
weights_per_snp_matrix = weights_per_snp_raw_matrix;


%% COMPUTE ALPHA
%alpha_full = zeros(length(y), 1);
%alpha_full(alpha(:, 2) + 1) = alpha(:, 1);

%% COMPUTE RANKINGS w
if (encoding_factor > 1)
	w = sum(abs(weights_per_snp_matrix) .^ p) .^ (1 / p);
else
	% There is no need to squeeze one dimension together into one again
	w = weights_per_snp_matrix;
end


%% COLLECT RESULTS
if rep > 1
	w_avg = median(w_val(:, :, ind), 1);
	w_avg = mean(w_avg(:)', 1); % TODO copy over the line from train_liblinear.m?
	w_avg = w_avg / norm(w_avg);
	result = struct( ...
		'w_avg', w_avg(:)', ...
		'w', w(:)', ...
		'acc_val', acc_val, ...
		'acc_test', acc_test, ...
		'acc_test_stderr', acc_test_stderr, ...
		'verbose', verbose, ...
		'Cs', Cs, ...
		'C', C, ...
		'rep', rep, ...
		'classy', classy, ...
		'epsilon', epsilon, ...
		'time', toc);
else
	result = struct( ...
		'w', w(:)', ...
		'Cs', Cs, ...
		'C', C, ...
		'rep', rep, ...
		'classy', classy, ...
		'epsilon', epsilon, ...
		'time', toc);
end

libsvm_raw_cleanup(using_shogun);

