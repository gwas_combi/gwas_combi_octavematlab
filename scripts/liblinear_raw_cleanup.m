% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function liblinear_raw_cleanup(using_shogun)
% A basic wrapper for libLinear (cleanup),
% that either invokes it through the Shogun Toolbox,
% or directly through the libraries own Matlab/Octave interface.
% using_shogun can be:
% * 0 for using libLinear directly, through its own Matlab/Octave interface
% * 1 to use it through the Shogun Toolbox

if ~exist('using_shogun', 'var')
	using_shogun = 0; % by default: use libLinear directly
end

if (using_shogun)
	%% CLEAN UP MEMORY
	sg('clean_features', 'TRAIN');
	sg('clean_features', 'TEST');
end

