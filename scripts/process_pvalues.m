% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function pvalue_filtered = process_pvalues(pvalue_mtest, killSelector)
% Given the P-values of the test and a selection
% of features to kill (zero/one Matrix), outputs new P-values.

% fetch the number of kernels
[~, ~, num_ks] = size(killSelector); % returns [rep, num_snp, num_ks]

pvalue_filtered = [];

for j = 1:num_ks
	pvalue_filtered = [pvalue_filtered; ...
		max(~killSelector(:, :, j), pvalue_mtest)];
end

