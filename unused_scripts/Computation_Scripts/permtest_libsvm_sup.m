% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = permtest_libsvm( X,data, y, rep, C, p, classy, epsilon, filter_window_size, p_pnorm_filter, top_k, noise_snps, inform_snps, alpha_sig )

tic;

% PARAMETERS
if ~exist('rep'), rep = 1000; end
if ~exist('C'), C = 1; end
if ~exist('p'),  p = 1; end
if ~exist('epsilon'), 
  epsilon = 1e-3;
end
if ~exist('classy'), 
  classy = 'LIBSVM';
  % Can be: 'SVMLIGHT', 'LIBSVM'
end
%sg('loglevel', 'ERROR');     % 'ERROR' or 'ALL'


% INITIALIZATION 
num_data = length(y);
sg('new_svm', classy);
sg('c', C);
sg('svm_use_bias', 1);
sg('svm_epsilon',epsilon);
sg('new_svm', classy);
if size(y,1)>size(y,2), y=y'; end
num_feat = size(X,1);
assert(mod(num_feat,3)==0);
w =  zeros(rep,num_feat/3);
ranking =  zeros(rep,num_feat);


% KERNEL PRE-COMPUTATION
X = double(X);
K = addRidge(X'*X);
sg('set_kernel', 'CUSTOM', K, 'FULL');
sg('init_kernel','TRAIN');


% PERMUTATION LOOP
idx = 0;
for i = 1:rep

	% RANDOM LABELS
	perm = randperm(num_data);
	labels_perm = y(perm);

	% SVM TRAINING
	sg('set_labels','TRAIN', labels_perm);
	sg('svm_train');

	% COMPUTE w
	[b,alpha] = sg('get_svm');
	w_ = X(:,alpha(:,2)+1) * alpha(:,1);
	w_ = abs(w_')/norm(w_);
	w_ = (sum(abs(reshape(w_(:)',3,num_feat/3)).^p)).^(1/p); 
	w_ = w_/norm(w_);
	w(i,:) = w_;
	
	w_filtered_ = filter_pnorm(w_,filter_window_size,p_pnorm_filter);
   
    %%% compute feature ranking %%%
    [foo, ranking_SVM_filtered_] = sort(w_filtered_, 'descend');
    
    %%% Compute p-values Bonferoni %%%
    [pvalue_mtest_,chisq_value,call_rate,maf,lex_min,lex_max,tables] = multiple_testing_chi(data ,labels_perm);
	
    %%% perform feature screening and compute new p-values %%%
    selector_filtered = ranking_to_selector(ranking_SVM_filtered_,top_k);  
    pvalue_combi_filtered_ = process_pvalues(pvalue_mtest_,selector_filtered);       pvalue_combi_filtered_sup = pvalue_combi_filtered_([1:noise_snps/2, noise_snps/2+inform_snps+1:(noise_snps+inform_snps)]);

    %%% save lowest p-value
    p_lowest(i)=min(pvalue_combi_filtered_);
    p_lowest_sup(i)=min(pvalue_combi_filtered_sup);
     
    %%% clear workspace
    clearvars selector_filtered foo labels_perm res w_filtered_ ranking_SVM_filtered_ pvalue_combi_filtered_ pvalue_combi_filtered_sup pvalue_mtest_ chisq_value call_rate maf lex_min lex_max tables w_;
		
end;
%%% Find t_star as alpha-percentile of sorted p-values
p_lowest_sorted = sort(p_lowest);
p_lowest_sorted_sup = sort(p_lowest_sup);
t_star = p_lowest_sorted(max(ceil(rep*alpha_sig)-1, 1));
t_star_sup = p_lowest_sorted_sup(max(ceil(rep*alpha_sig)-1, 1));

clearvars featmat data labels p_lowest_sorted_sup p_lowest_sorted;

% COLLECT RESULTS
result = struct('w',w,'C',C,'rep',rep,'classy',classy,'epsilon',epsilon,'time',toc,'p',p,'p_lowest',p_lowest,'p_lowest_sup', p_lowest_sup, 't_star', t_star, 't_star_sup', t_star_sup);


% CLEAN MEMORY
sg('clean_kernel');



