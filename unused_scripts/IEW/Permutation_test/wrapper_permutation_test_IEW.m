% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Choose Phenotype 				  			 	 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

phenotype = 'prosocA' % 'prosocA', 'med_rrp_gains', 'med_rrp_losses', 'timepref1', 'timepref2'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Directories: Please change to yours   		 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

workdir = '/home/bmieth/svn/iew/scripts/marius/old_scripts/Permutation_test/';
cd(workdir);
savedir = [workdir,phenotype],

datadir = '/home/bmieth/svn/iew/data/IEW/data2000/';
labels_file = ['/home/bmieth/svn/iew/data/IEW/data2000/',phenotype,'.mat'];
labels_di_file = ['/home/bmieth/svn/iew/data/IEW/data2000/',phenotype,'_di.mat'];

script_path ='/home/bmieth/svn/iew/scripts/marius/computation_scripts';
addpath(script_path);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  NOTE: the variable over which we parallelize is saved in "replacethis1"  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

chromos = [1:22];
chromo = chromos(replacethis1);

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Main Scripts!   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%

parameters_permutation_test_IEW;
permutation_test_IEW;
savefile = [savedir '_permtest_result' int2str_leading_zeros(chromo,2) '.mat'];
save(savefile);
