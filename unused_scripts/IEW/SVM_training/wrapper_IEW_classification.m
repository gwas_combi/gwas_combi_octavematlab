% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Directories: Please change to yours   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for chromo=1:22
savedir = '/home/bmieth/svn/iew/qsub/wtccc_crohn/results/IEW/IEW2000/prosocA/classification_L1/',
workdir = '/home/bmieth/svn/iew/qsub/wtccc_crohn/scripts/';
datadir = '/home/bmieth/svn/iew/data/IEW/data2000/';
cd(workdir);
addpath('/home/bmieth/svn/iew/scripts/marius');
%labels_file = '/home/bmieth/svn/iew/data/IEW/data2000/prosocA_di.mat';
labels_di_file = '/home/bmieth/svn/iew/data/IEW/data2000/prosocA_di.mat';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  NOTE: the variable over which we parallelize is saved in "replacethis1"  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

chromos = 1:22;
%chromo = chromos(replacethis1);
%chromo=8

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Main Scripts!   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%

rep=1
Cs = 0.00001 %0.00001
p_svm = 15;

%tube_eps = 0.001 %0.001

flag_schnell = false;

parameters_iew;
classy = 'LIBLINEAR_L1R_L2LOSS_SVC',
svm_training_iew_classification_L1;
savefile = [savedir 'result_' int2str_leading_zeros(chromo,2) '.mat'];
save(savefile);

%plot(res.w,'.')
%axis([0 length(res.w), 0,0.1])

%mean(abs(labels-mean(labels))),

end