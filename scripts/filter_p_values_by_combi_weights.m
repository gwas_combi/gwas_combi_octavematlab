% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [p_values, selected_indices] ...
	= filter_p_values_by_combi_weights(weights, p_values_all, k_to_keep)
% Filters P-values, leaving only the most important k_to_keep,
% settign all others to 1.

% Generate rankings (order SNP indices by their importance/weight)
[~, ranking] = sort(weights, 'descend');

% Select the most important SNP indices
selected_indices = ranking_to_selector(ranking, k_to_keep);

% For those SNPs, select their P-values, and set all others to 1
p_values = process_pvalues(p_values_all, selected_indices);

