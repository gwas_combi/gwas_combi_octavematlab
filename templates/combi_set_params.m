% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function params = combi_set_params(params)

% TODO
%data-dir=${DATA_DIR}
%6.file1-path=${LOAD_MAP_FILE}
%7.file2-path=${LOAD_PED_FILE}
%8.sample-info-path=${LOAD_SAMPLE_INFO_FILE}



params.use_libLinear = ${USE_LIB_LINEAR};
params.run_per_chromosome = ${PER_CHROMOSOME};
params.feature_embedding = '${GENOTYPE_ENCODING}';
params.feature_scaling_p_norm = ${GENOTYPE_ENCODING_P};
params.svm_Cs = ${SVM_C};
params.svm_epsilon = ${SVM_EPS};
%params.weights_decoding_p_norm = 2;
params.smoothing_filter_window_size = ${WEIGHTS_FILTER_WIDTH};
%params.smoothing_filter_p_norm = 2;
params.snps_to_keep_fraction = ${MARKERS_TO_KEEP_FRACTION};
