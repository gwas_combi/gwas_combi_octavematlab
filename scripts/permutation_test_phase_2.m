% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2016 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [thresholds] ...
	= permutation_test_phase_2(data, labels, combi_parameters, p_value_target, feature_matrix, encoding_factor)
% Calculate P-Values from COMBI runs, using random labels, many times,
% and collect the lowest P-Values, one from each iteration.
% With these and the alpha==t_star value, we calculate a threshold.

iterations = combi_parameters.threshold_calibration_permutation_2_iterations;

% Initialize
p_lowest = zeros(1, iterations);

for i = 1:iterations
	% Show progress
	disp([num2str(round(i * 100 / iterations)), '%']);

	% Permute labels
	num_labels = length(labels);
	labels_perm = zeros(1, num_labels);
	labels_perm(randperm(num_labels)) = labels;

	% Run the COMBI method, to evaluate the lowest P-Value
	[~, ~, ~, p_values_raw, ~, ~, ~] ...
		= combi_method(data, labels_perm, combi_parameters, feature_matrix, encoding_factor);

	% Save lowest p-value
	if (size(p_values_raw, 2) == 1)
		p_lowest(i) = min(p_values_raw);
	else
		p_lowest(i) = 1.0;
	end

end

% Find t_star as alpha-percentile of sorted p-values
%p_lowest_sorted = sort(p_lowest);
[~, closest_index] = min(abs(p_lowest - p_value_target));
t_star = p_lowest(closest_index);
threasholds = t_star;

