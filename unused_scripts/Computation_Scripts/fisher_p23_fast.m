% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Randomized and de-randomized p-values based on Fisher's  %
% exact test for (2x3)-tables                              %
% fast version                                             %
%                                                          %
% Thorsten Dickhaus, 24.09.2010                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [nonrand_p rand_p] = fisher_p23_fast(obs, epsilon)


	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% neuer Versuch
	% Angelehnt an http://www.mathworks.fr/matlabcentral/fileexchange/15399-myfisher23/content/myfisher23.m
 
	% sort observed matrix  according to row and col totals
	row_marginals = sum(obs, 2);
	col_marginals = sum(obs);
	N = sum(row_marginals);
	
	if ~issorted(col_marginals)
	[col_marginals,ind]=sort(col_marginals);
	obs=obs(:,ind);
	clear ind
	end
	if ~issorted(row_marginals)
	[row_marginals,ind]=sort(row_marginals);
	obs=obs(ind,:);
	clear ind
	end

	% Generate all possible obs
	pos11 = 0:1:min(row_marginals(1),col_marginals(1)); % all possible values of obs(1,1)
	max12 = min(col_marginals(2),row_marginals(1)-pos11); % max value of obs(1,2) given obs(1,1)
	num_tables =sum(max12+1); % number of tables to generate
	pos_tables = zeros(num_tables,6); % Matrix preallocation

	%compute the index

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%Mit For-Schleife
	%stop=cumsum(max12+1);
	%start=[1 stop(1:end-1)+1];

	%In the first round of the for cycle, Column 1 assignment should be skipped
	%because it is already zero. So, modify the cycle...
	%pos_tables(start(1):stop(1),2)=0:1:max12(1); %Put in the Column2 all the possible values of obs(1,2) given obs(1,1)

	%for i=2:length(pos11)
	%pos_tables(start(i):stop(i),1)=pos11(i); %replicate the pos_11(i) value for max_12(o)+1 times, Put in the Column2 all the possible values of obs(1,2) given obs(1,1)
	%pos_tables(start(i):stop(i),2)=0:1:max12(i); 
	%end
	
	%clear pos11 max12 start stop
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% ohne For-Schleife
	
index=zeros(1,sum(max12+1));
index([cumsum([1 (max12(max12>0)+1)])]) = 1;
index(1)=0;
pos_tables(:,1) = cumsum(index(1:find(index,1,'last')-1));

index = [0 ones(1,sum(max12+1)-1)];
indices = cumsum(max12(max12>0)+1)+1;
indices = indices(1:end-1);
index(indices)=-(max12(1:end-1));
pos_tables(:,2) = cumsum(index);

	clear index indices pos11 max12
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
	
	

	% Complete the table by calculating all possible values of obs(1,3) given obs(1,1) and obs(1,2)
	pos_tables(:,3)=row_marginals(1)-sum(pos_tables(:,1:2),2);
	% Complete the second row given the first row
	pos_tables(:,4:6)=repmat(col_marginals,num_tables,1)-pos_tables(:,1:3);

	%Compute log(x!) using the gammaln function
	zf=gammaln(pos_tables+1); %compute log(x!)
	K=sum(gammaln([row_marginals' col_marginals]+1))-gammaln(N+1); %The costant factor K=log(prod(Rs!)*prod(Cs!)/N!)
	np=exp(K-sum(zf,2)); %compute the p-value of each possible matrix
	[tf,obt]=ismember(obs(1,:),pos_tables(:,1:3),'rows'); %Find the observed table
	clear zf K tf
	%Finally compute the probability for 2-tailed test
	tables_of_interest = np(np<=np(obt));
	nonrand_p=sum(tables_of_interest);


	% Calculate random p
	prob_obs=np(obt);% Calculate probability of observation.

	prob_lauf=zeros(1,length(tables_of_interest));

	rand_count = length(find(abs(exp(prob_lauf) - exp(prob_obs)) < epsilon));        
	u = rand(1);
	rand_p = max(0.0, nonrand_p - u*rand_count*prob_obs);










%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Try for multiple 2x3 tables
%	num_snps=length(tables(1,1,:));
	%row_marginals = squeeze(sum(tables, 2));
	%col_marginals = squeeze(sum(tables,1));
	%N = squeeze(sum(row_marginals));
	
% sort observed matrices  according to row and col totals
%pos11=repmat(0:300, num_snps);
%max12=repmat(0:300, num_snps);

%for i = 1: num_snps
	
%	if ~issorted(col_marginals(:,i))
%	[col_marginals(:,i),ind]=sort(col_marginals(:,i));
%	tables(:,:,i)=tables(:,ind,i);
%	clear ind
%	end
%	if ~issorted(row_marginals(:,i))
%	[row_marginals(:,i),ind]=sort(row_marginals(:,i));
%	tables(:,:,i)=tables(ind,:,i);
%	clear ind
%	end
%	
	
%	pos11{i} = 0:1:min(row_marginals(1,i),col_marginals(1,i));
%	max12{i} = min(col_marginals(2,i),row_marginals(1,i)-pos11{i}); % max value of obs(1,2) given obs(1,1)
%
%end


%	num_tables =sum(max12{:}.+1); % number of tables to generate
%	pos_tables = zeros(num_tables,6); % Matrix preallocation


%index=zeros(1,sum(max12+1));
%index([cumsum([1 (max12(max12>0)+1)])]) = 1;
%index(1)=0;
%pos_tables(:,1) = cumsum(index(1:find(index,1,'last')-1));

%index = [0 ones(1,sum(max12+1)-1)];
%indices = cumsum(max12(max12>0)+1)+1;
%indices = indices(1:end-1);
%index(indices)=-(max12(1:end-1));
%pos_tables(:,2) = cumsum(index);

	%clear index indices pos11 max12
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
	
	

	% Complete the table by calculating all possible values of obs(1,3) given obs(1,1) and obs(1,2)
%	pos_tables(:,3)=row_marginals(1)-sum(pos_tables(:,1:2),2);
%	% Complete the second row given the first row
%	pos_tables(:,4:6)=repmat(col_marginals,num_tables,1)-pos_tables(:,1:3);
%
%	%Compute log(x!) using the gammaln function
%	zf=gammaln(pos_tables+1); %compute log(x!)
%	K=sum(gammaln([row_marginals' col_marginals]+1))-gammaln(N+1); %The costant factor K=log(prod(Rs!)*prod(Cs!)/N!)
%	np=exp(K-sum(zf,2)); %compute the p-value of each possible matrix
%	[tf,obt]=ismember(obs(1,:),pos_tables(:,1:3),'rows'); %Find the observed table
%	clear zf K tf
%	%Finally compute the probability for 2-tailed test
%	tables_of_interest = np(np<=np(obt));
%	nonrand_p=sum(tables_of_interest);
%
%
%	% Calculate random p
%	prob_obs=np(obt);% Calculate probability of observation.
%
%	prob_lauf=zeros(1,length(tables_of_interest));
%
%	rand_count = length(find(abs(exp(prob_lauf) - exp(prob_obs)) < epsilon));        
%	u = rand(1);
%	rand_p = max(0.0, nonrand_p - u*rand_count*prob_obs);
%

