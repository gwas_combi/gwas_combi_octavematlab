% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [weights_normalized, weights_raw] = liblinear_raw_train(X, y, cost, classifier, epsilon, loglevel, using_shogun, use_bias)
% A basic wrapper for libLinear (training),
% that either invokes it through the Shogun Toolbox,
% or directly through the libraries own Matlab/Octave interface.
% cost can be:
% * LIBLINEAR_L2R_LR
% * LIBLINEAR_L2R_L2LOSS_SVC_DUAL
% * LIBLINEAR_L2R_L2LOSS_SVC
% * LIBLINEAR_L2R_L1LOSS_SVC_DUAL
% * LIBLINEAR_L1R_L2LOSS_SVC
% using_shogun can be:
% * 0 for using libLinear directly, through its own Matlab/Octave interface
% * 1 to use it through the Shogun Toolbox

if ~exist('using_shogun', 'var')
	using_shogun = 0; % by default: use libLinear directly
end
if ~exist('use_bias', 'var')
	use_bias = 1; % by default: use a bias-term
end

if (using_shogun)
	sg('loglevel', loglevel); % 'ERROR' or 'ALL'

	%% INITIALIZATION
	sg('new_classifier', classifier);
	sg('svm_use_bias', use_bias);
	sg('svm_epsilon', epsilon);

	%% SVM TRAINING
	sg('set_features', 'TRAIN', X);
	sg('set_labels','TRAIN', y);
	sg('c', cost);
	%sg('svm_max_train_time', 60);
	sg('train_classifier');
	[~, weights_raw] = sg('get_classifier'); % returns [bias, weights]
	weights_raw = weights_raw';
else
	libLinear_classifier = liblinear_shogun_to_raw_classifier(classifier);
	libLinear_options = [ ...
		'-s ', num2str(libLinear_classifier), ...
		' -c ', num2str(cost), ...
		' -e ', num2str(epsilon), ...
		' -B ', num2str(use_bias * 2 - 1)];
	%fprintf('libLinear options: "%s"\n', libLinear_options);

	%X = X';
	y = y';
	y = double(sign(y)); % HACK we only do this to fix bad input data in Tutorial_Matrix, where one label at the beginning is -21 instead of 1 or -1
	X = sparse(X);

	model = train(y, X, libLinear_options);
	assert(~isempty(model), 'Failed training the SVM');
	assert(model.nr_class == 2, ['You may only have two different label values, but there are ', model.nr_class])

	w_ = model.w;
	% cut off the bias term (the last entry in the weights vector)
	weights_raw = w_(:, 1:model.nr_feature)';
end

weights_normalized = abs(weights_raw) / norm(weights_raw);

