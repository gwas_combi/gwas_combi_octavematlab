% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Parameters             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

top_k = 100;
svm_rep =1;
Cs=1.0000e-05;
pnorm_feature_scaling = 6;  
filter_window_size = 35;
p_pnorm_filter = 2;
classy = 'LIBSVM'; %Can be: LIBLINEAR_L2R_LR, LIBLINEAR_L2R_L2LOSS_SVC_DUAL, LIBLINEAR_L2R_L2LOSS_SVC, LIBLINEAR_L2R_L1LOSS_SVC_DUAL, 
svm_epsilon = 1e-3;
p_svm = 2;

B=10; %B = 1000; at least 10!!!

alpha_FWER_file = [alphadir, 'alpha_j.mat'];
load(alpha_FWER_file, 'alpha_j')
alpha_sig_FWER = alpha_j(chromo);

alpha_EV_file = [alphadir, 'alpha_j_EV.mat'];
load(alpha_EV_file, 'alpha_j_EV')
alpha_sig_EV = alpha_j_EV(chromo);