% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function selector = ranking_to_selector(ranking, kernels)

[num_repetitions, num_snp] = size(ranking);
num_kernels = size(kernels, 1);

selector = false(num_repetitions, num_snp, num_kernels);
if (num_snp > 0)
	for rep = 1:num_repetitions
		for k = 1:num_kernels
			selector(rep, ranking(rep, 1:kernels(k)), k) = true;
		end
	end
end

