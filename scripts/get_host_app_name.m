% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function name = get_host_app_name(capitalized)
% Returns the host applications name, either 'Octave' or 'Matlab'.

if (~exist('capitalized', 'var'))
	capitalized = 0;
end

if (capitalized)
	if (is_octave())
		name = 'Octave';
	else
		name = 'Matlab';
	end
else
	if (is_octave())
		name = 'octave';
	else
		name = 'matlab';
	end
end

