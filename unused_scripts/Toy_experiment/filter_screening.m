% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function pvalues_final = filter_screening(pvalues, top_k, window_size, p_pnorm)

scores_filtered = filter_pnorm(-log(pvalues), window_size, p_pnorm);
[~, ranking] = sort(scores_filtered, 'descend');
selector = ranking_to_selector(ranking, top_k);
pvalues_final = process_pvalues(pvalues, selector);
