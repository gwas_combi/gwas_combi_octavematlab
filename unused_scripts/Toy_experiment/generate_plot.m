% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Figure  Other baselines %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


cd(workdir);

set(0, 'DefaultAxesFontSize', 12, 'DefaultTextFontSize', 15);

 
figure();

subplot(1, 2, 1);
plot(rec, mean(prec_no_screening, 1), '-','color', [0.6, 0.6, 1], 'LineWidth', 3);  hold on;
plot(rec, mean(prec_filter_screening, 1), '-g', 'LineWidth', 3); hold on;
plot(rec, mean(prec_SVM_screening, 1), '--c', 'LineWidth', 3);  hold on;
plot(rec, mean(prec_SVM_filter_screening, 1), '-','color', [0, 0, 0.7], 'LineWidth', 3); hold on;

axis([0, 1, 0, 1]);
ylabel('Precision', 'FontSize', 15, 'FontWeight', 'bold');
xlabel('True-positive rate', 'FontSize', 15, 'FontWeight', 'bold');
set(gca, 'YTick', (0:0.2:1));
set(gca, 'XTick', (0:0.2:1));
set(gca, 'FontWeight', 'demi');

legend('RPVT', 'RPVT with filtered p-values', 'COMBI method without filter', 'COMBI method', 'Location', 'SouthWest');
 
subplot(1, 2, 2);


plot(FWER_no_screening, FWTPR_no_screening, '-', 'color', [0.6, 0.6, 1], 'LineWidth', 3);  hold on;
plot(FWER_filter_screening, FWTPR_filter_screening, '-g', 'LineWidth',3); hold on;
plot(FWER_SVM_screening, FWTPR_SVM_screening,'--c', 'LineWidth',3);  hold on;
plot(FWER_SVM_filter_screening, FWTPR_SVM_filter_screening, '-', 'color', [0, 0, 0.7], 'LineWidth', 3); hold on;


axis([0, 1, 0, 1]);
xlabel('Family-wise error rate', 'FontSize', 15, 'FontWeight', 'bold');
ylabel('(Family-wise) True-positive rate', 'FontSize', 15, 'FontWeight', 'bold');

legend('RPVT', 'RPVT with filtered p-values', 'COMBI method without filter', 'COMBI method', 'Location', 'SouthEast');



figure();

subplot(1, 2, 1);
plot(rec, mean(prec_no_screening, 1), '-', 'color', [0.6, 0.6, 1], 'LineWidth', 3);  hold on;
plot(rec, mean(prec_SVM_filter_screening, 1), '-', 'color', [0, 0, 0.7], 'LineWidth', 3); hold on;
plot(rec, mean(prec_roshan,1), '-g', 'LineWidth',3); hold on;
plot(rec, mean(prec_SVM_filter_screening_singlesplit, 1), '--c', 'LineWidth', 3); hold on;
plot(rec, mean(prec_SVM_filter_screening_multisplit, 1), '-y', 'LineWidth', 3); hold off;

axis([0, 1, 0, 1]);
ylabel('Precision', 'FontSize', 15, 'FontWeight', 'bold');
xlabel('True-positive rate','FontSize', 15, 'FontWeight', 'bold');
set(gca,'YTick', (0:0.2:1));
set(gca,'XTick', (0:0.2:1));
set(gca,'FontWeight', 'demi');

legend('RPVT', 'COMBI method', 'Roshan et al. (2011)','Wasserman and Roeder (2009)', 'Meinshausen et al. (2009)', 'Location', 'SouthWest');
 
subplot(1, 2, 2);

plot(FWER_no_screening, FWTPR_no_screening, '-', 'color', [0.6, 0.6, 1], 'LineWidth', 3);  hold on;
plot(FWER_SVM_filter_screening, FWTPR_SVM_filter_screening, '-', 'color', [0, 0, 0.7], 'LineWidth', 3); hold on;
plot(FWER_roshan ,FWTPR_roshan, '-g', 'LineWidth', 3); hold on;
plot(FWER_SVM_filter_screening_singlesplit, FWTPR_SVM_filter_screening_singlesplit, '--c', 'LineWidth', 3); hold on;
plot(FWER_SVM_filter_screening_multisplit, FWTPR_SVM_filter_screening_multisplit, '-y', 'LineWidth', 3); hold off;


%axis([0, 0.001, 0, 1]);
xlabel('Family-wise error rate', 'FontSize', 15, 'FontWeight', 'bold');
ylabel('(Family-wise) True-positive rate', 'FontSize', 15, 'FontWeight', 'bold');
set(gca,'YTick', (0:0.2:1));
set(gca,'XTick', (0:0.2:1));
set(gca,'FontWeight', 'demi');

legend('RPVT', 'COMBI method','Roshan et al. (2011)', 'Wasserman and Roeder (2009)', 'Meinshausen et al. (2009)', 'Location', 'SouthEast');
