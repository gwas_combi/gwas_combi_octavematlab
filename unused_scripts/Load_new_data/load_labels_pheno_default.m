% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function load_labels_pheno_default(input_dir, output_dir, filename_base)
% Loads affection labels from a '*.pheno' file
% and stores them in two '*.mat' files,
% to be used by the COMBI scripts.

filename_input_pheno = [input_dir, filename_base, '.pheno'];
filename_output_labels = [output_dir, filename_base, '.mat'];
filename_output_labels_dichotomized = [output_dir, filename_base, '_di.mat'];
[y, y_di] = load_labels_pheno(filename_input_pheno, filename_output_labels, filename_output_labels_dichotomized);

save(filename_output_labels, 'y');
save(filename_output_labels_dichotomized, 'y_di');
