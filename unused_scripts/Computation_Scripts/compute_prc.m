% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function auc = compute_prc(pvalue, pvalue_truth, prc_ub)
% Computes Area under PRC curve for all repetitions and all top_k

% pvalue can be a matrix with three dimensions m x n x k
% m = number of repetitions
% n = number of SNPs
% k = number of different top_k-values
% pvalue_truth is of size 1 x n. 
% The output is of size m x k.

% This function calls 'val_PRC' which computes the PRC AUC
% for one repetition and one top_k.

% If an upper bound for the Precision is not given, set it to 1 (the whole interval is analysed)
if ~exist('prc_ub', 'var')
	prc_ub = 1;
end

% Convert pvalues into scores from which AUC can be computed
score_method = -log(pvalue);
score_truth = -log(pvalue_truth);

% For each repetition and each top_k calculate PRC AUC
auc = zeros(size(pvalue, 1), size(pvalue, 3)); % pre-allocate memory
for i = 1:size(pvalue, 1)
	for j = 1:size(pvalue, 3)
		[~, ~, auc_] = val_PRC(score_method(i, :, j), score_truth, 'ub', prc_ub);
		auc(i, j) = auc_;
	end
end
