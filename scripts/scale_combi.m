% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [X, center_of_mass, std_dev] = scale_combi(X, p, method, d)
% Scaling each feature (row) of data matrix X
% to constant variance = 1/dim, hence we have Var(X) = 1.
%
% Synopsis:
%   [X, center_of_mass, std_dev] = scale_combi(X);
%   [X, center_of_mass, std_dev] = scale_combi(X, p);
%   [X, center_of_mass, std_dev] = scale_combi(X, p, method);
%   [X, center_of_mass, std_dev] = scale_combi(X, p, method, d);
%
% Arguments:
%   X:       data matrix, already centered
%   p:       p-norm, default is p = 2
%   method:  'local' = one std-dev per dimension
%            'global' = a single std-dev for all dimensions
%   d:       #dimensions the data lies in, used for calculations,
%            default = size(X, 1)
%
% Return:
%   X:               data matrix, scaled
%   center_of_mass:  dimensions centers of mass.
%                    as the input is required to be centered already,
%                    these are always all zero.
%   std_dev:         standard deviation before it was set to 1,
%                    one per dimension for method 'local',
%                    a single one for 'global'
%
% Author: Marius Kloft

if ~exist('p', 'var')
	p = 2;
end
[dims, n, dim3] = size(X);
assert(dim3 == 1, ...
	'The data has too many dimensions. It should have exactly 2.');
if ~exist('d', 'var')
	d = dims;
end
if ~exist('method', 'var')
	method = 'local';
end

%X = center(X); % NOTE The following code assumes already centered data!
center_of_mass = zeros(dims, 1); % because the data is already centered

std_dev = (mean(abs(X) .^ p, 2) * d) .^ (1 / p);

switch method
	case 'local'
		%X = X ./ repmat(stddev, [1, n]);
		X = X ./ (std_dev * ones(1, n));
		X(isnan(X) == 1) = 0;
	case 'global'
		std_dev = sqrt(sum(std_dev));
		X = X * std_dev^(-1) * d^(1/2);
	otherwise
		error(['unknown method: ', method]);
end

