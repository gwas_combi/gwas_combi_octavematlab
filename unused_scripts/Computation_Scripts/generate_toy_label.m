% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function labels = generate_toy_label(inform_data, noise_data, param, noise_parameter)
% Generate informative labels via a logistic model as
% proposed in Gilles' draft from August 11th, 2011.

%% Global Parameters

inform_snps = round(size(inform_data, 2) / 3);
%noise_snps = round(size(noise_data, 2) / 3);
individuals = size(inform_data, 1);
%num_snps = inform_snps + noise_snps;
%weights = param(1:end);
c = noise_parameter;
%c = param(end);


%% Assertions

assert(mod(size(noise_data, 2), 3) ~= 1);
assert(mod(size(noise_data, 2), 3) ~= 1);
assert(inform_snps > 0);


%% Generate labels

idx_snp = 10;
snp = inform_data(:, 3*idx_snp - 2:3*idx_snp - 1);
valid = (snp(:, 1) ~= '0');
%homozygous_minor = min(min(snp(valid)));
homozygous_major = max(max(snp(valid)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Gilles' proposal:       %
%    summand = 0 for aa,   %
%    summand = 1 for Aa,   %
%    summand = 2 for AA    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sum ...
	= (snp(:, 1) == homozygous_major) ... % XXX Should we maybe use homozygous_minor here?
	+ (snp(:, 2) == homozygous_major) ...
	+ (snp(:, 2) == '0') ...
	- 1;

probab = 1 ./ (1 + exp(-c * (sum - median(sum))));
%probab = 1 ./ (1 + exp(-c * (sum)));
u = rand(individuals, 1);
%u = 0.5;
labels = 2*(u <= probab) - 1;


%% Return labels

labels = labels(:)';
