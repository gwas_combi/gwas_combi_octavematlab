% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function pvalues_final = single_split(data, labels, top_k, pnorm_feature_scaling, svm_rep, Cs, p, classy, filter_window_size, p_pnorm_filter)
% Plan C algorithm
% 1. Pick randomly half of the data (half of the individuals)
% 2. Run Combi-Method and identify top_k best SNPs
% 3. For those SNPs compute p-values on the second half of the data
% 4. Use threshold-alpha/top_k to identify significant SNPS

%% PARAMETERS

if (size(labels, 1) > size(labels, 2))
	labels = labels';
end

num_subj = length(labels);
%num_snps = size(data, 2);


%% 1. Pick randomly half of the data (half of the individuals)

individual_perm = randperm(num_subj);
data1 = data(individual_perm(1:(num_subj/2)), :);
labels1 = labels(individual_perm(1:(num_subj/2)));
data2 = data(individual_perm((num_subj/2) + 1: num_subj), :);
labels2 = labels(individual_perm((num_subj/2) + 1: num_subj));


%% 2. Run Combi-Method and identify top_k best SNPs
% string data to feature_matrix
featmat1 = string_to_featmat(data1);
featmat1 = double(featmat1);
featmat1 = center(featmat1);
featmat1 = scale(featmat1, pnorm_feature_scaling);

% SVM training
res1 = train_liblinear(featmat1, labels1, svm_rep, Cs, p, classy);

% Postprocessing with Moving Average Filter
w_improved1 = filter_pnorm(res1.w, filter_window_size, p_pnorm_filter);

% Generate rankings
[~, ranking1] = sort(w_improved1, 'descend');
selector = ranking_to_selector(ranking1, top_k);


%% 3. For those SNPs compute p-values on the second half of the data

% Compute p-values Bonferoni
pvalues_final = chi_square_goodness_of_fit_test(data2, labels2);
%pvalues_final = min(pvalues_final*top_k, 1);

pvalues_final = process_pvalues(pvalues_final, selector);
