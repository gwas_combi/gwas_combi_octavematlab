% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_svr( X, y, rep, Cs, tube_eps, p, classy, acc_eps )

% function result = train_liblinear( X, y, rep,  )


% :( :(
X = double(X);
% should be fixed to limit memory requirements...


% PARAMETERS
if ~exist('rep'), rep = 10; end
if ~exist('Cs'), Cs = 0.01; end
%if ~exist('tube_eps'), tube_eps = logspace(-3,1,5); end
if ~exist('tube_eps'), tube_eps = 0.01; rep=1; end
if ~exist('p'),  p = 1; end
if ~exist('acc_eps'), 
  acc_eps = 1e-3;
end



% INITIALIZATION 
num_data = length(y);
num_feat = size(X,1);
mae_val = zeros(rep,length(Cs));
mae_test = zeros(rep,length(Cs));
w_val =  zeros(rep,num_feat,length(Cs));
w =  zeros(rep,num_feat);
ranking =  zeros(rep,num_feat,length(Cs));
K = X'*X;



% VALIDATION LOOP
idx = 0;
tic;
for i = 1:rep

	% TRAIN / VAL SPLITS
	perm = randperm(num_data);
	divtr = perm(1:floor(num_data/2));
	divval = perm(floor(num_data/2)+1:floor(num_data*3/4));
	divte = perm(floor(num_data*3/4)+1:end);	
    for j = 1:length(Cs)
	  C = Cs(j);
	  for k = 1:length(tube_eps)
	    tube_epsilon = tube_eps(k);        
      
        % SVM TRAINING
	    sg('loglevel', 'ERROR');     % 'ERROR' or 'ALL'
		sg('set_kernel', 'CUSTOM', K(divtr,divtr),'FULL');
	    sg('set_labels', 'TRAIN', y(divtr));
		sg('new_regression', 'SVRLIGHT');
		sg('svr_tube_epsilon', tube_epsilon);
	    sg('c', C);
	    sg('train_regression');
        [b,alpha]=sg('get_classifier');
		Xtr = X(:,divtr);
		w_ = Xtr(:,alpha(:,2)+1)*alpha(:,1);
        w_val(i,:,j,k) = abs(w_')/norm(w_);
		
        % SVM TESTING 
        out = alpha(:,1)'*K(alpha(:,2)+1,divval) + b;
		mae_val(i,j,k) = mean(abs(y(divval)-out));
        out = alpha(:,1)'*K(alpha(:,2)+1,divte) + b;
		mae_test(i,j,k) = mean(abs(y(divte)-out));
		%plot(y(divte),out,'x')  %sanity check

		idx = idx + 1;
		print_progress(idx,rep*length(Cs)*length(tube_eps));
		
	  end	
    end
end
clear Xtr w_ Kval Kte;


% MODEL SELECTION
[aux ind_C] = min(mean(mae_val,1),[],2);
[foo ind_tube] = min(aux);
ind_C = ind_C(ind_tube);
mae_test = mean(mae_test(:,ind_C,ind_tube),1);
C = Cs(ind_C);
tube_epsilon = tube_eps(ind_tube);



% SVM TRAINING ON FULL DATA
sg('set_kernel', 'CUSTOM', K,'FULL');
sg('set_labels', 'TRAIN', y);
sg('new_regression', 'SVRLIGHT');
sg('svr_tube_epsilon', tube_epsilon);
sg('c', C);
sg('train_regression');
[b,alpha]=sg('get_classifier');
w_ = X(:,alpha(:,2)+1)*alpha(:,1);
w = abs(w_')/norm(w_);


% COMPUTE RANKINGS
%p = 1;
w = (sum(abs(reshape(w(:)',3,num_feat/3)).^p)).^(1/p); 
%w = sum(reshape(w(:)',3,num_feat/3),1);
w = w/norm(w);
%[foo ranking] = sort(w,'descend');
w_avg = median(w_val(:,:,ind_C,ind_tube),1);
w_avg = w_avg/norm(w_avg);
%[foo, ranking_avg] = sort(w_avg,'descend');


% COLLECT RESULTS
% result = struct('w_avg',w_avg(:)','w',w(:)','mae_val',mae_val,'mae_test',mae_test,'Cs',Cs,'C',C,'rep',rep,'classy',classy,'time',time,'time_val',time_val,'mae_eps',mae_eps);
result = struct('w_avg',w_avg(:)','w',w(:)','mae_val',squeeze(mean(mae_val,1)),'mae_test',mae_test,'Cs',Cs,'C',C,'tube_epsilons',tube_eps,'tube_eps_opt',tube_epsilon,'rep',rep,'acc_eps',acc_eps);


% CLEAN UP MEMORY
sg('clean_kernel', 'TRAIN');
