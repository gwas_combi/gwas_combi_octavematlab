% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function K = kscale(K, varargin)
% Scales a kernel matrix K (or kernel matrices K) to unit variance
%
% Synopsis:
%   K = kscale(K);
% 
% Arguments: 
%   K:     kernel matrix/matrices
%
% Return:
%   X:     kernel matrix
%
% Example: (scale(X)' * scale(X)) == kscale(X' * X)
%
% Author: Marius Kloft

properties = propertylist2struct(varargin{:});
properties = set_defaults(properties, 'centeringANDscalekurtosis', 0);

if (properties.centeringANDscalekurtosis)
	if (ndims(K) == 2)
		K = kcenter(K);
		K = K ./ mean(diag(K) .* diag(K));
	elseif (ndims(K) == 3)
		for i = 1:size(K, 3)
			K(:, :, i) = kcenter(K(:, :, i));
			K(:, :, i) = K(:, :, i) ./ mean(diag(K(:, :, i)) .* diag(K(:, :, i)));
		end
	end
else
	if (ndims(K) == 2)
		K = K ./ (mean(diag(K)) - mean(K(:)));
	elseif (ndims(K) == 3)
		for i = 1:size(K, 3)
			Kaux = K(:, :, i);
			K(:, :, i) = Kaux ./ (mean(diag(Kaux)) - mean(Kaux(:)));
		end
	end
end
