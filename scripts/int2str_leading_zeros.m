% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function padded_str = int2str_leading_zeros(num, min_length)
% Ensures a minimum number of digits, padding with zeros in front,
% if necessary.
% examples:
% int2str_leading_zeros(9, 3) -> 009
% int2str_leading_zeros(9999, 3) -> 9999

raw_num_str = int2str(num);
num_zeros = min_length - size(raw_num_str, 1);
if (num_zeros > 0)
	padded_str = [char(48 * ones(1, num_zeros)), raw_num_str];
else
	padded_str = raw_num_str;
end

