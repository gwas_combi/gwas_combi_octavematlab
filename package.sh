#!/bin/sh

ORIG_DIR="$(pwd)"
SRC_ROOT=.
PROJECT_NAME=GWAS_COMBI_method_OctaveMatlab
BUILD_DIR=.
SCRIPTS_DIR="${BUILD_DIR}/scripts"
#DATA_DIR="${BUILD_DIR}/data"

# Copy over the data
#cp -r "${SRC_ROOT}/Load_new_data/data" "${DATA_DIR}"

# Convert the README to HTML
pandoc -t html "${BUILD_DIR}/README.markdown" > "${BUILD_DIR}/README.html"

# Create archives of the whole thing
CODE_HISTORY_ENTRY="$(git describe --always)"
TIME_ID=$(date +"%Y_%m_%d__%H_%M")
ARCHIVE_FILE_NAME_BASE="${PROJECT_NAME}_${TIME_ID}_${CODE_HISTORY_ENTRY}"

# Create a bare archive, containing only scripts and info files.
# This should be shipped to 3rd parties.
ARCHIVE_FILE_NAME="${ARCHIVE_FILE_NAME_BASE}.zip"
zip \
	--exclude \
		"package.sh" \
		".git*" \
		"unused_scripts/*" \
		"*~" \
		"*.zip" \
		"*.mat" \
		"*.mex*" \
		"*.bed" \
		"*.fam" \
		"*.bim" \
		"*.ped" \
		"*.map" \
	--recurse-paths \
	"${ARCHIVE_FILE_NAME}" \
	"${BUILD_DIR}"

echo ""

# Create an archive that already contains the data in the PLink binary format,
# and binary libLinear Octave & Matlab files for Linux 64bit ("*.mex*").
# This should only be used group-internally, and not be shipped to others,
# because they may use other architecture systems, and may want to use other data.
ARCHIVE_FILE_NAME_ENRICHED="${ARCHIVE_FILE_NAME_BASE}_enriched.zip"
zip \
	--exclude \
		"package.sh" \
		".git*" \
		"unused_scripts/*" \
		"*~" \
		"*.zip" \
		"*.mat" \
		"*.ped" \
		"*.map" \
	--recurse-paths \
	"${ARCHIVE_FILE_NAME_ENRICHED}" \
	"${BUILD_DIR}"

echo ""
echo "* Created archive \"${ARCHIVE_FILE_NAME}\""
echo "* Created archive \"${ARCHIVE_FILE_NAME_ENRICHED}\" (CAUTION: do not ship to others!)"
echo ""

cd "${ORIG_DIR}"

