% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%% Load labels and data
load([datadir 'labels.mat']);
labels = y; clear y;
data = load([datadir 'chromo_' int2str_leading_zeros(chromo,2) '.mat']);
data = data.X;

%%% Initialize
p_lowest = zeros(1,B);

%%% Calculate feature matrix
featmat = string_to_featmat( data );
featmat = center(featmat);
featmat = scale(featmat,pnorm_feature_scaling);

% Permutation Test
result = permtest_libsvm( featmat,data, labels, B, Cs, p_svm, classy, svm_epsilon, filter_window_size, p_pnorm_filter, top_k, alpha_sig_FWER, alpha_sig_EV );

% Assign results
pvalue_combi_filtered_all = result.all_pvalues;
p_lowest = result.p_lowest;
t_star = result.t_star;
t_star_EV = result.t_star_EV;

% Clear workspace
clearvars featmat data labels result;