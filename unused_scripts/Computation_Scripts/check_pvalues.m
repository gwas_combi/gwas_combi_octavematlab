% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%load('/home/bmieth/svn/iew/data/wtccc/BipolarDisorder/wtccc_BD.mat', 'rs_ident', 'chromos')
%addpath('/home/bmieth/svn/iew/scripts/marius/computation_scripts/') 

chr_now =16;
rs_ident_now = 'rs17221417',
disease_now = 'CrohnsDisease';

load(['/home/bmieth/svn/iew/data/wtccc/',disease_now,'/labels.mat'])
load(['/home/bmieth/svn/iew/data/wtccc/',disease_now,'/chromo_',int2str_leading_zeros(chr_now,2),'.mat'])

asympt_p_chi =  chi_square_goodness_of_fit_test(X,y);
asympt_p_trend =  cochran_armitage_trend_test(X,y);
pos=find(ismember(rs_ident, rs_ident_now)==1)-find(chromos==chr_now,1)+1;
asympt_p_trend(pos)
asympt_p_chi(pos)


return;


disease_now = 'BipolarDisorder';

length_now = 1;
start_points = 1;

asympt_p_chi=[];
asympt_p_trend=[];
for chromo =1:22
	chromo
	load(['/home/bmieth/svn/iew/data/wtccc/',disease_now,'/labels.mat'])
	load(['/home/bmieth/svn/iew/data/wtccc/',disease_now,'/chromo_',int2str_leading_zeros(chromo,2),'.mat'])
	
	asympt_p_chi= [asympt_p_chi,multiple_testing_chi(X,y)];
	asympt_p_trend =  [asympt_p_trend, multiple_testing_trend(X,y)];
	start_points = [start_points, length(asympt_p_trend)+length_now];
end

pvalue_mtest = asympt_p_trend;
figure(1)
%%% Chromosomewise plot of original p-values 
subplot(2,1,1)
%plot(-log10(pvalue_mtest), '.')
for i = 1:22
   seq = start_points(i):(start_points(i+1)-1);

  if(mod(i, 2))
    plot(seq,-log10(pvalue_mtest(seq)), '.','color', [0 0.75 0]);
  else
    plot(seq,-log10(pvalue_mtest(seq)), '.b');
  end
  hold on;
  plot(seq(-log10(pvalue_mtest(seq))>=5),-log10(pvalue_mtest(seq(-log10(pvalue_mtest(seq))>=5))), 'om', 'Linewidth', 2);
  text(start_points(i), -0.7,num2str(i));
end

%for i = 1:22
% line([start_points(i), start_points(i)], [0, 0.006],  'Color', [0.9,0.9,0.9])
% text(start_points(i), -0.5,num2str(i))
%end
line([0, start_points(end)], [5 5], 'Color', 'm', 'Linewidth', 0.5, 'Linestyle', '--');
ax =axis;
axis([0,start_points(end),0,15])
set(gca,'xtick',[]);
hold off; 
title('P-values Multiple Testing (constant threshold)', 'FontSize',20);
ylabel('-log10(p-value)');
xlabel('Chromosome-wise position of SNPs');

text(0, 17.5, ['Genome-wide scan for ' disease_now, ', Trend Test'], 'FontSize',25)




pvalue_mtest = asympt_p_chi;

subplot(2,1,2)
%plot(-log10(pvalue_mtest), '.')
for i = 1:22
   seq = start_points(i):(start_points(i+1)-1);

  if(mod(i, 2))
    plot(seq,-log10(pvalue_mtest(seq)), '.','color', [0 0.75 0]);
  else
    plot(seq,-log10(pvalue_mtest(seq)), '.b');
  end
  hold on;
  plot(seq(-log10(pvalue_mtest(seq))>=5),-log10(pvalue_mtest(seq(-log10(pvalue_mtest(seq))>=5))), 'om', 'Linewidth', 2);
  text(start_points(i), -0.7,num2str(i));
end

%for i = 1:22
% line([start_points(i), start_points(i)], [0, 0.006],  'Color', [0.9,0.9,0.9])
% text(start_points(i), -0.5,num2str(i))
%end
line([0, start_points(end)], [5 5], 'Color', 'm', 'Linewidth', 0.5, 'Linestyle', '--');
ax =axis;
axis([0,start_points(end),0,15])
set(gca,'xtick',[]);
hold off; 
title('P-values Multiple Testing (constant threshold)', 'FontSize',20);
ylabel('-log10(p-value)');
xlabel('Chromosome-wise position of SNPs');

text(0, 17.5, ['Genome-wide scan for ' disease_now, ', Chi-square Test'], 'FontSize',25)