% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% Directories: Please change to yours

save_dir = '/home/bmieth/svn/iew/qsub/wtccc_crohn/results/IEW/computation_results_2000er/';
data_file = '/home/bmieth/svn/iew/data/IEW/data2000/iew2000.mat';


%% Parameters

chrom_to_plot=1:22; %1:22
phenotype = 'prosocA'; % 'prosocA', 'med_rrp_gains', 'med_rrp_losses', 'timepref1', 'timepref2'
class_or_reg = 1; % 1 = Classification, 2 = Regression
save_results = 1; % Do you want to save a list of identifiers of snps (cottages and towers)?

%%% Paramters for post-processing
top_k = 100;
filter_window_size = 35; % 1-41, odd!!!
p_pnorm_filter = 5; % 1,2,4,100, 0.25, 0.5
filter_window_size_mtest = 9;  % 1-41, odd!!!
p_pnorm_filter_mtest = 6;   % 1,2,4,100, 0.25, 0.5


%% Plots

if class_or_reg == 1
	name_str=[phenotype, ' - SVM Classification'];
	savedir = [save_dir, phenotype, '/classification/'];
	savedir_tstar = [save_dir, phenotype,'/classification/permtest_classification/'];
	savedir_tables_main = [save_dir, phenotype,'/classification/']; % where tables should be saved if save_results == 1

elseif class_or_reg == 2
	name_str = [phenotype, ' - SVM Regression'];
	savedir = [save_dir, phenotype, '/with_pvalues/'];
	savedir_tstar = [save_dir, phenotype, '/permtest/'];
	savedir_tables_main = [save_dir, phenotype,'/']; % where tables should be saved if save_results == 1
end
savefile_const_sig =[savedir_tables_main, phenotype, '_significant_SNPs.txt'];
savefile_const_insig=[savedir_tables_main, phenotype, '_insignificant_SNPs.txt'];

t_star_all = zeros(1, length(chrom_to_plot));%0.00001 % t_star aus permuation_test
%t_star_all = repmat(0.00001,1, length(chrom_to_plot));

%%% Loading data and Postprocessing
w = cell(chrom_to_plot, 1);
w_filtered = cell(chrom_to_plot, 1);
pvalue_mtest = cell(chrom_to_plot, 1);
mtest_scores_filtered = cell(chrom_to_plot, 1);
ranking_SVM = cell(chrom_to_plot, 1);
ranking_SVM_filtered = cell(chrom_to_plot, 1);
ranking_mtest_filtered = cell(chrom_to_plot, 1);
pvalue_combi = cell(chrom_to_plot, 1);
pvalue_combi_filtered = cell(chrom_to_plot, 1);
pvalue_mtest_filtered = cell(chrom_to_plot, 1);
for chromo=chrom_to_plot
	%%% load results
	savefile = [savedir 'result_' int2str_leading_zeros(chromo,2) '.mat'];
	load(savefile, 'w_', 'pvalue_mtest_');
	%svm_training_time{chromo} = svm_training_time_;
	w{chromo} = w_;
	%w_avg{chromo} = w_avg_;
	%C(chromo) = C_;
	pvalue_mtest{chromo} = pvalue_mtest_;

	%%% filter postprocessing of w %%%
	w_filtered_ = filter_pnorm(w_,filter_window_size,p_pnorm_filter);
	mtest_scores_filtered_ = filter_pnorm(-log10(pvalue_mtest_),filter_window_size_mtest,p_pnorm_filter_mtest);

	%%% compute feature ranking %%%
	[~, ranking_SVM_] = sort(w_, 'descend');
	[~, ranking_SVM_filtered_] = sort(w_filtered_, 'descend');
	[~, ranking_mtest_filtered_] = sort(mtest_scores_filtered_, 'descend');

	%%% perform feature screening and compute new p-values %%%
	selector = ranking_to_selector(ranking_SVM_,top_k);
	selector_filtered = ranking_to_selector(ranking_SVM_filtered_,top_k);
	selector_mtest_filtered = ranking_to_selector(ranking_mtest_filtered_,top_k);
	pvalue_combi_ = process_pvalues(pvalue_mtest_,selector);
	pvalue_combi_filtered_ = process_pvalues(pvalue_mtest_,selector_filtered);
	pvalue_mtest_filtered_ = process_pvalues(pvalue_mtest_,selector_mtest_filtered);

	clear selector selector_filtered selector_mtest_filtered;

	w_filtered{chromo} = w_filtered_;
	mtest_scores_filtered{chromo} = mtest_scores_filtered_;
	ranking_SVM{chromo} = ranking_SVM_;
	ranking_SVM_filtered{chromo} = ranking_SVM_filtered_;
	ranking_mtest_filtered{chromo} = ranking_mtest_filtered_;
	pvalue_combi{chromo} = pvalue_combi_;
	pvalue_combi_filtered{chromo} = pvalue_combi_filtered_;
	pvalue_mtest_filtered{chromo} = pvalue_mtest_filtered_;

	%%% load t_star
	% if chromo~=13
	savefile = [savedir_tstar 'permtest_result' int2str_leading_zeros(chromo,2) '.mat'];
	load(savefile, 't_star');
	t_star_all(chromo)=t_star;
	%else
	% t_star_all(chromo)=0.00001;
	%end
	clear t_star;
end


%% Plots

%%% Chromosomewise plot of original p-values
start_point=1;
subplot(3,1,1)

for i = chrom_to_plot
	if(mod(i, 2))
		plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_mtest{i}), '.','color', [0 0.75 0]);
	else
		plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_mtest{i}), '.b');
	end
	hold on;
	seq = start_point:(start_point+length(pvalue_mtest{i})-1);
	plot(seq((-log10(pvalue_mtest{i})>=5)),-log10(pvalue_mtest{i}((-log10(pvalue_mtest{i})>=5))), 'om', 'Linewidth', 2);
	text(start_point, -0.7,num2str(i));

	start_point = start_point + length(pvalue_mtest{i});

end
%ax = axis;
line([0, start_point], [5 5], 'Color', 'm', 'Linewidth', 0.5, 'Linestyle', '--');
axis([0,start_point,0,15])
set(gca,'xtick',[]);
hold off;
title('Multiple Testing (constant threshold)', 'FontSize',20);
ylabel('-log10(p-value)');
xlabel('Chromosome-wise position of SNPs');

text(0, 17.5, ['Genome-wide scan for ' name_str], 'FontSize',25)





%%% Chromosomewise plot of SVM + Filter Screening p-values
start_point=1;
subplot(3,1,2)
selected_sig_pos = cell(chrom_to_plot, 1);
selected_pos = cell(chrom_to_plot, 1);
pvalue_combi_filtered_only_sig = cell(chrom_to_plot, 1);
locs_sig_in_chr = cell(chrom_to_plot, 1);
pks = cell(chrom_to_plot, 1);
locs_in_chr = cell(chrom_to_plot, 1);
locs = cell(chrom_to_plot, 1);
locs_sig = cell(chrom_to_plot, 1);
for i = chrom_to_plot
	if(mod(i, 2))
		plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_combi_filtered{i}), '.','color', [0 0.75 0]);
	else
		plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_combi_filtered{i}), '.b');
	end
	hold on;
	seq = start_point:(start_point+length(pvalue_mtest{i})-1);
	plot(seq((-log10(pvalue_combi_filtered{i})>=-log10(t_star_all(i)))),-log10(pvalue_combi_filtered{i}((-log10(pvalue_combi_filtered{i})>=-log10(t_star_all(i))))), 'om', 'Linewidth', 2);
	text(start_point, -0.7,num2str(i));

	selected_sig_pos{i} = start_point -1 +find(-log10(pvalue_combi_filtered{i})>=-log10(t_star_all(i)));
	selected_pos{i} = start_point -1 +find(-log10(pvalue_combi_filtered{i})>0);

	pvalue_combi_filtered_only_sig{i} = pvalue_combi_filtered{i};
	pvalue_combi_filtered_only_sig{i}((-log10(pvalue_combi_filtered{i})<-log10(t_star_all(i))))=1;

	[~,locs_sig_in_chr{i}] = findpeaks(-log10(pvalue_combi_filtered_only_sig{i}), 'minpeakdistance',150);
	[pks{i},locs_in_chr{i}] = findpeaks(-log10(pvalue_combi_filtered{i}), 'minpeakdistance',150);

	locs{i}=start_point-1+locs_in_chr{i};
	locs_sig{i}=start_point-1+locs_sig_in_chr{i};

	line([start_point, start_point+length(pvalue_mtest{i})-1], [-log10(t_star_all(i)), -log10(t_star_all(i))], 'Color', 'm', 'Linewidth', 0.5, 'Linestyle', '--');

	start_point = start_point + length(pvalue_mtest{i});
end

%ax = axis;
axis([0,start_point,0,15])
set(gca,'xtick',[]);
hold off;
title('Combi Method (chromosomewise threshold from permutation test)', 'FontSize',20);
ylabel('-log10(p-value)');
xlabel('Chromosome-wise position of SNPs');



%%% Chromosomewise plot of SVM weights
start_point=1;
subplot(3,1,3)
for i = chrom_to_plot

	if(mod(i, 2))
		plot(start_point:(start_point+length(pvalue_mtest{i})-1),w{i}*sqrt(length(w{i})), '.','color', [0 0.75 0]);
	else
		plot(start_point:(start_point+length(pvalue_mtest{i})-1),w{i}*sqrt(length(w{i})), '.b');
	end
	hold on;
	text(start_point, -0.5,num2str(i))
	seq = start_point:(start_point+length(pvalue_mtest{i})-1);

	start_point = start_point + length(pvalue_mtest{i});

end

ax =axis;
axis([0,start_point,ax(3), ax(4)]);
set(gca,'xtick',[]);
hold off;
title('SVM weights', 'FontSize',20);
ylabel('SVM weights');
xlabel('Chromosome-wise position of SNPs');



if save_results

	clear selected_sig_id selected_id representatives representatives_orderd representatives_sig_orderd representatives_sig;

	load(data_file, 'chromos', 'rs_ident')
	rs_ident_now=rs_ident((chromos~=0 & chromos~=23 & chromos~=24 & chromos~=25 & chromos~=26 ));
	chromo_now=chromos(chromos~=0 & chromos~=23 & chromos~=24 & chromos~=25 & chromos~=26 );

	pvalue_repres = cell(22, 1);
	pvalue_repres_sig = cell(22, 1);
	pvalue_repres_ordered=[];
	pvalue_repres_sig_ordered=[];
	representatives = cell(22, 1);
	w_repres = cell(22, 1);
	representatives_sig{i} = cell(22, 1);
	w_repres_sig{i} = cell(22, 1);
	selected_sig_id = cell(22, 1);
	selected_id = cell(22, 1);


	for i=1:22
		pvalue_repres{i}=[];
		pvalue_repres_sig{i}=[];

		% Delete significant SNPs in the representatives (insignificant)
		if length(locs_sig{i})>=1
			to_del=[];
			for j=1:length(locs_sig{i})
				for l = 1:length(locs{i})
					if locs_sig{i}(j)==locs{i}(l)
						to_del=[to_del,l];
					end
				end
			end
			locs{i}(to_del)=[];
			locs_in_chr{i}(to_del)=[];
		end

		num_locs = length(locs{i});
		representatives{i} = cell(num_locs, 1);
		w_repres{i} = zeros(num_locs, 1);
		for j=1:num_locs
			representatives{i}{j}=rs_ident_now{locs{i}(j)};
			pvalue_repres{i}(j)=pvalue_combi_filtered{i}(locs_in_chr{i}(j));
			w_repres{i}(j)=w_filtered{i}(locs_in_chr{i}(j));
		end




		num_locs_sigs = length(locs_sig{i});
		representatives_sig{i} = cell(num_locs_sigs, 1);
		w_repres_sig{i} = zeros(num_locs_sigs, 1);
		for j=1:num_locs_sigs
			representatives_sig{i}{j}=rs_ident_now{locs_sig{i}(j)};

			pvalue_repres_sig{i}(j)=pvalue_combi_filtered{i}(locs_sig_in_chr{i}(j));
			w_repres_sig{i}(j)=w_filtered{i}(locs_sig_in_chr{i}(j));
		end
		%	if (pvalue_repres_sig{i})
		%		if length(pvalue_repres_sig{i})>=1
		%			[pvalue_repres_sig_ordered{i}, indices_sorted_sig{i}]=sort(pvalue_repres_sig{i},'ascend');
		%			for j = 1:length(indices_sorted_sig{i})
		%				representatives_sig_orderd{i}{j}=representatives_sig{i}{indices_sorted_sig{i}(j)};
		%				w_repres_sig_ordered{i}(j)=w_repres_sig{i}(indices_sorted_sig{i}(j));
		%			end
		%		end
		%	end

		selected_sig_id{i} = cell(length(selected_sig_pos{i}), 1);
		selected_id{i} = cell(length(selected_pos{i}), 1);
		for j=1:length(selected_sig_pos{i})
			selected_sig_id{i}{j}=rs_ident_now{selected_sig_pos{i}(j)};
		end
		for j=1:length(selected_pos{i})
			selected_id{i}{j}=rs_ident_now{selected_pos{i}(j)};
		end

	end

	locs_sig = cell2mat(locs_sig);
	pvalue_repres_sig = cell2mat(pvalue_repres_sig);
	w_repres_sig = cell2mat(w_repres_sig);
	counter=0;
	for i= 1:length(representatives_sig)
		if ~isempty(representatives_sig{i})
			for j = 1:length(representatives_sig{i})
				counter=counter+1;
				rep_sig_now{counter}= representatives_sig{i}{j};
			end
		end
	end

	representatives_sig= rep_sig_now;

	locs = cell2mat(locs);
	pvalue_repres = cell2mat(pvalue_repres);
	w_repres = cell2mat(w_repres);
	counter=0;
	for i= 1:length(representatives)
		if ~isempty(representatives{i})
			for j = 1:length(representatives{i})
				counter=counter+1;
				rep_now{counter}= representatives{i}{j};
			end
		end
	end

	representatives= rep_now;

	if length(pvalue_repres)>=1
		[pvalue_repres_ordered, indices_sorted]=sort(pvalue_repres,'ascend');
		num_indices = length(indices_sorted);
		representatives_orderd = cell(num_indices, 1);
		w_repres_ordered = zeros(num_indices, 1);
		for j = 1:num_indices
			representatives_orderd{j}=representatives{indices_sorted(j)};
			w_repres_ordered(j)=w_repres(indices_sorted(j));
		end
	end

	if length(pvalue_repres_sig)>=1
		[pvalue_repres_sig_ordered, indices_sorted_sig]=sort(pvalue_repres_sig,'ascend');
		num_indices = length(indices_sorted_sig);
		representatives_sig_orderd = cell(num_indices, 1);
		w_repres_sig_ordered = zeros(num_indices, 1);
		for j = 1:num_indices
			representatives_sig_orderd{j}=representatives_sig{indices_sorted_sig(j)};
			w_repres_sig_ordered(j)=w_repres_sig(indices_sorted_sig(j));
		end
	end

	%posEmptyCells = find(cellfun('isempty',representatives))
	%posEmptyCells_sig = find(cellfun('isempty',representatives_sig))



	% Write data into file

	clear d;
	% Significant SNPs
	d{1,1} = '     chromosome';
	d{2,1} = '     rs_identifier';
	d{3,1} = '     p value';
	d{4,1} = '     SVM weights';
	for j = 1:length(representatives_sig_orderd)
		d{1,j+1}= chromo_now(locs_sig(indices_sorted_sig(j)));
		d{2,j+1} = representatives_sig_orderd{j};
		d{3,j+1} = pvalue_repres_sig_ordered(j);
		d{4,j+1} = w_repres_sig_ordered(j);
	end
	d2 = cellfun(@ensure_str,d,'UniformOutput',0);
	size_d2 = cellfun(@length,d2,'UniformOutput',0);
	str_length = max(max(cell2mat(size_d2)));
	d3 = cellfun(@(x) ensure_str_len(x,str_length),d2,'uniformoutput',0);
	d4 = cell2mat(d3');


	fid = fopen(savefile_const_sig,'wt');
	for i = 1:size(d4,1)
		fprintf(fid,'%s\n',d4(i,:));
	end
	fclose(fid);

	% Insignificant SNPs
	clear d;

	d{1,1} = '     chromosome';
	d{2,1} = '     rs_identifier';
	d{3,1} = '     p value';
	d{4,1} = '     SVM weights';

	for j = 1:length(representatives_orderd)
		d{1,j+1}= chromo_now(locs(indices_sorted(j)));
		d{2,j+1} = representatives_orderd{j};
		d{3,j+1} = pvalue_repres_ordered(j);
		d{4,j+1} = w_repres_ordered(j);
	end


	d2 = cellfun(@ensure_str,d,'UniformOutput',0);
	size_d2 = cellfun(@length,d2,'UniformOutput',0);
	str_length = max(max(cell2mat(size_d2)));
	d3 = cellfun(@(x) ensure_str_len(x,str_length),d2,'uniformoutput',0);
	d4 = cell2mat(d3');


	fid = fopen(savefile_const_insig,'wt');
	for i = 1:size(d4,1)
		fprintf(fid,'%s\n',d4(i,:));
	end
	fclose(fid);


end;

return;



%% Identify selected SNPs
% (and representatives of each tower)

clear selected_sig_id selected_id representatives;
for i=1:22
	for j=1:length(locs{i})
		representatives{i}{j}=rs_ident_now{locs{i}(j)};
	end
	for j=1:length(selected_sig_pos{i})
		selected_sig_id{i}{j}=rs_ident_now{selected_sig_pos{i}(j)};
	end
	for j=1:length(selected_pos{i})
		selected_id{i}{j}=rs_ident_now{selected_pos{i}(j)};
	end
end



find(~cellfun('isempty', strfind(selected_id{1},'rs6534347')))


sum=0;
for i = 1:22
	sum= size(pvalue_combi_filtered{1},2)+sum;
end

% Check if some snp was selected
find(ismember(selected_id{10}, '61961519')==1)
find(ismember(rs_ident, 'rs10458597')==1)

[pks,locs] = findpeaks(data);
