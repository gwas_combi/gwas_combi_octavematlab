With these two matlab scripts you can read new data and convert it into the appropriate matlab-format. 

The input data has to be supplied in 3 files with the following extensions: .fam, .bim, .bed (saved in 'savedir_data')
Thogether they make up a data-set in the PLink Binary format.

You also get a number of files containing the phenotypes with the extension: .pheno (saved in 'labels_dir')

Change the directories in load_data_bed.m (change names of phenotypes in lines 23-76 if necessary)

Run load_data.m to 
	- load labels from 'labels_dir' and save them and dichotimized labels in 'savedir_data'
	- load SNP position meta data and data matrix from 'savedir_data' and save in 'savefile'
	- extract and save chromosomewise data in 'savedir_data'

(extract_chrom.m is called within load_data.m)