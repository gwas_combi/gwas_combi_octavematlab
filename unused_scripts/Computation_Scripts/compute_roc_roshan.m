% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [tp, fp, auc] = compute_roc_roshan(roshan_score, pvalue_truth, roc_ub)
% Computes ROC curve (tp and fp are coordinates of curve,
% AUC is area under the curve) for all repetitions and a specific top_k.

% pvalue can be a matrix with two dimensions m x n
% m = number of repetitions
% n = number of SNPs
% pvalue_truth is of size 1 x n. 
% The output auc is of size m x 1.

% This function calls 'val_ROC' which computes the ROC coordinates
% and AUC for one repetition and one top_k.
% It also calls 'val_align_ROC' which aligns the coordinates
% to a smooth nice curve on the given intervall.

% If an upper bound for the False Positive Rate is not given, set it to 1
% (the whole interval is analysed).
if ~exist('roc_ub', 'var')
	roc_ub = 1;
end

% Convert pvalues into scores from which AUC can be computed
score_method = roshan_score;
score_truth = -log(pvalue_truth);

% Initialize false positive rate
fp = (0:roc_ub / 100:roc_ub);

% For each repetition calculate ROC corrdinates and AUC
auc = zeros(size(roshan_score, 1), 1); % pre-allocate memory
for i = 1:size(roshan_score, 1)
	[tp_, fp_, auc_] = val_ROC(score_method(i, :), score_truth, 'ub', roc_ub);
	roc.tp = tp_;
	roc.fp = fp_;
	auc(i) = auc_;
	tp(i, :) = val_align_ROC(roc, fp);
end
