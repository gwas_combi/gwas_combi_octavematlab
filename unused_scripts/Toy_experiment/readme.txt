These files are used to run the Toy experiment on semi-synthetic data where the genotypes come from real WTCCC data and the phenotypes are generated using a logistic regression model. 

Please run the script wrapper_toy_experiment.m after you have adjusted the settings in parameters_complete.m and the directories in wrapper_toy_experiment.m

You can use generate_plot.m if you want to only see the plots and not run the whole experiment again, then you need to manually load the results from where you saved them.