% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Parameters             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

chromos = [1:22];
top_k = 100;

pnorm_feature_scaling = 6;  
filter_window_size = 35;
p_pnorm_filter = 5;

B=10, %B = 1000; at least 10!!!
alpha_sig=0.05;

rep=1
Cs = 0.00001 %0.00001
tube_eps = 0.001 %0.001
p_svm = 15;
