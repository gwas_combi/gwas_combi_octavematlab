% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [t_star, p_lowest] = perm_test_mtest(data, labels, rep_permtest, alpha);


%%% Initialize
p_lowest = zeros(1,rep_permtest);

for i = 1:rep_permtest
  
   %%% Permute labels
   labels_perm=zeros(1,length(labels));
   labels_perm(randperm(length(labels)))=labels;
   
   %%% Compute p-values Bonferoni %%%
   [pvalue_mtest_,chisq_value,call_rate,maf,lex_min,lex_max,tables] = multiple_testing_chi(data,labels_perm);

   %%% save lowest p-value
   p_lowest(i)=min(pvalue_mtest_);
   
   %%% clear workspace
   clearvars labels_perm pvalue_mtest_ chisq_value call_rate maf lex_min lex_max tables;
   
end

%%% Find t_star as alpha-percentile of sorted p-values
p_lowest_sorted = sort(p_lowest);
t_star = p_lowest_sorted(max(ceil(rep_permtest*alpha_sig)-1, 1));

clearvars data labels p_lowest_sorted;
