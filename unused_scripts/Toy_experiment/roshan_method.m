% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function w_final = roshan_method(pvalue_no_screening, data, labels, top_k_roshan_1, top_k_roshan_2, pnorm_feature_scaling, svm_rep, Cs, p, classy, filter_window_size, p_pnorm_filter)

% PARAMETERS

if (size(labels, 1) > size(labels, 2))
	labels = labels';
end

%num_subj = length(labels);
%num_snps = size(data, 2);


%% Generate rankings
[~, ranking] = sort(pvalue_no_screening, 'ascend');
% identify top_k best SNPs
selector = ranking_to_selector(ranking, top_k_roshan_1);

%% string data to feature_matrix
featmat = string_to_featmat(data);
featmat = double(featmat);
featmat = center(featmat);
featmat = scale(featmat,pnorm_feature_scaling);

%% Only use the top_k SNPs
featmat_new = zeros(size(featmat));
featmat_1 = featmat(1:3:end, :);
featmat_2 = featmat(2:3:end, :);
featmat_3 = featmat(3:3:end, :);
featmat_1(~selector, :) = 0;
featmat_2(~selector, :) = 0;
featmat_3(~selector, :) = 0;

featmat_new(1:3:end, :) = featmat_1;
featmat_new(2:3:end, :) = featmat_2;
featmat_new(3:3:end, :) = featmat_3;

%% SVM training
res = train_liblinear(featmat_new, labels, svm_rep, Cs, p, classy);
w = res.w;

[~, ranking] = sort(abs(w), 'descend');
selector = ranking_to_selector(ranking, top_k_roshan_2);

w_final = zeros(size(w));
w_final(logical(selector)) = w(logical(selector));
