# GWAS COMBI method Matlab/Octave scripts - README

## Requirements

* The minimum COMBI method scripts (should come together with this README)
* Example data in the PLink Binary format.
	Most easily, use the _tutorial_ data-set from 'Tutorial_GWASpi.zip',
	which you can obtain from [the GWASpi homepage](http://www.gwaspi.org/?page_id=182).
* libLinear, compiled for your system and Matlab/Octave version.
	You can get it from [the lib-linear homepage](http://www.csie.ntu.edu.tw/~cjlin/liblinear/#download).

## Instructions

1. set-up libLinear
	* get the source (see Requirements)
	* extract the source
	* start Matlab/Octave
	* `cd libLinearSrcDir/matlab`
	* run in Matlab/Octave: `make`
		* (_NOTE_: For this to work in Octave, you need the optional package 'liboctave-dev' installed)
	* copy over the generated '\*.mex\*' files to the 'scripts/matlab/' or 'scripts/octave/' directory.
2. prepare the data: extract the archive (see Requirements) into 'data/'
3. if you do not use the suggested data, adjust the 'combi\_run\_with\_tutorial\_matrix.m' script with the correct file names.
4. in Matlab/Octave, `cd` to the directory containing 'combi.m'
5. eventually adjust the parameters for the COMBI method in the file 'combi\_set\_params.m'
6. run in Matlab/Octave: `combi_run_with_tutorial_matrix;`

