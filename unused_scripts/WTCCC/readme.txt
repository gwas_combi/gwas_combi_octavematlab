This folder contains all scripts to run the COMBI method on WTCCC data.

Go to the folder 'SVM_training' if you want to train SVMs.
Go to the folder 'Permutation_test' if you want to run the permutation test.

With generate_plot_and_result_lists.m you can use the results of both steps from above to generate Manahattan plots etc. and also generate txt.-files containing the significant towers and the insignificant cottages. (Please change directories in the file before you run this script)