% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% Directories: Please change to yours

script_path = '/home/bmieth/svn/iew/scripts/marius/computation_scripts';
addpath(script_path);

savedir_main = '/home/bmieth/svn/iew/scripts/marius/old_scripts/SVM_training/'; % where SVM training results are saved
savedir_tstar_main = '/home/bmieth/svn/iew/scripts/marius/old_scripts/Permutation_test/'; % where permutation test results are saved

savedir_tables_main = '/home/bmieth/svn/iew/scripts/marius/old_scripts/Permutation_test/'; % where tables should be saved if save_results == 1
savedir_original_data = '/home/bmieth/svn/iew/data/wtccc/CrohnsDisease/wtccc_CD.mat'; % where identifiers are loaded


%% Parameters

chrom_to_plot=22; %1:22
save_results = 1; % Do you want to save a list of identifiers of snps (cottages and towers)?

disease_abbs = {'BD'}; % {'BD', 'CAD','CD','HT','RA','T1D','T2D'};
diseases = {'Bipolar disorder'}; % {'Bipolar disorder', 'Coronary artery disease','Crohns disease','Hypertension','Rheumatoid arthritis','Type 1 Diabetes','Type 2 diabetes'};


%% Generate data

for disease_count = 1: length(disease_abbs)
	disease_abb = disease_abbs{disease_count};
	savedir = [savedir_main,disease_abb,'_'];
	savedir_tstar = [savedir_tstar_main,disease_abb,'_'];

	save_tables = [savedir_tables_main,disease_abb];
	savefile_const_sig =[save_tables, '_chromosome_significant_SNPs.txt'];
	savefile_const_insig=[save_tables, '_chromosome_insignificant_SNPs.txt'];

	%%% Paramters for post-processing
	top_k = 100;
	filter_window_size = 35; % 1-41, odd!!!
	p_pnorm_filter = 5; % 1,2,4,100, 0.25, 0.5
	filter_window_size_mtest = 9;  % 1-41, odd!!!
	p_pnorm_filter_mtest = 6;   % 1,2,4,100, 0.25, 0.5

	t_star_all = zeros(1, length(chrom_to_plot));%0.00001 % t_star aus permuation_test


	%%% Loading data and Postprocessing
	w = cell(chrom_to_plot, 1);
	w_filtered = cell(chrom_to_plot, 1);
	C = cell(chrom_to_plot, 1);
	pvalue_mtest = cell(chrom_to_plot, 1);
	mtest_scores_filtered = cell(chrom_to_plot, 1);
	ranking_SVM = cell(chrom_to_plot, 1);
	ranking_SVM_filtered = cell(chrom_to_plot, 1);
	ranking_mtest_filtered = cell(chrom_to_plot, 1);
	pvalue_combi = cell(chrom_to_plot, 1);
	pvalue_combi_filtered = cell(chrom_to_plot, 1);
	pvalue_mtest_filtered = cell(chrom_to_plot, 1);
	for chromo=chrom_to_plot
		%%% load results
		savefile = [savedir 'result_' int2str_leading_zeros(chromo,2) '.mat'];
		load(savefile, 'w_', 'pvalue_mtest_',  'C_','classy');
		w{chromo} = w_;
		C(chromo) = C_;
		pvalue_mtest{chromo} = pvalue_mtest_;

		% HACK non-modular code
		% For CAD, delete the highly significant SNPs that are not correct.
		% This was discussed with Arcadi.
		% Those SNPs are only significant due to some data processing mistake.
		% The COMBI method automatically discards them.
		% So we simply delete them in the reference data to have a nicer plot.
		if disease_count==2
			if chromo ~=9
				pvalue_mtest{chromo}(-log10(pvalue_mtest{chromo})>6)=1;
			end
		end

		%%% filter postprocessing of w %%%
		w_filtered_ = filter_pnorm(w_,filter_window_size,p_pnorm_filter);
		w_filtered{chromo}=w_filtered_;
		mtest_scores_filtered_ = filter_pnorm(-log10(pvalue_mtest_),filter_window_size_mtest,p_pnorm_filter_mtest);

		%%% compute feature ranking %%%
		[~, ranking_SVM_] = sort(w_, 'descend');
		[~, ranking_SVM_filtered_] = sort(w_filtered_, 'descend');
		[~, ranking_mtest_filtered_] = sort(mtest_scores_filtered_, 'descend');

		%%% perform feature screening and compute new p-values %%%
		selector = ranking_to_selector(ranking_SVM_,top_k);
		selector_filtered = ranking_to_selector(ranking_SVM_filtered_,top_k);
		selector_mtest_filtered = ranking_to_selector(ranking_mtest_filtered_,top_k);
		pvalue_combi_ = process_pvalues(pvalue_mtest_,selector);
		pvalue_combi_filtered_ = process_pvalues(pvalue_mtest_,selector_filtered);
		pvalue_mtest_filtered_ = process_pvalues(pvalue_mtest_,selector_mtest_filtered);

		clear selector selector_filtered selector_mtest_filtered;

		mtest_scores_filtered{chromo} = mtest_scores_filtered_;
		ranking_SVM{chromo} = ranking_SVM_;
		ranking_SVM_filtered{chromo} = ranking_SVM_filtered_;
		ranking_mtest_filtered{chromo} = ranking_mtest_filtered_;
		pvalue_combi{chromo} = pvalue_combi_;
		pvalue_combi_filtered{chromo} = pvalue_combi_filtered_;
		pvalue_mtest_filtered{chromo} = pvalue_mtest_filtered_;

		%%% load t_star
		savefile = [savedir_tstar 'permtest_result_' int2str_leading_zeros(chromo,2) '.mat'];
		load(savefile, 't_star');
		t_star_all(chromo)=t_star;
		clear t_star;
	end

	%% Plots

	%%% Chromosomewise plot of original p-values
	start_point=1;
	subplot(7,2,2*(disease_count-1)+1)

	length_chr = zeros(chrom_to_plot, 1);
	for i = chrom_to_plot
		if(mod(i, 2))
			plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_mtest{i}), '.','color', [0 0 0.5]);
		else
			plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_mtest{i}), '.', 'color', [0.4 0.4 0.8]);
		end
		hold on;
		seq = start_point:(start_point+length(pvalue_mtest{i})-1);
		plot(seq((-log10(pvalue_mtest{i})>=5)),-log10(pvalue_mtest{i}((-log10(pvalue_mtest{i})>=5))), '.', 'Linewidth', 2,'color', [0 0.8 0]);
		text(start_point+(length(pvalue_mtest{i})/2), -1,num2str(i),'rotation',270);

		set(gca,'color','none')
		if disease_count ==7 && i == 7
			text(start_point, -5,'Chromosome');
		end
		start_point = start_point + length(pvalue_mtest{i});
		length_chr(i) = length(pvalue_mtest{i});

	end
	line([0, start_point], [5 5], 'Color', 'k', 'Linewidth', 0.5, 'Linestyle', '--');
	%ax = axis;
	axis([0,start_point,0,15])
	set(gca,'xtick',[]);
	set(gca,'box','off')
	set(gca,'TickDir','out')
	hold off;
	if disease_count==1
		title('Raw p-value thresholding ', 'FontSize',20);
	end
	if disease_count ==4
		ylabel('-log_{10}(P)');
	end


	text(-150000, 7.5, diseases{disease_count}, 'FontSize',12, 'FontWeight', 'bold')





	%%% Chromosomewise plot of SVM + Filter Screening p-values
	start_point=1;
	subplot(7,2,2*(disease_count-1)+2)
	clear pks locs;

	selected_sig_pos = cell(chrom_to_plot, 1);
	selected_pos = cell(chrom_to_plot, 1);
	pvalue_combi_filtered_only_sig = cell(chrom_to_plot, 1);
	locs_sig_in_chr = cell(chrom_to_plot, 1);
	pks = cell(chrom_to_plot, 1);
	locs_in_chr = cell(chrom_to_plot, 1);
	locs = cell(chrom_to_plot, 1);
	locs_sig = cell(chrom_to_plot, 1);
	for i = chrom_to_plot
		if(mod(i, 2))
			plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_combi_filtered{i}), '.','color', [0 0 0.7]);
		else
			plot(start_point:(start_point+length(pvalue_mtest{i})-1),-log10(pvalue_combi_filtered{i}), '.','color', [0.4 0.4 0.8]);
		end
		hold on;
		set(gca,'color','none')
		seq = start_point:(start_point+length(pvalue_mtest{i})-1);
		plot(seq((-log10(pvalue_combi_filtered{i})>=-log10(t_star_all(i)))),-log10(pvalue_combi_filtered{i}((-log10(pvalue_combi_filtered{i})>=-log10(t_star_all(i))))), '.g', 'Linewidth', 2,'color', [0 0.8 0]);
		if disease_count ==7 && i == 7
			text(start_point, -5,'Chromosome');
		end
		text(start_point+(length(pvalue_mtest{i})/2), -1,num2str(i),'rotation',270);

		selected_sig_pos{i} = start_point -1 +find(-log10(pvalue_combi_filtered{i})>=-log10(t_star_all(i)));
		selected_pos{i} = start_point -1 +find(-log10(pvalue_combi_filtered{i})>0);

		pvalue_combi_filtered_only_sig{i} = pvalue_combi_filtered{i};
		pvalue_combi_filtered_only_sig{i}((-log10(pvalue_combi_filtered{i})<-log10(t_star_all(i))))=1;

		[~,locs_sig_in_chr{i}] = findpeaks(-log10(pvalue_combi_filtered_only_sig{i}), 'minpeakdistance',150);
		[pks{i},locs_in_chr{i}] = findpeaks(-log10(pvalue_combi_filtered{i}), 'minpeakdistance',150);

		locs{i}=start_point-1+locs_in_chr{i};
		locs_sig{i}=start_point-1+locs_sig_in_chr{i};

		line([start_point, start_point+length(pvalue_mtest{i})-1], [-log10(t_star_all(i)), -log10(t_star_all(i))], 'Color', 'k', 'Linewidth', 0.5, 'Linestyle', '--');

		start_point = start_point + length(pvalue_mtest{i});
	end

	%ax = axis;
	axis([0,start_point,0,15])
	set(gca,'xtick',[]);
	set(gca,'box','off')
	set(gca,'TickDir','out')
	hold off;
	if disease_count ==1
		title('COMBI method', 'FontSize',20);
	end
	if disease_count ==4
		ylabel('-log_{10}(P)');
	end
	%if disease_count ==7
	%	xlabel('Chromosome-wise position of SNPs');
	%end
end




if save_results
	clear selected_sig_id selected_id representatives representatives_orderd representatives_sig_orderd representatives_sig;

	load(savedir_original_data, 'rs_ident', 'chromos')
	rs_ident_now=rs_ident((chromos~=0 & chromos~=23));
	chromo_now=chromos(chromos~=0 & chromos~=23);

	pvalue_repres = cell(22, 1);
	pvalue_repres_sig = cell(22, 1);
	pvalue_repres_ordered=[];
	pvalue_repres_sig_ordered=[];
	representatives = cell(22, 1);
	w_repres = cell(22, 1);
	representatives_sig{i} = cell(22, 1);
	w_repres_sig{i} = cell(22, 1);
	selected_sig_id = cell(22, 1);
	selected_id = cell(22, 1);

	for i=1:22
		pvalue_repres{i}=[];
		pvalue_repres_sig{i}=[];

		% Delete significant SNPs in the representatives (insignificant)
		if length(locs_sig{i})>=1
			to_del=[];
			for j=1:length(locs_sig{i})
				for l = 1:length(locs{i})
					if locs_sig{i}(j)==locs{i}(l)
						to_del=[to_del,l];
					end
				end
			end
			locs{i}(to_del)=[];
			locs_in_chr{i}(to_del)=[];
		end

		num_locs = length(locs{i});
		representatives{i} = cell(num_locs, 1);
		w_repres{i} = zeros(num_locs, 1);
		for j=1:num_locs
			representatives{i}{j}=rs_ident_now{locs{i}(j)};
			pvalue_repres{i}(j)=pvalue_combi_filtered{i}(locs_in_chr{i}(j));
			w_repres{i}(j)=w_filtered{i}(locs_in_chr{i}(j));
		end




		num_locs_sigs = length(locs_sig{i});
		representatives_sig{i} = cell(num_locs_sigs, 1);
		w_repres_sig{i} = zeros(num_locs_sigs, 1);
		for j=1:num_locs_sigs
			representatives_sig{i}{j}=rs_ident_now{locs_sig{i}(j)};

			pvalue_repres_sig{i}(j)=pvalue_combi_filtered{i}(locs_sig_in_chr{i}(j));
			w_repres_sig{i}(j)=w_filtered{i}(locs_sig_in_chr{i}(j));
		end
		%	if (pvalue_repres_sig{i})
		%		if length(pvalue_repres_sig{i})>=1
		%			[pvalue_repres_sig_ordered{i}, indices_sorted_sig{i}]=sort(pvalue_repres_sig{i},'ascend');
		%			for j = 1:length(indices_sorted_sig{i})
		%				representatives_sig_orderd{i}{j}=representatives_sig{i}{indices_sorted_sig{i}(j)};
		%				w_repres_sig_ordered{i}(j)=w_repres_sig{i}(indices_sorted_sig{i}(j));
		%			end
		%		end
		%	end

		selected_sig_id{i} = cell(length(selected_sig_pos{i}), 1);
		selected_id{i} = cell(length(selected_pos{i}), 1);
		for j=1:length(selected_sig_pos{i})
			selected_sig_id{i}{j}=rs_ident_now{selected_sig_pos{i}(j)};
		end
		for j=1:length(selected_pos{i})
			selected_id{i}{j}=rs_ident_now{selected_pos{i}(j)};
		end
	end

	locs_sig = cell2mat(locs_sig);
	pvalue_repres_sig = cell2mat(pvalue_repres_sig);
	w_repres_sig = cell2mat(w_repres_sig);
	counter=0;
	for i= 1:length(representatives_sig)
		if ~isempty(representatives_sig{i})
			for j = 1:length(representatives_sig{i})
				counter=counter+1;
				rep_sig_now{counter}= representatives_sig{i}{j};
			end
		end
	end

	representatives_sig= rep_sig_now;

	locs = cell2mat(locs);
	pvalue_repres = cell2mat(pvalue_repres);
	w_repres = cell2mat(w_repres);
	counter=0;
	for i= 1:length(representatives)
		if ~isempty(representatives{i})
			for j = 1:length(representatives{i})
				counter=counter+1;
				rep_now{counter}= representatives{i}{j};
			end
		end
	end

	representatives= rep_now;

	if length(pvalue_repres)>=1
		[pvalue_repres_ordered, indices_sorted]=sort(pvalue_repres,'ascend');
		num_indices = length(indices_sorted);
		representatives_orderd = cell(num_indices, 1);
		w_repres_ordered = zeros(num_indices, 1);
		for j = 1:num_indices
			representatives_orderd{j}=representatives{indices_sorted(j)};
			w_repres_ordered(j)=w_repres(indices_sorted(j));
		end
	end

	if length(pvalue_repres_sig)>=1
		[pvalue_repres_sig_ordered, indices_sorted_sig]=sort(pvalue_repres_sig,'ascend');
		num_indices = length(indices_sorted_sig);
		representatives_sig_orderd = cell(num_indices, 1);
		w_repres_sig_ordered = zeros(num_indices, 1);
		for j = 1:num_indices
			representatives_sig_orderd{j}=representatives_sig{indices_sorted_sig(j)};
			w_repres_sig_ordered(j)=w_repres_sig(indices_sorted_sig(j));
		end
	end

	%posEmptyCells = find(cellfun('isempty',representatives))
	%posEmptyCells_sig = find(cellfun('isempty',representatives_sig))



	% Write data into file

	clear d;
	% Significant SNPs
	d{1,1} = '     chromosome';
	d{2,1} = '     rs_identifier';
	d{3,1} = '     p value';
	d{4,1} = '     SVM weights';
	for j = 1:length(representatives_sig_orderd)
		d{1,j+1}= chromo_now(locs_sig(indices_sorted_sig(j)));
		d{2,j+1} = representatives_sig_orderd{j};
		d{3,j+1} = pvalue_repres_sig_ordered(j);
		d{4,j+1} = w_repres_sig_ordered(j);
	end
	d2 = cellfun(@ensure_str,d,'UniformOutput',0);
	size_d2 = cellfun(@length,d2,'UniformOutput',0);
	str_length = max(max(cell2mat(size_d2)));
	d3 = cellfun(@(x) ensure_str_len(x,str_length),d2,'uniformoutput',0);
	d4 = cell2mat(d3');


	fid = fopen(savefile_const_sig,'wt');
	for i = 1:size(d4,1)
		fprintf(fid,'%s\n',d4(i,:));
	end
	fclose(fid);

	% Insignificant SNPs
	clear d;

	d{1,1} = '     chromosome';
	d{2,1} = '     rs_identifier';
	d{3,1} = '     p value';
	d{4,1} = '     SVM weights';

	for j = 1:length(representatives_orderd)
		d{1,j+1}= chromo_now(locs(indices_sorted(j)));
		d{2,j+1} = representatives_orderd{j};
		d{3,j+1} = pvalue_repres_ordered(j);
		d{4,j+1} = w_repres_ordered(j);
	end


	d2 = cellfun(@ensure_str,d,'UniformOutput',0);
	size_d2 = cellfun(@length,d2,'UniformOutput',0);
	str_length = max(max(cell2mat(size_d2)));
	d3 = cellfun(@(x) ensure_str_len(x,str_length),d2,'uniformoutput',0);
	d4 = cell2mat(d3');


	fid = fopen(savefile_const_insig,'wt');
	for i = 1:size(d4,1)
		fprintf(fid,'%s\n',d4(i,:));
	end
	fclose(fid);
end

return;



%% Identify selected SNPs
% (and representatives of each tower)

%load(savedir_original_data)

%rs_ident=data.rs_ident;
%chromos=data.chromos;

%rs_ident_now=rs_ident((chromos~=0 & chromos~=23));

clear selected_sig_id selected_id representatives;
for i=1:22
	for j=1:length(locs{i})
		representatives{i}{j}=rs_ident_now{locs{i}(j)};
	end
	for j=1:length(selected_sig_pos{i})
		selected_sig_id{i}{j}=rs_ident_now{selected_sig_pos{i}(j)};
	end
	for j=1:length(selected_pos{i})
		selected_id{i}{j}=rs_ident_now{selected_pos{i}(j)};
	end
end



find(~cellfun('isempty', strfind(selected_id{1},'rs6684865')))


sum=0;
for i = 1:22
	sum= size(pvalue_combi_filtered{1},2)+sum;
end

% Check if some snp was selected
find(ismember(selected_id{12}, 'rs12304921')==1)

[pks,locs] = findpeaks(data);
