% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [p_values, selected_indices, svm_weights, selected_rs_ids] ...
	= combi_run_with_suiza2000()
% Import data from PLink Binary data-set 'suiza2000_03',
% and run the COMBI method on it.

add_required_paths();

input_dir = [pwd, '/data/'];
input_files = { ...
	[input_dir, 'suiza2000_03.fam']; ...
	[input_dir, 'suiza2000_03.bim']; ...
	[input_dir, 'suiza2000_03.bed'] ...
	};
dataset_info = dataset_info_create('PLinkBinary', input_files);

output_dir = [input_dir, get_host_app_name(), '/'];

[p_values, selected_indices, svm_weights, selected_rs_ids] ...
	= combi(dataset_info, output_dir);

return;

%% Load and save labels and dichotimized label from '*.pheno' files

input_dir_labels = [input_dir_data, 'phenotypes/'];
output_dir_labels = input_dir_labels;

load_labels_pheno_default(input_dir_labels, output_dir_labels, 'prosocA');
load_labels_pheno_default(input_dir_labels, output_dir_labels, 'med_rrp_gains');
load_labels_pheno_default(input_dir_labels, output_dir_labels, 'med_rrp_losses');
load_labels_pheno_default(input_dir_labels, output_dir_labels, 'timepref1');
load_labels_pheno_default(input_dir_labels, output_dir_labels, 'timepref2');
load_labels_pheno_default(input_dir_labels, output_dir_labels, 'prosocA');
load_labels_pheno_default(input_dir_labels, output_dir_labels, 'prosocA');
