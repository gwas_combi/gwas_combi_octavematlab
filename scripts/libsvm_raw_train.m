% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [weights_normalized, weights_raw] = libsvm_raw_train(K, X, y, cost, classifier, epsilon, loglevel, using_shogun, use_bias)
% A basic wrapper for libLinear (training),
% that either invokes it through the Shogun Toolbox,
% or directly through the libraries own Matlab/Octave interface.
% classifier can be:
% * LIBLINEAR_L2R_LR
% * LIBLINEAR_L2R_L2LOSS_SVC_DUAL
% * LIBLINEAR_L2R_L2LOSS_SVC
% * LIBLINEAR_L2R_L1LOSS_SVC_DUAL
% * LIBLINEAR_L1R_L2LOSS_SVC
% using_shogun can be:
% * 0 for using libLinear directly, through its own Matlab/Octave interface
% * 1 to use it through the Shogun Toolbox

if ~exist('using_shogun', 'var')
	using_shogun = 0; % by default: use libLinear directly
end
if ~exist('use_bias', 'var')
	use_bias = 1; % by default: use a bias-term
end

if (using_shogun)
	sg('loglevel', loglevel); % 'ERROR' or 'ALL'

	%% INITIALIZATION
	sg('new_svm', classifier);
	sg('svm_use_bias', use_bias);
	sg('svm_epsilon', epsilon);

	%% TRAINING
	sg('set_labels','TRAIN', y);
	sg('set_kernel', 'CUSTOM', K, 'FULL');
	sg('init_kernel', 'TRAIN');
	sg('c', cost);
	%sg('svm_max_train_time', 60);
	sg('svm_train');
	[~, alpha] = sg('get_svm'); % returns [bias, alphas]
else
	libSvm_classifier = 0; % C-SVC % HACK?
	kernel_type = 4; % pre-computed
	libSvm_options = [ ...
		'-s ', num2str(libSvm_classifier), ...
		' -t ', num2str(kernel_type), ...
		' -c ', num2str(cost), ...
		' -e ', num2str(epsilon)];
	fprintf('libSvm options: "%s"\n', libSvm_options);

	%X = X';
	y = y';
	y = double(sign(y)); % HACK we only do this to fix bad input data in Tutorial_Matrix, where one label at the beginning is -21 instead of 1 or -1
	%X = sparse(X);

	num_feat = size(K, 1);
	% add the sample_serial_number column, as required by the libSvm format for pre-computed kernels
	libSvm_K = [(1:num_feat)', K];
	model = svmtrain(y, libSvm_K, libSvm_options);
	assert(~isempty(model), 'Failed training the SVM');
	assert(model.nr_class == 2, ['You may only have two different label values, but there are ', model.nr_class])
	%w = model.SVs' * model.sv_coef;
	alpha = [model.sv_coef, model.SVs];
end

alpha_full = zeros(size(y, 1), 1);
alpha_full(alpha(:, 2) + 1) = alpha(:, 1);
% cut off the bias term (the last entry in the weights vector)
% XXX Maybe this is not required when using shogun?
alpha_full = alpha_full(1:num_feat, :);
weights_raw = alpha_full' * X;
weights_raw = weights_raw';
weights_normalized = abs(weights_raw) / norm(weights_raw);

