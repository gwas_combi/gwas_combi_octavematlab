% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_lasso( X, y, rep, Cs )

% function result = train_liblinear( X, y, rep,  )


% :( :(
X = double(X);
% should be fixed to limit memory requirements...


% PARAMETERS
if ~exist('rep'), rep = 1; end
if ~exist('Cs'), Cs = 100; rep=1; end
if ~exist('p'),  p = 1; end



% INITIALIZATION 
y = (y-mean(y))/max(abs(y));
num_data = length(y);
num_feat = size(X,1);
mae_val = zeros(rep,length(Cs));
mae_test = zeros(rep,length(Cs));
w_val =  zeros(rep,num_feat,length(Cs));
w =  zeros(rep,num_feat);
ranking =  zeros(rep,num_feat,length(Cs));
addpath('dal_ver1.05');
Cs = Cs.^(-1);



% VALIDATION LOOP
idx = 0;
tic;
for i = 1:rep

	% TRAIN / VAL SPLITS
	perm = randperm(num_data);
	divtr = perm(1:floor(num_data/2));
	divval = perm(floor(num_data/2)+1:floor(num_data*3/4));
	divte = perm(floor(num_data*3/4)+1:end);	

    for j = 1:length(Cs)
 	    C = Cs(j);
      
        % LASSO TRAINING
		[w_,status] = dalsql1(zeros(num_feat+1,1), [X(:,divtr);ones(1,length(divtr))]', y(divtr)', C);
		w_ = full(w_);
		b = w_(end);
		w_ = w_(1:end-1);
        w_val(i,:,j) = abs(w_')/norm(w_);
		
		[w_,status] = dalsql1(zeros(num_feat,1), [X(:,divtr)]', y(divtr)', C);
		
        % SVM TESTING 
        out = w_'*X(:,divval) + b;
		mae_val(i,j) = mean(abs(y(divval)-out));
        out = w_'*X(:,divte) + b;
		mae_test(i,j) = mean(abs(y(divte)-out));
		%plot(y(divte),out,'x')  %sanity check

		idx = idx + 1;
		print_progress(idx,rep*length(Cs));
    end
	
end
clear Xtr w_ Kval Kte;


% MODEL SELECTION
[aux ind_C] = min(mean(mae_val,1),[],2);
C = Cs(ind_C);
mae_test = mean(mae_test(:,ind_C),1);


% SVM TRAINING ON FULL DATA
[w_,status] = dalsql1(zeros(num_feat+1,1), [X;ones(1,num_data)]', y', C);
while length(find(w_~=0))<500
  C = C*0.01;
  [w_,status] = dalsql1(zeros(num_feat+1,1), [X;ones(1,num_data)]', y', C);
end
w_ = full(w_);
w_ = w_(1:end-1);
w = abs(w_')/norm(w_);


% COMPUTE RANKINGS
%p = 1;
w = (sum(abs(reshape(w(:)',3,num_feat/3)).^p)).^(1/p); 
%w = sum(reshape(w(:)',3,num_feat/3),1);
w = w/norm(w);
%[foo ranking] = sort(w,'descend');
w_avg = median(w_val(:,:,ind_C),1);
w_avg = w_avg/norm(w_avg);
%[foo, ranking_avg] = sort(w_avg,'descend');


% COLLECT RESULTS
% result = struct('w_avg',w_avg(:)','w',w(:)','mae_val',mae_val,'mae_test',mae_test,'Cs',Cs,'C',C,'rep',rep,'classy',classy,'time',time,'time_val',time_val,'mae_eps',mae_eps);
result = struct('w_avg',w_avg(:)','w',w(:)','mae_val',squeeze(mean(mae_val,1)),'mae_test',mae_test,'Cs',Cs.^(-1),'C',C^(-1),'rep',rep);

% CLEAN UP MEMORY
sg('clean_kernel', 'TRAIN');
