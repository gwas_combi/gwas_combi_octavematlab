These files are used to run the permutation test for WTCCC data. 

You can either do the computations on the cluster with submit_permutation_test.sh (recommended!)
	First adjust the settings in parameters_permutation_test.m and the directories/disease 	in wrapper_permutation_test.m
	Then adjust the directories etc. in submit_permutation_test.sh 
	Then type 'qsub submit_permutation_test.sh' in your console

Or you can run it manually.
	First adjust the settings in parameters_permutation_test.m and the directories/disease 	in 	wrapper_permutation_test.m	
	Then run the following for-loop:
		for replacethis1=1:22
			wrapper_permutation_test
		end