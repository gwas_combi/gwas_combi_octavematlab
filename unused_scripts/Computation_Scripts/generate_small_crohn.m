% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function void = generate_small_crohn(individuals,inform_snps,noise_snps,rep);

% function void = generate_small_crohn(individuals,inform_snps,noise_snps,rep);

%%%%%%%%%%%%%%%%%
%% Parameters %%%
%%%%%%%%%%%%%%%%%

if ~exist('rep'), rep = 1; end
inform_snps = inform_snps*rep;
num_snps = inform_snps + noise_snps;
offset_chromo1 = 10000;  %take informatives from Chromo1 and noise from Chromo2
offset_chromo2 = 50000;
nr_copies = 1;

%%%%%%%%%%%%%%%%
%% Filenames %%%
%%%%%%%%%%%%%%%%

load_dir = '/home/bmieth/svn/iew/data/morbus_crohn/';
save_dir = '/home/bmieth/svn/iew/scripts/marius/tmp/';
%filename = ['crohn_part_chromo2_',int2str(individuals),'x',int2str(anzahl_snps),'.txt'];
save_file_informative = ['crohn_informative.txt'];
save_file_noise = ['crohn_noise.txt'];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Has the data already been extracted? %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if all(logical([exist('/home/bmieth/svn/iew/scripts/marius/tmp/crohn_informative.txt'),exist('/home/bmieth/svn/iew/scripts/marius/tmp/crohn_noise.txt')]));
  num_lines_inform = str2num( perl('count_lines.pl','/home/bmieth/svn/iew/scripts/marius/tmp/crohn_informative.txt') );
  num_lines_noise = str2num( perl('count_lines.pl','/home/bmieth/svn/iew/scripts/marius/tmp/crohn_noise.txt') );
  num_columns_inform = count_columns('/home/bmieth/svn/iew/scripts/marius/tmp/crohn_informative.txt');
  num_columns_noise = count_columns('/home/bmieth/svn/iew/scripts/marius/tmp/crohn_noise.txt');
  file_ok = (num_lines_inform==nr_copies*individuals) && (num_lines_noise==nr_copies*individuals) && (num_columns_inform==inform_snps) && (num_columns_noise==noise_snps);
  if ~file_ok 
    fprintf('\nExisting base data file does not match the parameters, num_individuals and num_snps.\nWill extract new data file (may take some time). Press key to continue.\n');
  end
else
  fprintf('\nNo base data file found. Will extract new data file (may take some time).\n   ###  Press key to continue!  ###\n');
  file_ok = false; 
end


%%%%%%%%%%%%%%%%%%%%%%
%% Data Extraction %%%
%%%%%%%%%%%%%%%%%%%%%%

if ~file_ok, 
	pause;
	clear dat;
	dat = load([load_dir 'genotypes.mat']);
	num_indiv = size(dat.allele1,1);

	%% Delete informative SNPs with Minor Allele Frequecy < 0.15 %%%
	j = offset_chromo1;
	counter = 1;
	index1 = zeros(1,num_snps);
	fprintf('\nChecking minor allele frequencies...\n')
	tic;
	while counter <= num_snps
	  valid1 = find(dat.allele1(:,j)~=48);
	  valid2 = find(dat.allele2(:,j)~=48);
	  valid_alleles = [dat.allele1(valid1,j); dat.allele2(valid2,j)];
	  lex_min = min(valid_alleles);
	  maf = sum(valid_alleles==lex_min)/(2*num_indiv);
	  maf = min(maf,1-maf);
	  if maf>0.15
		index1(counter) = j;
		counter = counter + 1;
		print_progress(counter,2*num_snps);
	  end
	  j = j+1;
	end
	assert(j<offset_chromo2);

	%% Delete noise SNPs with Minor Allele Frequecy < 0.15 %%%
	j = offset_chromo2;
	counter = 1;
	index2 = zeros(1,num_snps);
	tic;
	while counter <= num_snps
	  valid1 = find(dat.allele1(:,j)~=48);
	  valid2 = find(dat.allele2(:,j)~=48);
	  valid_alleles = [dat.allele1(valid1,j); dat.allele2(valid2,j)];
	  lex_min = min(valid_alleles);
	  maf = sum(valid_alleles==lex_min)/(2*num_indiv);
	  maf = min(maf,1-maf);
	  if maf>0.15
		index2(counter) = j;
		counter = counter + 1;
		print_progress(num_snps+counter,2*num_snps);
	  end
	  j = j+1;
	end

	%% Write Informative Snips File %%%
	fprintf('\nWriting file...\n')
	fid = fopen([save_dir save_file_informative], 'w');
	tic;
	for i=1:nr_copies*individuals
	  for j=1:inform_snps
		if dat.allele1(i,index1(j)) <= dat.allele2(i,index1(j))
		  fprintf(fid, '%c%c ',dat.allele1(i,index1(j)), dat.allele2(i,index1(j)));
		else 
		  fprintf(fid, '%c%c ',dat.allele2(i,index1(j)), dat.allele1(i,index1(j)));
		end
	  end
	  fprintf(fid, '\n');
	  print_progress(i,2*nr_copies*individuals);
	end
	fclose(fid);

	%% Write Noise Snips File %%%
	fid = fopen([save_dir save_file_noise], 'w');
	tic;
	for i=1:nr_copies*individuals
	  for j=1:noise_snps
		if dat.allele1(i,index2(j)) <= dat.allele2(i,index2(j))
		  fprintf(fid, '%c%c ',dat.allele1(i,index2(j)), dat.allele2(i,index2(j)));
		else 
		  fprintf(fid, '%c%c ',dat.allele2(i,index2(j)), dat.allele1(i,index2(j)));
		end
	  end
	  fprintf(fid, '\n');
	  print_progress(nr_copies*individuals+i,2*nr_copies*individuals);
	end
	fclose(fid);	

end

