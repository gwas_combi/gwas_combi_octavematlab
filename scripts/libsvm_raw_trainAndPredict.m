% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [weights_normalized, accuracy_validation, accuracy_testing] ...
	= libsvm_raw_trainAndPredict( ...
	K_training, X_training, y_training, ...
	K_validation, y_validation, ...
	K_testing, y_testing, ...
	cost, classifier, epsilon, loglevel, ...
	using_shogun, ...
	use_bias)
% A basic wrapper for libLinear (training),
% that either invokes it through the Shogun Toolbox,
% or directly through the libraries own Matlab/Octave interface.
% using_shogun can be:
% * 0 for using libLinear directly, through its own Matlab/Octave interface
% * 1 to use it through the Shogun Toolbox

if ~exist('using_shogun', 'var')
	using_shogun = 0; % by default: use libLinear directly
end
if ~exist('use_bias', 'var')
	use_bias = 1; % by default: use a bias-term
end

if (using_shogun)
	sg('loglevel', loglevel); % 'ERROR' or 'ALL'

	%% INITIALIZATION
	sg('new_svm', classifier);
	sg('svm_use_bias', use_bias);
	sg('svm_epsilon', epsilon);

	%% TRAINING
	sg('set_labels','TRAIN', y_training);
	sg('set_kernel', 'CUSTOM', K_training, 'FULL');
	sg('init_kernel', 'TRAIN');
	sg('c', cost);
	%sg('svm_max_train_time', 60);
	sg('svm_train');
	[~, alpha] = sg('get_svm'); % returns [bias, alphas]
	%[bias, alpha] = sg('get_svm');

	%% VALIDATION
	sg('set_kernel', 'CUSTOM', K_validation, 'FULL');
	sg('init_kernel', 'TEST');
	out_validation = sg('classify');
	%out_validation = bias + alpha(:, 1)' * K_validation(alpha(:, 2) + 1, :);
	accuracy_validation = 1 - mean(y_validation ~= sign(out_validation));

	%% TESTING
	sg('set_kernel', 'CUSTOM', K_testing, 'FULL');
	sg('init_kernel', 'TEST');
	out_testing = sg('classify');
	%out_testing = bias + alpha(:, 1)' * K_validation(alpha(:, 2) + 1, :);
	accuracy_testing = 1 - mean(y_testing ~= sign(out_testing));
else
	libSvm_classifier = 0; % C-SVC % HACK?
	kernel_type = 4; % pre-computed
	libSvm_options = [ ...
		'-s ', num2str(libSvm_classifier), ...
		' -t ', num2str(kernel_type), ...
		' -c ', num2str(cost), ...
		' -e ', num2str(epsilon)];
	%fprintf('libSvm options: "%s"\n', libSvm_options);

	%% TRAINING
	% add the sample_serial_number column, as required by the libSvm format for pre-computed kernels
	libSvm_K_training = [(1:num_feat)', K_training];
	model = svmtrain(y_training, libSvm_K_training, libSvm_options);
	alpha = [model.sv_coef, model.SVs];

	%% VALIDATION
	% add the sample_serial_number column, as required by the libSvm format for pre-computed kernels
	libSvm_K_validation = [(1:num_feat)', K_validation];
	[out_validation, accuracy_validation_ll, ~] = svmpredict(y_validation, libSvm_K_validation, model);
	accuracy_validation_self = 1 - mean(y_validation ~= sign(out_validation));
	assert(accuracy_validation_self ~= accuracy_validation_ll, ...
		'The accurracy for validation we calculated ourselfs (%f) differs from the one calculated by libLinear (%f)', ...
		accuracy_validation_self, accuracy_validation_ll);
	accuracy_validation = accuracy_validation_self;

	%% TESTING
	% add the sample_serial_number column, as required by the libSvm format for pre-computed kernels
	libSvm_K_testing = [(1:num_feat)', K_testing];
	[~, accuracy_testing, ~] = svmpredict(y_testing, libSvm_K_testing, model);
end

alpha_full = zeros(size(y_training, 1), 1);
alpha_full(alpha(:, 2) + 1) = alpha(:, 1);
% cut off the bias term (the last entry in the weights vector)
% XXX Maybe this is not required when using shogun?
alpha_full = alpha_full(1:num_feat, :);
weights_raw = alpha_full' * X_training;
weights_raw = weights_raw';
weights_normalized = abs(weights_raw) / norm(weights_raw);

