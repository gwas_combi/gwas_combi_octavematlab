% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2016 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [p_value_targets] = permutation_test_phase_1(data, labels, combi_parameters, alpha)
% Calculate P-Values using random labels, many times,
% and collect the lowest P-Values, one from each iteration.
% Supplies one P-Value target per chromosome,
% or a single one for the whole genome.

iterations = combi_parameters.threshold_calibration_permutation_1_iterations;

% Initialize
p_lowest = zeros(1, iterations);

for i = 1:iterations
	% Show progress
	disp([num2str(round(i * 100 / iterations)), '%']);

	% Permute labels
	num_labels = length(labels);
	labels_perm = zeros(1, num_labels);
	labels_perm(randperm(num_labels)) = labels;

	% Compute p-values Bonferoni
	%[pvalue_mtest_, chisq_value, call_rate, maf, lex_min, lex_max, tables] = multiple_testing_chi(data, labels_perm);
	[pvalue_mtest, ~] = chi_square_goodness_of_fit_test(data', labels_perm);

	% Save lowest p-value
	if (size(pvalue_mtest, 2) == 1)
		p_lowest(i) = min(pvalue_mtest);
	else
		p_lowest(i) = 1.0;
	end
	%pvalue_mtest_all{i} = pvalue_mtest;
end

% Find t_star as alpha-percentile of sorted p-values
p_lowest_sorted = sort(p_lowest);
t_star = p_lowest_sorted(max(ceil(iterations * alpha) - 1, 1));
p_value_targets = t_star;

