% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [prec, rec, auc] = compute_prc_curve(pvalue_method, pvalue_truth, prc_ub)
% Computes PRC curve (prec and rec are coordinates of curve, auc is area under the curve) for all repetitions and a specific top_k

% pvalue can be a matrix with two dimensions m x n
% m = number of repetitions
% n = number of SNPs
% pvalue_truth is of size 1 x n. 
% The output auc is of size m x 1.

% This function calls 'val_PRC' which computes the PRC coordinates and AUC for one repetition and one top_k. 
% It also calls 'val_align_PRC' which aligns the coordinates to a smooth nice curve on the given intervall.

% If an upper bound for the Precision is not given, set it to 1 (the whole interval is analysed)
if ~exist('prc_ub', 'var')
	prc_ub = 1;
end

% Convert pvalues into scores from which AUC can be computed
score_method = -log(pvalue_method);
score_truth = -log(pvalue_truth);

% Initialize recall
rec = (0:prc_ub/100:prc_ub);
rec = sort([rec (prc_ub/100 + prc_ub/1000):(prc_ub/100):prc_ub]);

% For each repetition calculate PRC corrdinates and AUC
auc = zeros(size(pvalue_method, 1)); % pre-allocate memory
for i = 1:size(pvalue_method, 1)
	[prec_, rec_, auc_] = val_PRC(score_method(i, :), score_truth, 'ub', prc_ub);
	prc.prec = prec_;
	prc.rec = rec_;
	auc(i) = auc_;
	prec(i, :) = val_align_PRC(prc, rec);
end
