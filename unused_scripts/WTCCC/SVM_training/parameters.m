% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Parameters             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

chromos = [1:22];
top_k = 100;
svm_rep =1;
Cs=1.0000e-05;
pnorm_feature_scaling = 6;  % 0.5, 1, 2, 10, 20, Inf =no scaling
filter_window_size = 35; % 1-41, odd!!!
p_pnorm_filter = 2; % 1,2,4,100, 0.25, 0.5
classy = 'LIBLINEAR_L2R_L1LOSS_SVC_DUAL'; %Can be: LIBLINEAR_L2R_LR, LIBLINEAR_L2R_L2LOSS_SVC_DUAL, LIBLINEAR_L2R_L2LOSS_SVC, LIBLINEAR_L2R_L1LOSS_SVC_DUAL, 
svm_epsilon = 1e-3;
p_svm = 2;






