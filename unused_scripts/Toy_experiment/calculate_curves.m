% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%    ROC Analysis   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%
tic,

%% True oracle pvalues
pvalue_truth = [ones(1, noise_snps/2) zeros(1, inform_snps) ones(1, noise_snps/2)];
truth_mat = repmat(pvalue_truth, rep, 1);

%% Calculate curves
[tp_no_screening, ~, auc_roc_no_screening] = compute_roc(pvalue_no_screening, pvalue_truth, roc_ub);
[prec_no_screening, ~, auc_prc_no_screening] = compute_prc_curve(pvalue_no_screening, pvalue_truth, prc_ub);

[tp_filter_screening, ~, auc_roc_filter_screening] = compute_roc(pvalue_filter_screening, pvalue_truth, roc_ub);
[prec_filter_screening, ~, auc_prc_filter_screening] = compute_prc_curve(pvalue_filter_screening, pvalue_truth, prc_ub);

[tp_SVM_screening, ~, auc_roc_SVM_screening] = compute_roc(pvalue_SVM_screening, pvalue_truth, roc_ub);
[prec_SVM_screening, ~, auc_prc_SVM_screening] = compute_prc_curve(pvalue_SVM_screening, pvalue_truth, prc_ub);
	
[tp_SVM_filter_screening, ~, auc_roc_SVM_filter_screening] = compute_roc(pvalue_SVM_filter_screening, pvalue_truth, roc_ub);
[prec_SVM_filter_screening, ~, auc_prc_SVM_filter_screening] = compute_prc_curve(pvalue_SVM_filter_screening, pvalue_truth, prc_ub);

[tp_roshan, ~, auc_roc_roshan] = compute_roc_roshan(roshan_scores, pvalue_truth, roc_ub);
[prec_roshan, ~, auc_prc_roshan] = compute_prc_roshan(roshan_scores, pvalue_truth, prc_ub);

[tp_SVM_filter_screening_multisplit, ~, auc_roc_SVM_filter_screening_multisplit] = compute_roc(pvalue_SVM_filter_screening_multisplit, pvalue_truth, roc_ub);
[prec_SVM_filter_screening_multisplit, ~, auc_prc_SVM_filter_screening_multisplit] = compute_prc_curve(pvalue_SVM_filter_screening_multisplit, pvalue_truth, prc_ub);

[tp_SVM_filter_screening_singlesplit, ~, auc_roc_SVM_filter_screening_singlesplit] = compute_roc(pvalue_SVM_filter_screening_singlesplit, pvalue_truth, roc_ub);
[prec_SVM_filter_screening_singlesplit, ~, auc_prc_SVM_filter_screening_singlesplit] = compute_prc_curve(pvalue_SVM_filter_screening_singlesplit, pvalue_truth, prc_ub);


%% Now analysis of varying t to control for FWER
FWER_no_screening = ones(1, length(t_vector));
FWER_filter_screening = ones(1, length(t_vector));
FWER_SVM_screening = ones(1, length(t_vector));
FWER_SVM_filter_screening = ones(1, length(t_vector));
FWER_roshan = ones(1, length(t_vector));
FWER_SVM_filter_screening_multisplit = ones(1, length(t_vector));
FWER_SVM_filter_screening_singlesplit = ones(1, length(t_vector));

FWTPR_no_screening = ones(1, length(t_vector));
FWTPR_filter_screening = ones(1, length(t_vector));
FWTPR_SVM_screening = ones(1, length(t_vector));
FWTPR_SVM_filter_screening = ones(1, length(t_vector));
FWTPR_roshan = ones(1, length(t_vector));
FWTPR_SVM_filter_screening_multisplit = ones(1, length(t_vector));
FWTPR_SVM_filter_screening_singlesplit = ones(1, length(t_vector));

roshan_scores_zero_one_space = exp(-(roshan_scores)); 

for i = 1:length(t_vector)
	print_progress(i, length(t_vector));
	t = t_vector(i);
	
	t_mat = repmat(t, rep, num_snps);

	positives_no_screening = pvalue_no_screening < t_mat;
	positives_filter_screening = pvalue_filter_screening < t_mat;
	positives_SVM_screening = pvalue_SVM_screening < t_mat;
	positives_SVM_filter_screening = pvalue_SVM_filter_screening < t_mat;
	positives_roshan = roshan_scores_zero_one_space < t_mat;
	positives_SVM_filter_screening_multisplit = pvalue_SVM_filter_screening_multisplit < t_mat;
	positives_SVM_filter_screening_singlesplit = pvalue_SVM_filter_screening_singlesplit < t_mat;

	%% False positives
	false_pos_no_screening = sum(sum(positives_no_screening & truth_mat, 2) ~= 0);
	false_pos_filter_screening = sum(sum(positives_filter_screening & truth_mat, 2) ~= 0);
	false_pos_SVM_screening = sum(sum(positives_SVM_screening & truth_mat, 2) ~= 0);
	false_pos_SVM_filter_screening = sum(sum(positives_SVM_filter_screening & truth_mat, 2) ~= 0);
	false_pos_roshan = sum(sum(positives_roshan & truth_mat, 2) ~= 0);
	false_pos_SVM_filter_screening_multisplit = sum(sum(positives_SVM_filter_screening_multisplit & truth_mat, 2) ~= 0);
	false_pos_SVM_filter_screening_singlesplit = sum(sum(positives_SVM_filter_screening_singlesplit & truth_mat, 2) ~= 0);

	%% observed FWER
	FWER_no_screening(i) = false_pos_no_screening/rep;
	FWER_filter_screening(i) = false_pos_filter_screening/rep;
	FWER_SVM_screening(i) = false_pos_SVM_screening/rep;
	FWER_SVM_filter_screening(i) = false_pos_SVM_filter_screening/rep;
	FWER_roshan(i) = false_pos_roshan/rep;
	FWER_SVM_filter_screening_multisplit(i) = false_pos_SVM_filter_screening_multisplit/rep;
	FWER_SVM_filter_screening_singlesplit(i) = false_pos_SVM_filter_screening_singlesplit/rep;

	%% True positives
	true_pos_no_screening = sum(sum(positives_no_screening & ~truth_mat));
	true_pos_filter_screening = sum(sum(positives_filter_screening & ~truth_mat));
	true_pos_SVM_screening = sum(sum(positives_SVM_screening & ~truth_mat));
	true_pos_SVM_filter_screening = sum(sum(positives_SVM_filter_screening & ~truth_mat));
	true_pos_roshan = sum(sum(positives_roshan & ~truth_mat));
	true_pos_SVM_filter_screening_multisplit = sum(sum(positives_SVM_filter_screening_multisplit & ~truth_mat));
	true_pos_SVM_filter_screening_singlesplit = sum(sum(positives_SVM_filter_screening_singlesplit & ~truth_mat));

	%% observed FW-TPR
	FWTPR_no_screening(i) = true_pos_no_screening/(inform_snps*rep);
	FWTPR_filter_screening(i) = true_pos_filter_screening/(inform_snps*rep);
	FWTPR_SVM_screening(i) = true_pos_SVM_screening/(inform_snps*rep);
	FWTPR_SVM_filter_screening(i) = true_pos_SVM_filter_screening/(inform_snps*rep);
	FWTPR_roshan(i) = true_pos_roshan/(inform_snps*rep);
	FWTPR_SVM_filter_screening_multisplit(i) = true_pos_SVM_filter_screening_multisplit/(inform_snps*rep);
	FWTPR_SVM_filter_screening_singlesplit(i) = true_pos_SVM_filter_screening_singlesplit/(inform_snps*rep);

	clearvars t t_mat sig_snps_now positives_mtest_now positives_combi_now false_pos_mtest_now false_pos_combi_now true_pos_mtest_now true_pos_combi_now
end

return;

pvalue_no_screening = pvalue_no_screening(1:rep, :);
pvalue_filter_screening = pvalue_filter_screening(1:rep, :);
pvalue_SVM_screening = pvalue_SVM_screening(1:rep, :);
pvalue_SVM_filter_screening=pvalue_SVM_filter_screening(1:rep, :);
pvalue_SVM_filter_screening_multisplit=pvalue_SVM_filter_screening_multisplit(1:rep, :);
pvalue_SVM_filter_screening_singlesplit=pvalue_SVM_filter_screening_singlesplit(1:rep, :);


%% Now analysis of varying t to control for FWER

FWER_roshan = ones(1, length(t_vector));

FWTPR_roshan = ones(1, length(t_vector));

roshan_scores_zero_one_space = exp(-(roshan_scores)); 

for i = 1:length(t_vector)
	print_progress(i, length(t_vector));
	t = t_vector(i);

	t_mat = repmat(t, rep, num_snps);

	positives_roshan = roshan_scores_zero_one_space < t_mat;

	%% False positives
	false_pos_roshan = sum(sum(positives_roshan & truth_mat, 2) ~= 0);

	%% observed FWER
	FWER_roshan(i) = false_pos_roshan/rep;

	%% True positives
	true_pos_roshan = sum(sum(positives_roshan & ~truth_mat));

	%% observed FW-TPR
	FWTPR_roshan(i) = true_pos_roshan/(inform_snps*rep);

	clearvars t t_mat sig_snps_now positives_mtest_now positives_combi_now false_pos_mtest_now false_pos_combi_now true_pos_mtest_now true_pos_combi_now
end
