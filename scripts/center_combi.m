% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function X = center_combi(X)
% center - center a data matrix
%
% Synopsis:
%    X = center(X);
%
% Arguments:
%    X:    data matrix
%
% Return:
%    X: centered data matrix
%

%n = size(X,2);

if (ndims(X) == 2)
	% X = X * (eye(n) - 1/n * ones(n, n));

	X = X - mean(X, 2) * ones(1, size(X, 2));
	%X = X - repmat(mean(X, 2), 1, size(X,2));
elseif (ndims(X) == 3)
	for i = 1:size(X, 3)
		%X(:,:,i) = X(:,:,i) * (eye(n) - 1/n *  ones(n,n));
		X(:, :, i) = X(:, :, i) - mean(X(:, :, i), 2) * ones(1, size(X, 2));
	end
end

