% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% Fix a number top_k and an FWER-Level alpha, for example: 5%
top_k = 30;
top_k_multi = top_k*3;
top_k_roshan_2 = top_k;
top_k_roshan_1 = top_k_roshan_2*5; % 60, 150, 300 % top_k*2, 5, 10


alpha_sig = 0.05;
rep = 2; % 10000
num_splits = 10; % 1000

%% Other Parameters

svm_rep = 1; % 20, 40
svm_rep_permtest = 1;
Cs = 0.0022; % logspace(-4, 4, 7), logspace(-4, -1, 3)
use_seed = 1; % Use seed a specified seed for random number generation, for reproducibility of results? (0=no, 1=yes)
seed = 13;
use_scaling = 1; % Use Feature Scaling? (0=no, 1=yes)
use_filter = 1; % Use Moving P-norm Filter? (0=no, 1=yes)

feature_embedding = 'genotypic';  % genotypic, allelic, nominal
pnorm_feature_scaling = 2; % 0.5, 1, 2, 10, 20, Inf=no scaling
filter_window_size = 35; % 35, 1-41, odd!!!
p_pnorm_filter = 2; % 1, 2, 4, 100, 0.25, 0.5
classy = 'LIBLINEAR_L2R_L1LOSS_SVC_DUAL'; % Can be: LIBLINEAR_L2R_LR, LIBLINEAR_L2R_L2LOSS_SVC_DUAL, LIBLINEAR_L2R_L2LOSS_SVC, LIBLINEAR_L2R_L1LOSS_SVC_DUAL

filter_window_size_mtest = 9; % 1-41, odd!!!
p_pnorm_filter_mtest = 6; % 1, 2, 4, 100, 0.25, 0.5

p_svm = 2;
svm_epsilon = 1e-3;

rep_inform = 10;
inform_snps = 20; % 15 - 20
noise_snps = 10000; % even
individuals = 300; % 100 - 300
noise_parameter = 6;

roc_ub = 1;
prc_ub = 1;

% varying t to control for FWER
t_vector = [0.000000001:0.000000001:0.000001, 0.000001:0.000001:0.001, 0.001:0.001:1]; % 0.0000001:0.0000001:0.001;
%t_vector = t_vector(1:5:end);

gammas_multisplit = 0.05:0.0001:1; 
