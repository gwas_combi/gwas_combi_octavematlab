% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function nr_lines = count_lines(fname)

% we need to quote because otherwise it will fail if there are spaces in the path.
to_be_read_file = quote(fname);

if (ispc())
	% on Windows
	% we need to quote because otherwise it will fail if there are spaces in the path.
	script_file = quote([mfilename('fullpath'), '.pl']);
	nr_lines = str2num(perl(script_file, to_be_read_file));
else
	% on non-Windows (Linux, OS X, BSD)
	[status, output] = system(['wc -l ', to_be_read_file, ' | awk "{ print \$1 }"']);
	nr_lines = str2num(output);
end

