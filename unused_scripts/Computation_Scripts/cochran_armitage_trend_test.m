% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function  asympt_p =  multiple_testing_trend(data,labels)
weights = [0,1, 2];


	
%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     Assertions     %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

assert(mod(size(data,2),3)==0);

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Initialization   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

labels = labels(:)';
individuals = size(data,1);
num_snps = size(data,2)/3;
nillich = uint8('0');
lex_min = zeros(1, num_snps, 'uint8');
lex_max = zeros(1, num_snps, 'uint8');
call_rate = zeros(1, num_snps);
tables = zeros(2, 3, num_snps);
maf = zeros(1, num_snps);
asympt_p = zeros(1, num_snps);
chisq_value = zeros(1, num_snps);
toleranz = 2*eps(1.0);

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%    Prepare data    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

allele1 = uint8(data(:,1:3:end));
allele2 = uint8(data(:,2:3:end));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find successfully called individuals and calculate calling rate
valide = (allele1 ~= nillich)&(allele2 ~= nillich);
nutz_n=sum(valide,1);
call_rate=nutz_n / individuals;

% build (2x3)-tables
temp1 = allele1;
temp2 = allele2;
temp1(~valide) = 255;
temp2(~valide) = 255;
lex_min_1 = min(temp1);
lex_min_2 = min(temp2);
lex_min = min(lex_min_1,lex_min_2);
lex_max_1 = max(allele1);
lex_max_2 = max(allele2);
lex_max = max(lex_max_1,lex_max_2);
clearvars temp;

cases = (labels==1)';
cases = repmat(cases,1,num_snps);
controls = (labels==-1)';
controls = repmat(controls,1,num_snps);

valide_cases = valide & cases;
num_valide_cases = sum(valide_cases)';
valide_controls = valide & controls;
num_valide_controls = sum(valide_controls)';

clearvars cases controls;

min_mat=repmat(lex_min,individuals,1);

lex_min_mat1= allele1==min_mat;
lex_min_mat2= allele2==min_mat;
not_equal_mat= allele1~=allele2;

clearvars min_mat;

tables(1,1,:) = sum(lex_min_mat1 & lex_min_mat2 & valide_cases,1);
tables(1,2,:) = sum(not_equal_mat& valide_cases,1);
tables(1,3,:) = num_valide_cases - squeeze(tables(1,1,:)) - squeeze(tables(1,2,:));
tables(2,1,:) = sum(lex_min_mat1 & lex_min_mat2 & valide_controls,1);
tables(2,2,:) = sum(not_equal_mat& valide_controls,1);
tables(2,3,:) = num_valide_controls - squeeze(tables(2,1,:)) - squeeze(tables(2,2,:));

% compute p-values

% build marginals = (n1., n2., n.1, n.2, n.3)
row_marginals = squeeze(sum(tables, 2))';
col_marginals = squeeze(sum(tables,1))';
if num_snps == 1
	col_marginals = col_marginals';
end
marginals = [row_marginals, col_marginals];

% calculate Chi-squared value for observed table
n = sum(marginals,2) / 2;
p = marginals(:,1) ./ n;


row1 = squeeze(tables(1, :,:))';
if num_snps == 1
	row1=row1';
end
weighted_mean = sum(col_marginals .* repmat(weights,num_snps,1),2) ./ n;
norm_weights = repmat(weights,num_snps,1) - repmat(weighted_mean,1,3);
nominator = (sum(row1 .* norm_weights,2)).^2;
denominator = p .* (1 - p) .* sum(col_marginals .* (norm_weights .^ 2),2);
chisq_value = nominator ./ denominator;

% calculate aymptotic p-value
asympt_p =  1 - chi2cdf(chisq_value, 1); 
asympt_p(isnan(asympt_p))=1;

asympt_p=asympt_p';







