% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_liblinear( X, y, rep, Cs, p, classy, epsilon,filter_window_size,p_pnorm_filter)

tic;

% :( :(
X = double(X);
% should be fixed to limit memory requirements...

% PARAMETERS
if ~exist('rep'), rep = 10; end
if ~exist('Cs'), Cs = logspace(-5,5,11); end
if ~exist('p'),  p = 1; end
if ~exist('epsilon'),
  epsilon = 1e-3;
end
if ~exist('classy'),
  classy = 'LIBLINEAR_L2R_L1LOSS_SVC_DUAL';
  % Can be: LIBLINEAR_L2R_LR, LIBLINEAR_L2R_L2LOSS_SVC_DUAL, LIBLINEAR_L2R_L2LOSS_SVC, LIBLINEAR_L2R_L1LOSS_SVC_DUAL, 'LIBLINEAR_L1R_L2LOSS_SVC'

end
sg('loglevel', 'ERROR');     % 'ERROR' or 'ALL'


% SVM CLASSIFICATION PARAMETERS
use_bias = true;


% INITIALIZATION
num_data = length(y);
num_feat = size(X,1);
if (classy(12)=='1')
  Cs = Cs*((num_feat/3)^(0.5));
end
acc_val = zeros(rep,length(Cs));
acc_val_filtered = zeros(rep,length(Cs));
acc_test = zeros(rep,length(Cs));
acc_test_filtered = zeros(rep,length(Cs));
w_val =  zeros(rep,num_feat,length(Cs));
w_val_filtered =  zeros(rep,num_feat,length(Cs));
w =  zeros(rep,num_feat);
%ranking =  zeros(rep,num_feat,length(Cs));


% VALIDATION LOOP
idx = 0;
tic;
for i = 1:rep

	% TRAIN / VAL SPLITS
	perm = randperm(num_data);
	divtr = perm(1:floor(num_data/2));
	divval = perm(floor(num_data/2)+1:floor(num_data*3/4));
	divte = perm(floor(num_data*3/4)+1:end);

    for j = 1:length(Cs)
        C = Cs(j);

        % SVM TRAINING
		if strcmp(classy,'LIBLINEAR_L1R_L2LOSS_SVC')
		  sg('set_features','TRAIN',X(:,divtr)');
		else
		  sg('set_features','TRAIN',X(:,divtr));
		end
		sg('set_labels','TRAIN',y(divtr));
        sg('c', C);
        sg('svm_use_bias', use_bias);
        sg('svm_epsilon', epsilon);
		%sg('svm_max_train_time', 60);
		sg('new_classifier',classy);
        sg('train_classifier');
        [b,w_]=sg('get_classifier');
        w_filtered_ = filter_pnorm(w_',filter_window_size,p_pnorm_filter)';

		w_ = w_ + b;
		w_filtered_ = w_filtered + b;

		w_val(i,:,j) = abs(w_')/norm(w_);
		w_val_filtered(i,:,j) = abs(w_filtered_')/norm(w_filtered_);

        % SVM TESTING
		%sg('set_features','TEST',X(:,divval));
        %out = sg('classify');
		out = w_'*X(:,divval)+b;
		out_filtered = w_filtered_'*X(:,divval)+b;

		acc_val(i,j) = 1-mean(y(divval)~=sign(out));
        acc_val_filtered(i,j) = 1-mean(y(divval)~=sign(out_filtered));

        %sg('set_features','TEST',X(:,divte));
        %out = sg('classify');
		out = w_'*X(:,divte);
		out_filtered = w_filtered_'*X(:,divte);

		acc_test(i,j) = 1-mean(y(divte)~=sign(out));
        acc_test_filtered(i,j) = 1-mean(y(divte)~=sign(out_filtered));

		idx = idx + 1;
		print_progress(idx,rep*length(Cs));

    end;

end;


% MODEL SELECTION
[foo ind] = max(mean(acc_val,1));
acc_test_all = acc_test;
acc_test = mean(acc_test(:,ind),1);
C = Cs(ind);

% MODEL SELECTION (FILTERED)
[foo ind] = max(mean(acc_val_filtered,1));
acc_test_all_filtered = acc_test_filtered;
acc_test_filtered = mean(acc_test_filtered(:,ind),1);
C_filtered = Cs(ind);

% SVM TRAINING ON FULL DATA
sg('set_labels','TRAIN',y);
sg('c',C);
sg('svm_use_bias', use_bias);
sg('svm_epsilon', epsilon);
if strcmp(classy,'LIBLINEAR_L1R_L2LOSS_SVC')
  sg('set_features','TRAIN',X');
else
  sg('set_features','TRAIN',X);
end
sg('new_classifier',classy);
sg('train_classifier');
[b,w_] = sg('get_classifier');

w = abs(w_')/norm(w_);


% COMPUTE RANKINGS
w = (sum(abs(reshape(w(:)',3,num_feat/3)).^p)).^(1/p);
w = w/norm(w);
%[foo ranking] = sort(w,'descend');
w_avg = median(median(w_val,1),3);
w_avg = mean(reshape(w_avg(:)',3,num_feat/3),1);
w_avg = w_avg/norm(w_avg);
w_avg_filtered = median(median(w_val_filtered,1),3);
w_avg_filtered = mean(reshape(w_avg_filtered(:)',3,num_feat/3),1);
w_avg_filtered = w_avg_filtered/norm(w_avg_filtered);
%[foo, ranking_avg] = sort(w_avg,'descend');


% COLLECT RESULTS
% result = struct('w_avg',w_avg(:)','w',w(:)','acc_val',acc_val,'acc_test',acc_test,'Cs',Cs,'C',C,'rep',rep,'classy',classy,'time',time,'time_val',time_val,'epsilon',epsilon);
result = struct('w_avg',w_avg(:)','w_avg_filtered', w_avg_filtered,'w',w(:)','acc_val',acc_val,'acc_test',acc_test,'acc_val_filtered',acc_val_filtered,'acc_test_filtered',acc_test_filtered,'acc_test_all',acc_test_all,'acc_test_all_filtered',acc_test_all_filtered, 'Cs',Cs,'C',C,'rep',rep,'classy',classy,'epsilon',epsilon);


% CLEAN UP MEMORY
sg('clean_features', 'TRAIN');
sg('clean_features', 'TEST');

