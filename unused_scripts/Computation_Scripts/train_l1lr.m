% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_l1lr( X, y, rep, Cs, p)

tic;

% function result = train_liblinear( X, y, rep,  )

% :( :(
X = double(X);
% should be fixed to limit memory requirements...

% PARAMETERS
if ~exist('rep') 
  if length(Cs)==1,
    rep = 1; 
  else
    rep = 30;
  end
end
if ~exist('Cs'), Cs = logspace(-5,5,11); end
if ~exist('p'),  p = 2; end

% INITIALIZATION 
num_data = length(y);
num_feat = size(X,1);
acc_val = zeros(rep,length(Cs));
acc_test = zeros(rep,length(Cs));
train_time = zeros(rep,length(Cs));
w_val =  zeros(rep,num_feat,length(Cs));
w =  zeros(rep,num_feat);
ranking =  zeros(rep,num_feat,length(Cs));
w_warmstart = zeros(num_feat,1); % for Warmstart
b = 0; % for Warmstart
opt.display = 0;


% VALIDATION LOOP
if rep > 1 | length(Cs)>1
	% if rep==1 skip validation loop!

	idx = 0;
	for i = 1:rep

		% TRAIN / VAL SPLITS
		perm = randperm(num_data);
		divtr = perm(1:floor(num_data/2));
		divval = perm(floor(num_data/2)+1:floor(num_data*3/4));
		divte = perm(floor(num_data*3/4)+1:end);	
		Xtr = X(:,divtr);
		Xval = X(:,divval);
		Xte = X(:,divte);
		ytr = y(divtr);
		yval = y(divval);
		yte = y(divte);

		for j = 1:length(Cs)
			C = Cs(j);
		  
			% SVM TRAINING
			tic;
			[w_,b,status] = dallrl1( w_warmstart, b, Xtr', ytr', 1/C,  opt );
			w_warmstart = w_;
			w_ = full(w_);
			train_time(i,j) = toc;
			w_val(i,:,j) = abs(w_)/norm(w_);

			
			% SVM TESTING 
			out = w_' * Xval + b;
			acc_val(i,j) = 1-mean(yval~=sign(out));
			out = w_' * Xte + b;
			acc_test(i,j) = 1-mean(yte~=sign(out));

			idx = idx + 1;
			%print_progress(idx,rep*length(Cs));
			
		end;

	end;

	% MODEL SELECTION
	[foo ind] = max(mean(acc_val,1));
	verbose.acc_test = mean(acc_test,1);
	verbose.acc_test_stderr = std(acc_test,[],1) / sqrt(rep);
	acc_test_stderr = std(acc_test(:,ind)) / sqrt(rep);
	acc_test = mean(acc_test(:,ind),1);
	C = Cs(ind);

else

	C = Cs;
	w_ = w_warmstart;
	b = 0;

end


% SVM TRAINING ON FULL DATA
[w_,b,status] = dallrl1( w_, b, X', y', 1/C, opt );
w_warmstart = w_;
w_ = full(w_);
w = abs(w_)/norm(w_);


% COMPUTE RANKINGS
w = (sum(abs(reshape(w(:)',3,num_feat/3)).^p)).^(1/p); 
w = w/norm(w);
if rep>1
	w_avg = median(w_val(:,:,ind),1);
	w_avg = mean(reshape(w_avg(:)',3,num_feat/3),1);
	w_avg = w_avg/norm(w_avg);
	result = struct('w_avg',w_avg(:)','w',w(:)','acc_val',acc_val,'acc_test',acc_test,'acc_test_stderr',acc_test_stderr,'verbose',verbose,'Cs',Cs,'C',C,'rep',rep,'time',toc,'train_time',train_time);
else
	% COLLECT RESULTS
	result = struct('w',w(:)','Cs',Cs,'C',C,'rep',rep,'time',toc,'train_time',train_time);
end


