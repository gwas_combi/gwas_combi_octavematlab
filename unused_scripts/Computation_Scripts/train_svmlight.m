% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function result = train_svmlight( X, y, rep, Cs, p, classy, epsilon )

tic;

% PARAMETERS
if ~exist('rep') 
  if length(Cs)==1,
    rep = 1; 
  else
    rep = 30;
  end
end
if ~exist('Cs'), Cs = logspace(-5,5,11); end
if ~exist('p'),  p = 2; end
if ~exist('epsilon'), 
  epsilon = 1e-3;
end
if ~exist('classy'), 
  classy = 'SVMLIGHT';
  % Can be: 'SVMLIGHT', 'LIBSVM'
end
%sg('loglevel', 'ERROR');     % 'ERROR' or 'ALL'



% INITIALIZATION 
num_data = length(y);
sg('new_svm', classy);
sg('svm_use_bias', 1);
if size(y,1)>size(y,2), y=y'; end
sg('svm_epsilon',epsilon);
num_feat = size(X,1);
acc_val = zeros(rep,length(Cs));
acc_test = zeros(rep,length(Cs));
w_val =  zeros(rep,num_feat,length(Cs));
w =  zeros(rep,num_feat);
ranking =  zeros(rep,num_feat,length(Cs));


% KERNEL PRE-COMPUTATION
X = double(X);
K = addRidge(X'*X);


% VALIDATION LOOP
if rep > 1 | length(Cs)>1
	% if rep==1 skip validation loop!

	idx = 0;
	for i = 1:rep

		% TRAIN / VAL SPLITS
		perm = randperm(num_data);
		divtr = perm(1:floor(num_data/2));
		divval = perm(floor(num_data/2)+1:floor(num_data*3/4));
		divte = perm(floor(num_data*3/4)+1:end);	
		Xtr = X(:,divtr);
		Ktr = K(divtr,divtr);
		Kval = K(divtr, divval);
		Kte = K(divtr, divte);
		ytr = y(divtr);
		yval = y(divval);
		yte = y(divte);

		for j = 1:length(Cs)
			C = Cs(j);
				
			% SVM TRAINING
			sg('set_labels','TRAIN', ytr);
			sg('set_kernel', 'CUSTOM', Ktr, 'FULL');
			sg('init_kernel','TRAIN');
			sg('c', C);
			sg('svm_train');
			[b,alpha] = sg('get_svm');
			w_ = Xtr(:,alpha(:,2)+1) * alpha(:,1);
			w_val(i,:,j) = abs(w_')/norm(w_);
			
			% SVM TESTING 
			s = b + alpha(:,1)' * Kval(alpha(:,2)+1,:);
			acc_val(i,j) = 1-mean(yval~=sign(s));
			s = b + alpha(:,1)' * Kte(alpha(:,2)+1,:);
			acc_test(i,j) = 1-mean(yte~=sign(s));

			idx = idx + 1;
			%print_progress(idx,rep*length(Cs));
			
		end;

	end;


	% MODEL SELECTION
	[foo ind] = max(mean(acc_val,1));
	acc_test = mean(acc_test(:,ind),1);
	C = Cs(ind);

else

	C = Cs;

end


% SVM TRAINING ON FULL DATA
sg('set_labels','TRAIN', y);
sg('set_kernel', 'CUSTOM', K, 'FULL');
sg('init_kernel','TRAIN');
sg('new_svm', classy);
sg('c', C);
sg('svm_train');
[b,alpha] = sg('get_svm');
sg('clean_kernel');


% COMPUTE w
w_ = X(:,alpha(:,2)+1) * alpha(:,1);
w = abs(w_')/norm(w_);
w = (sum(abs(reshape(w(:)',3,num_feat/3)).^p)).^(1/p); 
w = w/norm(w);
if rep>1
	w_avg = median(w_val(:,:,ind),1);
	w_avg = mean(reshape(w_avg(:)',3,num_feat/3),1);
	w_avg = w_avg/norm(w_avg);
	result = struct('w_avg',w_avg(:)','w',w(:)','acc_val',acc_val,'acc_test',acc_test,'Cs',Cs,'C',C,'rep',rep,'classy',classy,'epsilon',epsilon,'time',toc);
else
	% COLLECT RESULTS
	result = struct('w',w(:)','Cs',Cs,'C',C,'rep',rep,'classy',classy,'epsilon',epsilon,'time',toc);
end




