% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Choose Phenotype (i.e. disease)  			 	 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disease_abb = 'BD'; %'BD', 'CAD','CD','HT','RA','T1D','T2D'
disease = 'BipolarDisorder' %'BipolarDisorder', 'CoronaryArteryDisease','CrohnsDisease','Hypertension','RheumatoidArthritis','Type1Diabetes','Type2Diabetes'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Directories: Please change to yours  			 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

workdir = '/home/bmieth/svn/iew/scripts/marius/old_scripts/SVM_training/';
cd(workdir);
savedir = ['/home/bmieth/svn/iew/scripts/marius/old_scripts/SVM_training/',disease_abb,'_'];
datadir = ['/home/bmieth/svn/iew/data/wtccc/',disease,'/'];
script_path ='/home/bmieth/svn/iew/scripts/marius/computation_scripts';
addpath(script_path);

chromos = 1:22;

for chromo = chromos
	chromo,
	%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%   Main Scripts!   %%%
	%%%%%%%%%%%%%%%%%%%%%%%%%
	
	parameters;
	svm_training;

	savefile = [savedir 'result_' int2str_leading_zeros(chromo,2) '.mat'];
	save(savefile);
end