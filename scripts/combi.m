% GWAS COMBI method Matlab/Octave scripts
% Copyright (C) 2015 Technische Universitaet Berlin
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [p_values, selected_indices, svm_weights, selected_rs_ids] ...
	= combi(dataset_info, data_dir)
% A complete COMBI test run,
% which expects data in our own Matlab/Octave format;
% see load_dataset.m for more detail about that.

add_required_paths();

load_dataset(dataset_info, data_dir);

results_dir = data_dir;

% init params to default values
combi_params = combi_create_default_params();
% overwrite with user-defined values
combi_params = combi_set_params(combi_params);

[p_values, selected_indices, svm_weights, selected_rs_ids] ...
	= combi_method_and_plots(data_dir, combi_params, results_dir);

